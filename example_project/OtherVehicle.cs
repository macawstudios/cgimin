﻿using System;
using System.Collections.Generic;
using Engine.cgimin.helpers;
using lv1.vehicle;
using lv1.track;
using lv1.courses;
using Engine.cgimin.camera;
using OpenTK;

namespace lv1
{
    class OtherVehicle
    {
        // eneum for actions, wait, attack etc.
        public enum ActionEnum : int {
            NONE = 0,
            ATTACK_LEFT = 1,
            ATTACK_RIGHT = 2,
            ROLL_LEFT = 3,
            ROLL_RIGHT = 4,
            WAIT_ATTACK = 5
        }

        // own ID
        int ownID;
        
        // acceleration values
        private float accelerationBoost = 0.05f;
        private float acceleration = 0.03f;
        private float deAcceleration = 0.975f;

        // the position on the Street, ignoring jumping up position
        private Vector3 positionOnStreet;

        // the position
        private Vector3 position;

        // trackPos is the current position regarding the TrackData 
        private float trackPos;

        // relXPos in a range 0.0 ... 1.0, left side if track -> right side of track
        private float relXPos;

        // speed value in x-direction 
        private float xSpeed;

        // properties for wipe and scew animation
        private float wipeValue;
        private float scewValue;
        private float wipeValueToGo;
        private float scewValueToGo;

        // animation rotation value
        private float aniRotY = 0.0f;

        // uncollide blink
        private int uncollideBlink;

        // the vehicle object
        public Vehicle vehicleObject { get; private set; }

        // vectors to describe vehicle direction
        private Vector3 left;
        private Vector3 up;
        private Vector3 front;

        // the current speed
        public float Speed { get; set; }

        // bounce is the direction in which  the vehicle bounces 
        public Vector2 Bounce;

        // driving logic, where to steer next?
        private int changeTime;
        private float lastXGoal;

        // properties for exposion logic
        private ActionEnum currentAction;
        private int actionCounter;

        // explosion logic
        private int explosionCounter;

        // startCounter
        private int startCounter = 120;

        // animation Matrix;
        private Matrix4 animationMatrix1;
        private Matrix4 animationMatrix2;

        // logic for in air state (when vehicle is jumping / falling doww)
        private bool inAir;
        private bool inFall;
        private float airHeight;
        private float fallingSpeed;
        private int inFallCount;

        // properties for boost logic, when vehicle 
        private int boostCounter;
        private float maxAccelerationSpeed;

        // instances of all other vehicles for collision detection / behaviour
        public static List<OtherVehicle> Instances = new List<OtherVehicle>();

        // general update for animation-logic etc.
        private int update;
        private Random random;

        // energy / health
        private float energy;
        private int subEnergyWait;

        // up / down velocity
        private float upDownValue;

        //non-public constructor for deserialization. Otherwise JsonDeserializer would call the normal constructor with 0 as int values, causeing division by zero errors.
        private OtherVehicle()
        {

        }
        
        // driver skill
        private int skill;
        private int skillCount;
        private bool driveSlower;
        private int lightThrottle;

        public OtherVehicle(int startID, int startColumns, int driverSkill)
        {
            relXPos = TrackPart.GetRelXFromStartID(startID, startColumns);
            trackPos = TrackPart.GetTrackPosFromStartID(startID, startColumns);

            skill = driverSkill;

            if (skill == 0) driveSlower = true;

            vehicleObject = new Vehicle(CourseBase.deferredScreneDescription.SpecularIBL);
            positionOnStreet = TrackPart.GetPosition(trackPos, relXPos);
            position = positionOnStreet;

            Bounce = new Vector2();
            update = startID * 20;

            front = new Vector3(0, 0, 1);
            up = new Vector3(0, 1, 0);
            left = new Vector3(1, 0, 0);

            // drive logic init;
            float testSpeed = 0;
            for (int i = 0; i < 200; i++)
            {
                testSpeed += accelerationBoost;
                testSpeed *= deAcceleration;
            }
            maxAccelerationSpeed = testSpeed;

            random = new Random();
            lastXGoal = relXPos;
            changeTime = 120 + random.Next(500);
            xSpeed = 0;
            currentAction = ActionEnum.NONE;
            actionCounter = 400;
            wipeValue = 0;
            scewValue = 0;
            wipeValueToGo = 0;
            scewValueToGo = 0;
            boostCounter = 0;
            explosionCounter = 0;
            fallingSpeed = 0;
            inFallCount = 0;
            uncollideBlink = 0;
            energy = 100;

            skillCount = random.Next(300) + 200;

            Instances.Add(this);
            ownID = Instances.Count - 1;

            inAir = false;
            inFall = false;
            airHeight = 0;
        }


        public float GetRaceOverallPos()
        {
            return trackPos - TrackPart.StartTrackPos;
        }


        public Vector3 GetLeftLightPosition()
        {
            return position + up * 0.2f - front + left * 0.2f ;
        }

        public Vector3 GetRightLightPosition()
        {
            return position + up * 0.2f - front - left * 0.2f;
        }

        /// <summary>
        /// Returns real word position. mTrackPos is the Array-Position in Trackdata, mRelPos the relative x-pos (0.0 -> 1.0) 
        /// </summary> 
        private Vector3 GetPosition(float mTrackPos, float mRelXPos)
        {
            if (mTrackPos < 0) mTrackPos += TrackPart.TrackInfo.Count;

            int trPos = (int)Math.Floor(mTrackPos) % TrackPart.TrackInfo.Count;
            Vector3 back = (TrackPart.TrackInfo[trPos].rightStreetBack - TrackPart.TrackInfo[trPos].leftStreetBack) * mRelXPos + TrackPart.TrackInfo[trPos].leftStreetBack;
            Vector3 front = (TrackPart.TrackInfo[trPos].rightStreetFront - TrackPart.TrackInfo[trPos].leftStreetFront) * mRelXPos + TrackPart.TrackInfo[trPos].leftStreetFront;
            float subPos = (mTrackPos % TrackPart.TrackInfo.Count) - trPos;
            return (front - back) * subPos + back;
        }


        /// <summary>
        /// Check if Y-Gap is crossed. Y-Gap is when track continous on on a lower posion
        /// </summary> 
        private bool CrossedYGap(float oldTrackPos, float currentTrackPos)
        {
            bool ret = false;
            int trPos1 = (int)Math.Floor(oldTrackPos) % TrackPart.TrackInfo.Count;
            int trPos2 = (int)Math.Floor(currentTrackPos) % TrackPart.TrackInfo.Count;

            if (trPos1 < 0) trPos1 = 0;
            if (trPos2 < 0) trPos2 = 0;

            for (int i = trPos1; i <= trPos2; i++) {
                if (TrackPart.TrackInfo[i].hasYGap) ret = true;
            }
            return ret;
        }


        /// <summary>
        /// Drive forward, return new position in Track-Array space.
        /// </summary> 
        private float DriveForward(float mTrackPos, float mRelXPos, float mSpeed)
        {
            bool added = false;
            if (mTrackPos < 0)
            {
                added = true;
                mTrackPos += TrackPart.TrackInfo.Count;
            }
            Vector3 back = new Vector3();
            Vector3 front = new Vector3();
            float currentLength = 1.0f;
            int trackLength = TrackPart.TrackInfo.Count;

            bool end = false;
            while (end == false) {
                int trPos = (int)Math.Floor(mTrackPos) % trackLength;
                back = (TrackPart.TrackInfo[trPos].rightStreetBack - TrackPart.TrackInfo[trPos].leftStreetBack) * mRelXPos + TrackPart.TrackInfo[trPos].leftStreetBack;
                front = (TrackPart.TrackInfo[trPos].rightStreetFront - TrackPart.TrackInfo[trPos].leftStreetFront) * mRelXPos + TrackPart.TrackInfo[trPos].leftStreetFront;
                currentLength = (back - front).Length;
                
                if (mSpeed >= currentLength)
                {
                    mSpeed -= currentLength;
                    mTrackPos += 1;
                }
                else { 
                    mTrackPos += mSpeed / currentLength;
                    end = true;
                }
            }

            if (added) mTrackPos -= TrackPart.TrackInfo.Count;

            return mTrackPos;
        }


        /// <summary>
        /// Compare method for z-sorting
        /// </summary> 
        public int ZCompareTo(OtherVehicle otherVehicle)
        {
            float td1 = (positionOnStreet - Camera.Position).LengthSquared;
            float td2 = (otherVehicle.positionOnStreet - Camera.Position).LengthSquared;

            if (td1 < td2) return -1;
            if (td1 > td2) return 1;
            return 0;
        }


        /// <summary>
        /// Check if on brake field
        /// </summary> 
        private bool IsOnBrakeField(float mTrackPos, float mRelXPos)
        {
            if (mRelXPos < 0 || mRelXPos > 1.0) return false;
            if (mTrackPos < 0) mTrackPos += TrackPart.TrackInfo.Count;
            int trPos = (int)Math.Floor(mTrackPos) % TrackPart.TrackInfo.Count;
            return (TrackPart.TrackInfo[trPos].brakeFields[5-(int)Math.Round(mRelXPos * 5)]);
        }


        /// <summary>
        /// Get front vector using position and front position in Track-Array space
        /// </summary> 
        private Vector3 GetDirectFront(float mTrackPos, float mTrackPosFront, float mRelXPos)
        {
            return (GetPosition(mTrackPosFront, mRelXPos) - GetPosition(mTrackPos, mRelXPos)).Normalized();
        }


        /// <summary>
        /// Get up vector
        /// </summary> 
        private Vector3 GetUp(float mTrackPos, float mRelXPos)
        {
            Vector3 p1 = GetPosition(mTrackPos, mRelXPos - 0.1f);
            Vector3 p2 = GetPosition(mTrackPos + 0.1f, mRelXPos);
            Vector3 p3 = GetPosition(mTrackPos, mRelXPos + 0.1f);
            return Vector3.Cross((p2 - p1), (p3 - p2)).Normalized();
        }


        /// <summary>
        /// Returns a random relative x position where no brake field is set
        /// </summary> 
        private float ChooseSafeRelX(int trPos)
        {
            List<int> solutions = new List<int>(); 

            for (int o = 0; o < 6; o++) {
                if (!TrackPart.TrackInfo[trPos].brakeFields[o])
                {
                    solutions.Add(o);
                }
            }

            if (solutions.Count == 0)
            {
                return 0.5f;
            } else
            {
                return 1.0f-(solutions[(int)(solutions.Count * random.NextDouble())] / 6.0f + 0.1666f);
            }
        }


        /// <summary>
        /// Returns the current track width, parameter mTrackPos in Track-Array space.
        /// </summary> 
        private float GetTrackWidth(float mTrackPos)
        {
            if (mTrackPos < 0) mTrackPos += TrackPart.TrackInfo.Count;
            int trPos = (int)Math.Floor(mTrackPos) % TrackPart.TrackInfo.Count;
            return (TrackPart.TrackInfo[trPos].leftBorderBack - TrackPart.TrackInfo[trPos].rightBorderBack).Length;
        }


        /// <summary>
        /// Updates the attack driving logic
        /// </summary> 
        private void updateDrivingAttackLogic()
        {
            aniRotY = 0;
            if (currentAction == ActionEnum.ROLL_LEFT)
            {
                if (xSpeed > -0.07f) xSpeed -= 0.02f;
                if (xSpeed < -0.07f) xSpeed = -0.07f;
                wipeValueToGo = -4.2f;
                aniRotY = -(float)(Math.PI * 2.0f) / 20.0f * actionCounter;
            }

            if (currentAction == ActionEnum.ROLL_RIGHT)
            {
                if (xSpeed < 0.07f) xSpeed += 0.02f;
                if (xSpeed > 0.07f) xSpeed = 0.07f;
                wipeValueToGo = 4.2f;
                aniRotY = (float)(Math.PI * 2.0f) / 20.0f * actionCounter;
            }

            if (currentAction == ActionEnum.ATTACK_LEFT)
            {
                if (xSpeed > -0.05f) xSpeed -= 0.01f;
                if (xSpeed < -0.05f) xSpeed = -0.05f;

                if (wipeValueToGo < 4.0) wipeValueToGo += 1.0f;
                if (wipeValueToGo > 4.0) wipeValueToGo = 4.0f;

                scewValueToGo = 2;
            }

            if (currentAction == ActionEnum.ATTACK_RIGHT)
            {
                if (xSpeed < 0.05f) xSpeed += 0.01f;
                if (xSpeed > 0.05f) xSpeed = 0.05f;

                if (wipeValueToGo > -4.0) wipeValueToGo -= 1.0f;
                if (wipeValueToGo < -4.0) wipeValueToGo = -4.0f;

                scewValueToGo = -2;
            }
        }


        /// <summary>
        /// Updates the steering logic
        /// </summary> 
        private void steerToXGoal()
        {
            if (currentAction == ActionEnum.NONE || currentAction == ActionEnum.WAIT_ATTACK)
            {
                float relXGoal = getRelXGoal(trackPos, relXPos);
                if (Math.Abs(relXGoal - relXPos) > 0.05f)
                {
                    if (relXGoal > relXPos) xSpeed += 0.01f;
                    if (relXGoal < relXPos) xSpeed -= 0.01f;

                    if (xSpeed > 0.04f) xSpeed = 0.04f;
                    if (xSpeed < -0.04f) xSpeed = -0.04f;

                }
                else
                {
                    if (xSpeed > 0) xSpeed -= 0.01f;
                    if (xSpeed < 0) xSpeed += 0.01f;

                    if (Math.Abs(xSpeed) <= 0.02f) xSpeed = 0;
                }
            }
        }


        /// <summary>
        /// Updates the speed logic
        /// </summary> 
        private void updateSpeedLogic(float trackWidth)
        {
            if (startCounter == 0 && explosionCounter == 0)
            {
                relXPos += xSpeed / trackWidth;

                if (boostCounter == 0)
                {
                    Speed += acceleration;
                }
                else
                {
                    Speed += accelerationBoost;
                }
                if (boostCounter > 0) boostCounter--;
                Speed *= deAcceleration;
            }
        }


        /// <summary>
        /// Check if collision with track-items is there and do influence
        /// </summary> 
        private void checkItemCollision()
        {
            for (int i = 0; i < TrackPart.TrackItems.Count; i++)
            {
                if ((TrackPart.TrackItems[i].position - position).LengthSquared < 2.0f * 2.0f)
                {
                    if (TrackPart.TrackItems[i].type == 0 && boostCounter < 100) // boost
                    {
                        boostCounter = 120;
                        Speed = maxAccelerationSpeed;
                    }
                    if (TrackPart.TrackItems[i].type == 1) // jump
                    {
                        inAir = true;
                        fallingSpeed = -0.33f;
                        position.Y += 0.75f;
                    }
                }
            }
        }


        /// <summary>
        /// Returns the next x position goal. 
        /// </summary> 
        private float getRelXGoal(float mTrackPos, float mRelXPos)
        {
            if (mRelXPos > 1.0f || mRelXPos < 0.0f) return lastXGoal;

            if (mTrackPos < 0) mTrackPos += TrackPart.TrackInfo.Count;

            // look a bit forward
            int trPos = (int)Math.Floor(mTrackPos) % TrackPart.TrackInfo.Count;

            // check for brake field
            for (int i = 0; i < 40; i+=2)
            {
                int trPosFront = ((int)Math.Floor(mTrackPos) + i) % TrackPart.TrackInfo.Count;
                if (TrackPart.TrackInfo[trPosFront].containsBrakeField)
                {
                    if (TrackPart.TrackInfo[trPosFront].brakeFields[5 - (int)Math.Round(mRelXPos * 5)]) // only change when you are on a field which will turn into brake field
                    {
                        changeTime = 100;
                        lastXGoal = ChooseSafeRelX(trPosFront);
                        return lastXGoal;
                    }
                }
            }


            for (int i = 220; i < 225; i++)
            {
                int trPosFront = ((int)Math.Floor(mTrackPos) + i) % TrackPart.TrackInfo.Count;
                
                // check for special item
                if (TrackPart.TrackInfo[trPosFront].fieldSpecial != null)
                {
                    // Check for speed item on track
                    List<float> speedList = new List<float>();
                    foreach (TrackPart.ItemOnTrack item in TrackPart.TrackInfo[trPosFront].fieldSpecial)
                    {
                        if (item.type == 0) { // speed
                            speedList.Add(item.relX);
                        }
                    }
                    if (speedList.Count > 0) {
                        float nearest = 100.0f;
                        float goal = -1;
                        foreach (float xInSpeed in speedList) {
                            float oneMinus = 1.0f - xInSpeed;
                            if (Math.Abs(nearest - oneMinus) < nearest)
                            {
                                nearest = Math.Abs(oneMinus - xInSpeed);
                                goal = oneMinus;
                            }
                        }
                        if (goal > -1) {
                            changeTime = 300;
                            lastXGoal = goal;
                            return lastXGoal;
                        }
                    }
                }
            }


            changeTime--;
            if (changeTime <= 0)
            {
                //lastXGoal = random.Next(1000) / 1000.0f;
                lastXGoal = ChooseSafeRelX(trPos);
                changeTime = 300 + random.Next(500);
            }

            return lastXGoal;

        }


        /// <summary>
        /// decides which action to do next (attack, roll, wait)
        /// </summary> 
        private ActionEnum getCurrentAction(ActionEnum currentAction) {
            ActionEnum ret = currentAction;

            if (actionCounter > 0)
            {
                actionCounter--;
                if (actionCounter == 0)
                {
           
                    switch (currentAction)
                    {
                        case ActionEnum.ROLL_LEFT:
                            ret = ActionEnum.NONE; actionCounter = 100;
                            break;
                        case ActionEnum.ROLL_RIGHT:
                            ret = ActionEnum.NONE; actionCounter = 100;
                            break;
                        case ActionEnum.ATTACK_LEFT:
                            ret = ActionEnum.NONE; actionCounter = 100;
                            break;
                        case ActionEnum.ATTACK_RIGHT:
                            ret = ActionEnum.NONE; actionCounter = 100;
                            break;
                        case ActionEnum.WAIT_ATTACK:
                            actionCounter = 20;

                            // check for left right attack

                            if ((OwnVehicle.Instance.position - positionOnStreet).LengthSquared < 10 * 10)
                            {
                                float mD = -Vector3.Dot(positionOnStreet, left);
                                float planeD = GeometryHelpers.PlaneDistanceToPoint(left, mD, OwnVehicle.Instance.position);
                                bool leftOfYou = planeD < 0;

                                mD = -Vector3.Dot(positionOnStreet, front);
                                planeD = GeometryHelpers.PlaneDistanceToPoint(front, mD, OwnVehicle.Instance.position);
                                bool frontOfYou = planeD < 0;

                                bool youAreFaster = Speed < OwnVehicle.Instance.speed;

                                #region frontOfYou && youAreFaster
                                if (frontOfYou && youAreFaster)
                                {
                                    if (random.Next(100) < 70)
                                    {
                                        // Wipe Attack
                                        if (leftOfYou)
                                        {
                                            ret = ActionEnum.ATTACK_LEFT;
                                            actionCounter = 100;
                                        }
                                        else
                                        {
                                            ret = ActionEnum.ATTACK_RIGHT;
                                            actionCounter = 100;
                                        }
                                    }
                                    else
                                    {
                                        // Roll Attack
                                        if (leftOfYou)
                                        {
                                            ret = ActionEnum.ROLL_LEFT;
                                            actionCounter = 20;
                                        }
                                        else
                                        {
                                            ret = ActionEnum.ROLL_RIGHT;
                                            actionCounter = 20;
                                        }
                                    }
                                }
                                #endregion

                                #region !frontOfYou && !youAreFaster
                                if (!frontOfYou && !youAreFaster)
                                {
                                    if (random.Next(100) < 50)
                                    {
                                        if (leftOfYou)
                                        {
                                            ret = ActionEnum.ROLL_LEFT;
                                            actionCounter = 20;
                                        }
                                        else
                                        {
                                            ret = ActionEnum.ROLL_RIGHT;
                                            actionCounter = 20;
                                        }
                                    }
                                }
                                #endregion
                            }
                            break;
                        case ActionEnum.NONE:
                            ret = ActionEnum.WAIT_ATTACK;
                            actionCounter = 10;
                            break;
                    }
                }
            }
            return ret;
        }


        /// <summary>
        /// Checks collision with other vehicles
        /// </summary> 
        private void checkCollisionWithOthers()
        {
            if (IsCollidable())
            {
                for (int i = 0; i < OtherVehicle.Instances.Count; i++)
                {
                    if (ownID != i)
                    {
                        float d = -Vector3.Dot(GetPosition(trackPos, relXPos), left);
                        float xDist = GeometryHelpers.PlaneDistanceToPoint(left, d, OtherVehicle.Instances[i].positionOnStreet);

                        float zDist = trackPos - OtherVehicle.Instances[i].trackPos;

                        if (Math.Abs(xDist) < 2.0f && Math.Abs(zDist) < 4.0f)
                        {
                            xSpeed = 0;
                            Bounce.X = xDist * 0.1f;
                            Bounce.Y = zDist * 0.1f;
                            subEnergy(5);
                        }
                    }
                }
            }
        }


        /// <summary>
        /// Checks collision with cylinders on track
        /// </summary> 
        private void checkCollisionWithCylinders()
        {
            foreach (TrackPart.TrackItem item in TrackPart.TrackItems)
            {
                if (item.type >= 2) // cylinders
                {
                    Vector3 np = GeometryHelpers.NearestPointOnLine(item.position, item.positionUpper, position);
                    float itemCompare = item.radius + 0.7f;
                    if ((np-position).LengthSquared < itemCompare * itemCompare)
                    {
                        xSpeed = 0;
                        BounceFromPos(np, Math.Max(Speed * 1.0f, 0.02f));
                        Speed *= 0.1f;
                        subEnergy(2);
                    }
                    
                }
            }
        }


        /// <summary>
        /// Updates wipe and turn value
        /// </summary> 
        private void updateWipeAndTurnvalue()
        {
            if (currentAction != ActionEnum.ATTACK_RIGHT && currentAction != ActionEnum.ATTACK_LEFT && currentAction != ActionEnum.ROLL_LEFT && currentAction != ActionEnum.ROLL_RIGHT)
            {
                float backPos = trackPos - 10.0f;
                if (backPos < 0) backPos += TrackPart.TrackInfo.Count;
                float d = -Vector3.Dot(GetPosition(backPos, relXPos), left);
                float val = Vector3.Dot(left, GetPosition(trackPos + 15.0f, relXPos)) + d;
                wipeValueToGo = val * 1.0f;
                scewValueToGo = val * 1.2f;
                wipeValueToGo = Math.Max(-2.0f, wipeValueToGo);
                wipeValueToGo = Math.Min(2.0f, wipeValueToGo);
                scewValueToGo = Math.Max(-1.5f, scewValueToGo);
                scewValueToGo = Math.Min(1.5f, scewValueToGo);
            }
            wipeValue = wipeValue * 0.8f + wipeValueToGo * 0.2f;
            scewValue = scewValue * 0.8f + scewValueToGo * 0.2f;
        }


        /// <summary>
        /// reduced the energy of vehicle
        /// </summary> 
        private void subEnergy(float amount)
        {
            if (subEnergyWait > 0) return;
            subEnergyWait = 10;
            energy -= amount;
            if (energy <= 0)
            {
                energy = 100;
                Explode();
            }
        }

        /// <summary>
        /// decides whether to drive a little bit slower or to use boost, depending if skill is 0, 1, or 2
        /// </summary> 
        private void skillDriveLogic()
        {
            if (skillCount > 0) skillCount--;
            if (skillCount == 0)
            {
                int rnd = random.Next(100);
                if (skill == 0)
                {
                    if (rnd < 10)
                    {
                        if (energy > 50)
                        {
                            boostCounter = 120;
                            Speed = maxAccelerationSpeed;
                            subEnergy(17);
                        }
                    }
                    else if (rnd < 60)
                    {
                        driveSlower = true;
                    }
                    else
                    {
                        driveSlower = false;
                    }
                }

                if (skill == 1)
                {
                    if (rnd < 20)
                    {
                        if (energy > 50)
                        {
                            boostCounter = 120;
                            Speed = maxAccelerationSpeed;
                            subEnergy(17);
                        }
                    }
                    else if (rnd < 30)
                    {
                        driveSlower = true;
                    }
                    else
                    {
                        driveSlower = false;
                    }
                }

                if (skill == 2)
                {
                    if (rnd < 60)
                    {
                        if (energy > 30)
                        {
                            boostCounter = 120;
                            Speed = maxAccelerationSpeed;
                            subEnergy(17);
                        }
                    }

                    driveSlower = false;
                }

                skillCount = random.Next(500) + 500;
            }
        }


        private void checkUpdateXSteer(float mTrackPos,  float mTrackWidth)
        {

            if (mTrackPos < 0) mTrackPos += TrackPart.TrackInfo.Count;
            int trPos = (int)Math.Floor(mTrackPos) % TrackPart.TrackInfo.Count;


            float realXPos = relXPos * mTrackWidth;
            if (realXPos > mTrackWidth - 0.5f && TrackPart.TrackInfo[trPos].hasLeftBorder) { realXPos = mTrackWidth - 0.5f; Bounce.X = -Math.Abs(Bounce.X); subEnergy(2); }
            if (realXPos < 0.5f && TrackPart.TrackInfo[trPos].hasRightBorder) { realXPos = 0.5f; Bounce.X = Math.Abs(Bounce.X); subEnergy(2); }
            relXPos = realXPos / mTrackWidth;

        }


        /// <summary>
        /// Update called 60 times a second
        /// </summary> 
        public void Update()
        {
            // update
            update++;

            skillDriveLogic();

            if (subEnergyWait > 0) subEnergyWait--;

            // red blink when energy is low
            vehicleObject.SetRedBlink(energy < 20);

            // uncollide blink
            if (uncollideBlink > 0) uncollideBlink--;

            // explosion counter
            if (explosionCounter > 0) explosionCounter--;

            // start counter
            if (startCounter > 0) startCounter--;

            // track width
            float trackWidth = GetTrackWidth(trackPos);

            // bounce
            Bounce *= 0.97f;
            if (Bounce.Length < 0.01) Bounce *= 0;

            // add bounce to xpos;
            relXPos += Bounce.X / trackWidth;

            checkUpdateXSteer(trackPos, trackWidth);

            steerToXGoal();

            updateDrivingAttackLogic();

            currentAction = getCurrentAction(currentAction);

            updateSpeedLogic(trackWidth);

            // drive forward
            float oldTrackPos = trackPos;
            float driveLogicSpeed = Speed;
            if (driveSlower) driveLogicSpeed *= 0.95f;
            trackPos = DriveForward(trackPos, relXPos, driveLogicSpeed * OwnVehicle.SPEED_CLASS_MUL + Bounce.Y);
            Vector3 oldPositon = positionOnStreet; // save old position
            float oldY = positionOnStreet.Y;
            positionOnStreet = GetPosition(trackPos, relXPos);

            // check for brake field
            if (IsOnBrakeField(trackPos, relXPos)) {
                Speed *= 0.95f;
            }

            // checking for yGap
            if (CrossedYGap(oldTrackPos, trackPos) && (inAir == false)) {
                inAir = true;
                airHeight = Math.Abs(oldY - positionOnStreet.Y);
                fallingSpeed = 0;
            }

            // logic for in-air mode
            if (inAir || inFall) {
                airHeight -= fallingSpeed;
                fallingSpeed += TrackPart.gravityForce;
                if (airHeight <= 0) {
                    if (relXPos >= 0 && relXPos <= 1.0f && !inFall) // check for fall cases again here
                    {
                        fallingSpeed = 0;
                        airHeight = 0;
                    }
                    else {
                        if (!inFall) inFallCount = 0;
                        inFall = true;
                    }

                    inAir = false;
                }
            }

            // out of bounds
            if (relXPos < 0 || relXPos > 1.0f && !inFall) {
                inFall = true;
                inFallCount = 0;
            }

            if (inFall)
            {
                inFallCount++;
                if (inFallCount >= 300) {
                    Speed = 0;
                    relXPos = 0.5f;
                    inAir = false;
                    inFallCount = 0;
                    inFall = false;
                    uncollideBlink = 100;
                    airHeight = 0;
                }
            }

            // light throttle
            
            if (lightThrottle > 10) lightThrottle--;
            if (lightThrottle < 10) lightThrottle++;
            if (boostCounter > 0) lightThrottle = 20;

            // calculate position
            position = positionOnStreet + Vector3.UnitY * airHeight;

            // checken if above
            checkItemCollision();

            // calculate up, left front
            up = GetUp(trackPos, relXPos);
            Vector3 frontDirect = GetDirectFront(trackPos, trackPos + 10, relXPos);
            left = Vector3.Cross(up, frontDirect).Normalized();
            front = Vector3.Cross(left, up).Normalized();

            // collision mit den anderen
            checkCollisionWithOthers();

            // collision mit den cylindern
            checkCollisionWithCylinders();

            // calculate turn 'angle' and wipe
            updateWipeAndTurnvalue();

            // set rotation matrix from up, left, front
            Matrix4 frotateMatrix = Matrix4.Identity;
            frotateMatrix.Row0 = new Vector4(left, 0);
            frotateMatrix.Row1 = new Vector4(up, 0);
            frotateMatrix.Row2 = new Vector4(front, 0);

            // calc vehicleTransform
            Matrix4 vehicleTransform = Matrix4.CreateRotationY(-(float)Math.PI / 2.0f + aniRotY) * frotateMatrix;
            vehicleTransform *= Matrix4.CreateTranslation(position + up * 0.2f);
            vehicleObject.SetTransform(vehicleTransform);
            vehicleObject.CreatePartTransforms(wipeValue, scewValue);

            // animations
            animationMatrix2 = Matrix4.Identity;
            animationMatrix2 *= Matrix4.CreateTranslation(up * ((float)Math.Sin(update * 0.1f) * Math.Max(0, 0.01f * (1.0f - Speed)) + Math.Max(upDownValue, -0.10f)));
            animationMatrix1 = Matrix4.Identity;
            animationMatrix1 *= Matrix4.CreateRotationX((float)Math.Sin(update * 0.07f) * (float)Math.Sin(update * 0.03f) * 0.025f);

            // add tale to vehicle objects
            vehicleObject.AddTale(position + up * 0.02f, left);

            // up-down velocity handling
            if (inAir == false && inFall == false)
            {
                float dd = -Vector3.Dot(oldPositon, up);
                float velDist = GeometryHelpers.PlaneDistanceToPoint(up, dd, position);
                upDownValue += velDist * OwnVehicle.VELO_FORCE;
                if (upDownValue < -0.15f) upDownValue = -0.10f;
            }
            upDownValue *= OwnVehicle.VELO_SMOOTHOUT;


            // update vehicle object
            vehicleObject.Update();
        }


        /// <summary>
        /// draw
        /// </summary> 
        public void Draw()
        {
            vehicleObject.Draw(animationMatrix1, animationMatrix2);

            //if (explosionCounter == 0) vehicleObject.DrawLightTale(Math.Min(Speed * 2, 1.0f), ownID);
        }


        /// <summary>
        /// draws explosion, handling in vehicleObject is that this is only the case when there is really an explosion initated
        /// </summary> 
        public void DrawExplosion()
        {
            vehicleObject.DrawExplosion();
        }

        public void DrawLights()
        {
            vehicleObject.DrawLights(animationMatrix1, animationMatrix2, lightThrottle / 10.0f, 3.5f);
        }

  
        public void DrawFire()
        {
            vehicleObject.DrawFire(animationMatrix1, animationMatrix2, Math.Min(Speed * 20, 7));
        }
       

        /// <summary>
        /// draw the shadows
        /// </summary> 
        public void DrawShadow()
        {
            vehicleObject.DrawShadow(animationMatrix1, animationMatrix2);
        }


        /// <summary>
        /// returns if other vehicle can collide with this cehicle
        /// </summary> 
        public bool IsCollidable() {
            return explosionCounter == 0 && uncollideBlink == 0;
        }


        /// <summary>
        /// lets vehiclke blink
        /// </summary> 
        public void Blink()
        {
            vehicleObject.Blink();
        }


        /// <summary>
        /// lets vehiclke explode
        /// </summary> 
        public void Explode()
        {
            explosionCounter = 350;
            Speed = 0;
            vehicleObject.Explode();
        }


        /// <summary>
        /// Set Bounce, also called from outside for collision detection bewtween vehicles.
        /// </summary> 
        public void BounceFromPos(Vector3 pos, float intensity)
        {
            float xd = -Vector3.Dot(positionOnStreet, left);
            float xBounce = -GeometryHelpers.PlaneDistanceToPoint(left, xd, pos);
            float zd = -Vector3.Dot(positionOnStreet, front);
            float zBounce = -GeometryHelpers.PlaneDistanceToPoint(front, zd, pos);

            Bounce.X = xBounce;
            Bounce.Y = zBounce;

            Bounce.Normalize();
            Bounce *= intensity;
        }

        public OtherVehicle InitializeFrom(OtherVehicle toBeReplaced)
        {
            vehicleObject = toBeReplaced.vehicleObject;
            toBeReplaced.vehicleObject = null;

            return this;
        }

    }




}
