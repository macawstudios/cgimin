﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lv1
{
    internal static class OwnVehicleExtensions
    {
        static readonly float debugTickSpeed = 0.05f;

        internal static void TickDebugLeft(this OwnVehicle vehicle)
        {
            vehicle.position += vehicle.left * debugTickSpeed;

            vehicle.positionCameraToFollowVehicle(false);
            vehicle.updateVehicleGraphics(0);
        }

        internal static void TickDebugRight(this OwnVehicle vehicle)
        {
            vehicle.position -= vehicle.left * debugTickSpeed;

            vehicle.positionCameraToFollowVehicle(false);
            vehicle.updateVehicleGraphics(0);
        }

        internal static void TickDebugUp(this OwnVehicle vehicle)
        {
            vehicle.position += vehicle.up * debugTickSpeed;

            vehicle.positionCameraToFollowVehicle(false);
            vehicle.updateVehicleGraphics(0);
        }

        internal static void TickDebugDown(this OwnVehicle vehicle)
        {
            vehicle.position -= vehicle.up * debugTickSpeed;

            vehicle.positionCameraToFollowVehicle(false);
            vehicle.updateVehicleGraphics(0);
        }

        internal static void TickDebugForward(this OwnVehicle vehicle)
        {
            vehicle.position += vehicle.front * debugTickSpeed;

            vehicle.positionCameraToFollowVehicle(false);
            vehicle.updateVehicleGraphics(0);
        }

        internal static void TickDebugBackward(this OwnVehicle vehicle)
        {
            vehicle.position -= vehicle.front * debugTickSpeed;

            vehicle.positionCameraToFollowVehicle(false);
            vehicle.updateVehicleGraphics(0);
        }

        internal static void TickDebugSpeedIncrease(this OwnVehicle vehicle)
        {
            vehicle.speed += debugTickSpeed * 10;            
        }

        internal static void TickDebugSpeedDecrease(this OwnVehicle vehicle)
        {
            vehicle.speed -= debugTickSpeed * 10;
        }

        internal static void TickDebugRotateRight(this OwnVehicle vehicle)
        {
            //vehicle.CalculateVehicleDirection(false, true, OwnVehicle.ROTATE_SPEED, OwnVehicle.MAX_ROTATION, OwnVehicle.rBackIntens);
            vehicle.SetNewVehicleDirection(0.1f);
            vehicle.positionCameraToFollowVehicle(false);
            vehicle.updateVehicleGraphics(0);
        }

        internal static void TickDebugRotateLeft(this OwnVehicle vehicle)
        {
            //vehicle.CalculateVehicleDirection(true, false, OwnVehicle.ROTATE_SPEED, OwnVehicle.MAX_ROTATION, OwnVehicle.rBackIntens);
            vehicle.SetNewVehicleDirection(-0.1f);
            vehicle.positionCameraToFollowVehicle(false);
            vehicle.updateVehicleGraphics(0);
        }


        internal static void TickDebugCameraHigher(this OwnVehicle vehicle)
        {
            vehicle.currentCameraPreset.VehicleCameraHeightCoefficient += debugTickSpeed / 2;
            Console.WriteLine(String.Format("VehicleCameraHeightCoefficient: {0}", vehicle.currentCameraPreset.VehicleCameraHeightCoefficient));
        }


        internal static void TickDebugCameraLower(this OwnVehicle vehicle)
        {
            vehicle.currentCameraPreset.VehicleCameraHeightCoefficient -= debugTickSpeed / 2;
            Console.WriteLine(String.Format("VehicleCameraHeightCoefficient: {0}", vehicle.currentCameraPreset.VehicleCameraHeightCoefficient));
        }

        internal static void TickDebugCameraNearer(this OwnVehicle vehicle)
        {
            vehicle.currentCameraPreset.VehicleCameraDistanceCoefficient -= debugTickSpeed / 2;
            Console.WriteLine(String.Format("VehicleCameraDistanceCoefficient: {0}", vehicle.currentCameraPreset.VehicleCameraDistanceCoefficient));
        }

        internal static void TickDebugCameraFarther(this OwnVehicle vehicle)
        {
            vehicle.currentCameraPreset.VehicleCameraDistanceCoefficient += debugTickSpeed / 2;
            Console.WriteLine(String.Format("VehicleCameraDistanceCoefficient: {0}", vehicle.currentCameraPreset.VehicleCameraDistanceCoefficient));
        }

        internal static void TickDebugCameraAngleHigher(this OwnVehicle vehicle)
        {
            vehicle.currentCameraPreset.VehicleCameraAngleCoefficient += debugTickSpeed / 2;
            Console.WriteLine(String.Format("VehicleCameraAngleCoefficient: {0}", vehicle.currentCameraPreset.VehicleCameraAngleCoefficient));
        }

        internal static void TickDebugCameraAngleLower(this OwnVehicle vehicle)
        {
            vehicle.currentCameraPreset.VehicleCameraAngleCoefficient -= debugTickSpeed /2;
            Console.WriteLine(String.Format("VehicleCameraAngleCoefficient: {0}", vehicle.currentCameraPreset.VehicleCameraAngleCoefficient));
        }


        internal static void TickDebugEyeCameraInertiaHigher(this OwnVehicle vehicle)
        {
            vehicle.currentCameraPreset.VehicleCameraEyeInertiaCoefficient += debugTickSpeed/10;
            vehicle.currentCameraPreset.VehicleCameraEyeInertiaCoefficient = Math.Min(1f, vehicle.currentCameraPreset.VehicleCameraEyeInertiaCoefficient);
            vehicle.currentCameraPreset.VehicleCameraEyeInertiaCoefficient = Math.Max(0f, vehicle.currentCameraPreset.VehicleCameraEyeInertiaCoefficient);
            Console.WriteLine(String.Format("VehicleCameraEyeInertiaCoefficient: {0}", vehicle.currentCameraPreset.VehicleCameraEyeInertiaCoefficient));
        }


        internal static void TickDebugEyeCameraInertiaLower(this OwnVehicle vehicle)
        {
            vehicle.currentCameraPreset.VehicleCameraEyeInertiaCoefficient -= debugTickSpeed/10;
            vehicle.currentCameraPreset.VehicleCameraEyeInertiaCoefficient = Math.Min(1f, vehicle.currentCameraPreset.VehicleCameraEyeInertiaCoefficient);
            vehicle.currentCameraPreset.VehicleCameraEyeInertiaCoefficient = Math.Max(0f, vehicle.currentCameraPreset.VehicleCameraEyeInertiaCoefficient);
            Console.WriteLine(String.Format("VehicleCameraEyeInertiaCoefficient: {0}", vehicle.currentCameraPreset.VehicleCameraEyeInertiaCoefficient));
        }

        internal static void TickDebugTargetCameraInertiaHigher(this OwnVehicle vehicle)
        {
            vehicle.currentCameraPreset.VehicleCameraTargetInertiaCoefficient += debugTickSpeed / 10;
            vehicle.currentCameraPreset.VehicleCameraTargetInertiaCoefficient = Math.Min(1f, vehicle.currentCameraPreset.VehicleCameraTargetInertiaCoefficient);
            vehicle.currentCameraPreset.VehicleCameraTargetInertiaCoefficient = Math.Max(0f, vehicle.currentCameraPreset.VehicleCameraTargetInertiaCoefficient);
            Console.WriteLine(String.Format("VehicleCameraTargetInertiaCoefficient: {0}", vehicle.currentCameraPreset.VehicleCameraTargetInertiaCoefficient));
        }


        internal static void TickDebugTargetCameraInertiaLower(this OwnVehicle vehicle)
        {
            vehicle.currentCameraPreset.VehicleCameraTargetInertiaCoefficient -= debugTickSpeed / 10;
            vehicle.currentCameraPreset.VehicleCameraTargetInertiaCoefficient = Math.Min(1f, vehicle.currentCameraPreset.VehicleCameraTargetInertiaCoefficient);
            vehicle.currentCameraPreset.VehicleCameraTargetInertiaCoefficient = Math.Max(0f, vehicle.currentCameraPreset.VehicleCameraTargetInertiaCoefficient);
            Console.WriteLine(String.Format("VehicleCameraTargetInertiaCoefficient: {0}", vehicle.currentCameraPreset.VehicleCameraTargetInertiaCoefficient));
        }

    }
}
