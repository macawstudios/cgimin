﻿using OpenTK;
using lv1.track;
using System.Collections.Generic;
using Engine.cgimin.collision;
using Engine.cgimin.texture;
using Engine.cgimin.octree;
using Engine.cgimin.material;
using Engine.cgimin.object3d;
using Engine.cgimin.light;
using Engine.cgimin.camera;
using OpenTK.Graphics.OpenGL;
using Engine.cgimin.helpers;
using System;

namespace lv1.courses
{
    class SecondCourse : CourseBase
    {

        private int specularIBL;
        private int irradianceIBL;

        public override void LoadAndGenerate()
        {
            // Light
            Light.SetDirectionalLight(new Vector3(0.5f, 0.5f, 0.5f), new Vector4(0.15f, 0.15f, 0.15f, 1), new Vector4(0.9f, 0.9f, 0.9f, 0.0f), new Vector4(0.0f, 0.0f, 0.0f, 0.0f));

            // Textures
            List<string> textures = new List<string>();

            /*
            textures.Add("../../data/textures/skybox/Sky_Day Sun High SummerSky_Small_RT.png");
            textures.Add("../../data/textures/skybox/Sky_Day Sun High SummerSky_Small_LF.png");
            textures.Add("../../data/textures/skybox/Sky_Day Sun High SummerSky_Small_UP.png");
            textures.Add("../../data/textures/skybox/Sky_Day Sun High SummerSky_Small_DN.png");
            textures.Add("../../data/textures/skybox/Sky_Day Sun High SummerSky_Small_FR.png");
            textures.Add("../../data/textures/skybox/Sky_Day Sun High SummerSky_Small_BK.png");
            // skyCubeReflectionID = TextureManager.LoadCubemap(textures);
            */

            specularIBL = TextureManager.LoadIBLMap("../../data/Courses/vtrack/ibl/abend", "bmp");
            irradianceIBL = TextureManager.LoadIBLIrradiance("../../data/Courses/vtrack/ibl/abend", "bmp");

            CourseBase.deferredScreneDescription.SpecularIBL = specularIBL;
            CourseBase.deferredScreneDescription.IrradianceIBL = irradianceIBL; 

            Octree.MaxIterationDepth = 7;
            octree = new Octree(new Vector3(-1000, -1000, -1000), new Vector3(1000, 1000, 1000));
            shadowOctree = new Octree(new Vector3(-1000, -1000, -1000), new Vector3(1000, 1000, 1000));
            lightOctree = new Octree(new Vector3(-1000, -1000, -1000), new Vector3(1000, 1000, 1000));

            trackParts = new List<TrackPart>();

            float width = 15.0f;

            TrackPart.SetBaseHeight(47);
            TrackPart.SetStartRotation(0);
            TrackPart.SetStartWidth(width);
            TrackPart.SetBrakeFieldTexture(brakeColor, brakeNormal);

            collisionGrid = new CollisionGrid(100, 100, 100);
            TrackPart.SetDisplayOptions(0.7f, streetColor1, streetNormal1, 15, new bool[] { false, false, false, false, false, false }, TrackPart.BorderMode.border, TrackPart.BorderMode.border);

            // 1
            trackParts.Add(new TrackPart(collisionGrid, 0, 0, 0, width, 150, true));
            trackParts.Add(new TrackPart(collisionGrid,-20, 0, 0, width, 150, true));
            trackParts.Add(new TrackPart(collisionGrid, -20, 10, 0, width, 150, true));
            trackParts.Add(new TrackPart(collisionGrid, 10, 10, 0, width, 70, true));
            trackParts.Add(new TrackPart(collisionGrid, -10, 0, 0, width, 70, true));
            trackParts.Add(new TrackPart(collisionGrid, 10, 0, 0, width, 170, true));
            trackParts.Add(new TrackPart(collisionGrid, 90, 5, 0, width, 100, true));
            trackParts.Add(new TrackPart(collisionGrid, 90, 0, 0, 10, 100, true));
            trackParts.Add(new TrackPart(collisionGrid, 180, 12, 0, 10, 100, true));
         
            trackParts.Add(new TrackPart(collisionGrid, 180, 15, 5, 10, 50, true));
            trackParts.Add(new TrackPart(collisionGrid, 120, 12, 5, 10, 70, true));
            trackParts.Add(new TrackPart(collisionGrid, 100, 10, 0, 10, 50, true));
            trackParts.Add(new TrackPart(collisionGrid, 120, 5, -5, 10, 50, true));
            trackParts.Add(new TrackPart(collisionGrid, 90, 10, -5, 10, 50, true));
            trackParts.Add(new TrackPart(collisionGrid, 100, 9, -5, 10, 50, true));
            trackParts.Add(new TrackPart(collisionGrid, 90, 10, 0, 10, 50, true));
            trackParts.Add(new TrackPart(collisionGrid, 130, 6, 0, width, 150, true));
            trackParts.Add(new TrackPart(collisionGrid, 180, 0, 0, width, 150, true));
            trackParts.Add(new TrackPart(collisionGrid, 160, 17, 5, width, 200, true));
            trackParts.Add(new TrackPart(collisionGrid, 180, 17, 5, width, 200, true));
            trackParts.Add(new TrackPart(collisionGrid, 160, 12, 5, width, 100, true));
            trackParts.Add(new TrackPart(collisionGrid, 180, 10, 0, width, 100, true));
            trackParts.Add(new TrackPart(collisionGrid, 180, -10, 0, width, 310, true, 5));

            trackParts.Add(new TrackPart(collisionGrid, 270, 0, 0, width, 165, true));
            trackParts.Add(new TrackPart(collisionGrid, 330, 0, 5, width, 165, true));
            trackParts.Add(new TrackPart(collisionGrid, 270, 0, 0, width, 165, true));
            trackParts.Add(new TrackPart(collisionGrid, 330, 0, -5, width, 165, true));
            trackParts.Add(new TrackPart(collisionGrid, 270, 0, 0, width, 165, true));

            trackParts.Add(new TrackPart(collisionGrid, 360, 0, 0, width, 100, true));
            trackParts.Add(new TrackPart(collisionGrid, 360, 0, 0, width, 100, true, -1, true));

            // WriteObjFile("def_track.obj");
            // jumps, accelerators

            /*
            TrackPart.AddAccelerator(475 + cornerLength * 2, 0.25f);
            TrackPart.AddAccelerator(900 + cornerLength * 3, 0.75f);
            TrackPart.AddJump(1225 + cornerLength * 4, 0.25f);
            TrackPart.AddJump(1375 + cornerLength * 4, 0.75f);
            */

            TrackSceneFactory.Init();

            /*
            TrackSceneFactory.AddOption(new TrackSceneFactory.TrackSceneObjectOptions() { objectName = "breiter_pfeiler", fresnelValue = 1.0f, castShadow = true });
            TrackSceneFactory.AddOption(new TrackSceneFactory.TrackSceneObjectOptions() { objectName = "sail_side", fresnelValue = 1.0f, castShadow = true });
            TrackSceneFactory.AddOption(new TrackSceneFactory.TrackSceneObjectOptions() { objectName = "goal", fresnelValue = 1.0f, castShadow = true });
            TrackSceneFactory.AddOption(new TrackSceneFactory.TrackSceneObjectOptions() { objectName = "goal_light_panel", castShadow = true, material = TrackMaterials.worldGlow, postProcessing = true });
            TrackSceneFactory.AddOption(new TrackSceneFactory.TrackSceneObjectOptions() { objectName = "goal_cycle_light", castShadow = true, material = TrackMaterials.worldGlowCycle, speed = 0.01f, postProcessing = true });
            TrackSceneFactory.AddOption(new TrackSceneFactory.TrackSceneObjectOptions() { objectName = "glow_tower_glow", castShadow = false, material = TrackMaterials.worldGlow, postProcessing = true });
            */

            TrackSceneFactory.Build("vtrack", specularIBL, octree, shadowOctree, lightOctree, collisionGrid);

            //
            AddEnergyFence(false, 980, 10);
            AddEnergyFence(false, 1470, 10);
            AddEnergyFence(true, 2150, 20);
            AddEnergyFence(false, 2660, 30);


            //foreach (TrackPart trackPart in trackParts) trackPart.AddToOctree(octree, skyCubeReflectionID, 1.0f, TrackMaterials.world);
            //foreach (TrackPart trackPart in trackParts) trackPart.AddToOctreeWithShadow(octree, shadowOctree, skyCubeReflectionID, 1.0f, worldCascaded, trackCastShadow);

            base.ItemsToOctree();
            collisionGrid.FinalizeCollision();


        }

        public override void DrawShadows()
        {
            //shadowOctree.Draw();
        }

        public override void DrawLights()
        {
            lightOctree.Draw();
        }


        public override void Draw(float speedValue)
        {
            // Camera settings
            //Camera.SetDynamicFovNearFar(60, 0.1f, 1500.0f);
            Camera.SetupFog(30, 1000, new Vector3(0.7f, 0.5f, 0.0f));

            GL.Enable(EnableCap.CullFace);
            GL.CullFace(CullFaceMode.Front);

            TrackMaterials.world.PrepareSortedFlushDraw();
            octree.Draw();
            TrackMaterials.world.FlushDraw();


            GL.Disable(EnableCap.CullFace);
        }


    

    }
}
