﻿using System.Collections.Generic;
using Engine.cgimin.octree;
using lv1.track;
using Engine.cgimin.collision;
using Engine.cgimin.object3d;
using Engine.cgimin.texture;
using Engine.cgimin.deferred;
using OpenTK.Graphics.OpenGL;
using Engine.cgimin.deferred;
using OpenTK;

namespace lv1.courses
{
    public class CourseBase
    {

        public static DeferredSceneDesc deferredScreneDescription;

        // street textures
        internal int streetColor1;
        internal int streetNormal1;

        internal int streetColor2;
        internal int streetNormal2;

        internal int streetColor3;
        internal int streetNormal3;

        // brake field texture
        internal int brakeColor;
        internal int brakeNormal;

        // side textures
        internal int sideColor1;
        internal int sideNormal1;

        internal int sideColor2;
        internal int sideNormal2;

        internal int sideColor3;
        internal int sideNormal3;


        internal List<TrackPart> trackParts;

        public CollisionGrid collisionGrid { get; internal set; }

        internal Octree octree;
        internal Octree shadowOctree;
        internal Octree lightOctree;

        internal DeferredMaterialSettings castMaterialSettings;

        // 3d data / textures fot the items
        private BaseObject3D accelArrow;
        private BaseObject3D accelBottom;
        private DeferredMaterialSettings accelArrowMaterial;
        private DeferredMaterialSettings accelBottomMaterial;
        private int arrowTexture;

        private BaseObject3D cylinderSmall;

        // energy fence
        private BaseObject3D energyFenceObject;
        private int energyFenceNormalTexture;
        private int energyFenceColorTexture;
        private int energyFenceAdditionalTexture;

        public CourseBase()
        {
            castMaterialSettings = new DeferredMaterialSettings();
           
            arrowTexture = TextureManager.LoadTexture("data/textures/white_marble.png");
            accelArrow = new ObjLoaderObject3D("data/objects/accelerator_arrow.obj", 1.3f, true, true);
            accelBottom = new ObjLoaderObject3D("data/objects/accelerator.obj", 1.3f, true, true);
            cylinderSmall = new ObjLoaderObject3D("data/objects/solid_cylinder_small.obj", 1.0f, true);

            accelArrowMaterial = new DeferredMaterialSettings();
            accelArrowMaterial.colorTexture = arrowTexture;

            accelBottomMaterial = new DeferredMaterialSettings();
            accelBottomMaterial.colorTexture = arrowTexture;

            deferredScreneDescription = new DeferredSceneDesc();

            // energy fence
            energyFenceObject = new ObjLoaderObject3D("../../data/Courses/vtrack/energyfence.obj", 1.0f, true);
            energyFenceNormalTexture = TextureManager.LoadTexture("../../data/Courses/vtrack/energyfence_normal.tif");
            energyFenceColorTexture = TextureManager.LoadTexture("../../data/Courses/vtrack/energyfence.tif");
            energyFenceAdditionalTexture = TextureManager.LoadTexture("../../data/Courses/vtrack/energyfence_additional.tif");
        }


        public void WriteObjFile(string filename)
        {
            List<BaseObject3D> data = new List<BaseObject3D>();
            for (int i = 0; i < trackParts.Count; i++) data.Add(trackParts[i].streetPart);
            ObjLoaderObject3D.WriteObjFile(data, filename);
        }


        public void ItemsToOctree()
        {
            for (int i = 0; i < TrackPart.TrackItems.Count; i++)
            {
                if (TrackPart.TrackItems[i].type == 0) // speed
                {
                    octree.AddEntity(new OctreeEntity(accelArrow, TrackMaterials.world, accelArrowMaterial, TrackPart.TrackItems[i].transform));
                    octree.AddEntity(new OctreeEntity(accelBottom, TrackMaterials.world, accelBottomMaterial, TrackPart.TrackItems[i].transform));
                }

                if (TrackPart.TrackItems[i].type == 1) // jump
                {
                    octree.AddEntity(new OctreeEntity(accelArrow, TrackMaterials.world, accelArrowMaterial, TrackPart.TrackItems[i].transform));
                    octree.AddEntity(new OctreeEntity(accelBottom, TrackMaterials.world, accelBottomMaterial, TrackPart.TrackItems[i].transform));
                }

                if (TrackPart.TrackItems[i].type == 2) // small cylinder
                {
                    octree.AddEntity(new OctreeEntity(cylinderSmall, TrackMaterials.world, accelArrowMaterial, TrackPart.TrackItems[i].transform));
                }
            }
        }


        // glow fence
        private ObjLoaderObject3D glowFencePostRightObject;
        private ObjLoaderObject3D glowFencePostLeftObject;
        private ObjLoaderObject3D glowFencePlankObject;
        private int glowFenceStep;
        private int glowFenceTextureID;
        private float glowFenceScale;
        private DeferredMaterialSettings glowFencePostSetting;
        private DeferredMaterialSettings glowFencePlankSetting;

        public void InitGlowFence(int cubeTexture, int step, float scale, float speed, string plankObject, string postObject, string texture)
        {
            glowFenceScale = scale;
            glowFenceStep = step;
            glowFenceTextureID = TextureManager.LoadTexture(texture);
            glowFencePostRightObject = new ObjLoaderObject3D(postObject, scale, false, true);
            glowFencePostLeftObject = new ObjLoaderObject3D(postObject, scale, false, true, true);
            glowFencePlankObject = new ObjLoaderObject3D(plankObject, 1, false, true);

            glowFencePostSetting = new DeferredMaterialSettings();
            glowFencePostSetting.colorTexture = glowFenceTextureID;

            glowFencePlankSetting = new DeferredMaterialSettings();
            glowFencePlankSetting.colorTexture = glowFenceTextureID;
            glowFencePlankSetting.blendFactorDest = BlendingFactorDest.DstColor;
            glowFencePlankSetting.blendFactorSource = BlendingFactorSrc.One;
            glowFencePlankSetting.alpha = 1.0f;
            //glowFencePlankSetting.transparent = true;
            glowFencePlankSetting.textureAnimationSpeed = speed;

        }

        public void AddEnergyFence(bool left, int start, int count)
        {
            for (int i = 0; i < count; i++)
            {
                int index = start + i * 10;
                for (int o = index; o < index + 10; o++) {
                    if (left) {
                        TrackPart.TrackInfo[o].leftBorderType = 1;
                    } else {
                        TrackPart.TrackInfo[o].rightBorderType = 1;
                    }
                }
                TrackWrapObject wrapObject = new TrackWrapObject(energyFenceObject, index, TrackWrapObject.WRAPMODE_CONST_YUP, left, false, 1.0f);
                AddObject(wrapObject, energyFenceColorTexture, energyFenceNormalTexture, energyFenceAdditionalTexture,  false, false, wrapObject.Transformation);
            }
        }


        // wrap helpers
        /*
        public void WrapCubical(bool left, ObjLoaderObject3D object3d, int start, int count, int step, int colorTexture, int cubeTexture, int normalTexture, float fresnel, bool castShadow, bool castTransparent, bool receiveShadow,int wrapMode, float scale, bool startOutz = false,CollisionGrid grid = null)
        {
            for (int i = 0; i < count; i++) {
                TrackWrapObject wrapObject = new TrackWrapObject(object3d, start + i * step, wrapMode, left, startOutz, scale);
                AddObject(wrapObject, colorTexture, cubeTexture, normalTexture, fresnel, castShadow, castTransparent, receiveShadow, wrapObject.Transformation, grid);
            }
        }
        */


        public void WrapCubicalGlow(bool left, ObjLoaderObject3D object3d, int start, int count, int step, int colorTexture, int wrapMode, float scale, bool startOutz = false, CollisionGrid grid = null)
        {
            for (int i = 0; i < count; i++)
            {
                TrackWrapObject wrapObject = new TrackWrapObject(object3d, start + i * step, wrapMode, left, startOutz, scale);
                AddGlowObject(wrapObject, colorTexture, wrapObject.Transformation, grid);
            }
        }

        public void WrapCubicalGlowCycle(bool left, ObjLoaderObject3D object3d, int start, int count, int step, int colorTexture, int wrapMode, float scale, float speed, bool startOutz = false, CollisionGrid grid = null)
        {
            for (int i = 0; i < count; i++)
            {
                TrackWrapObject wrapObject = new TrackWrapObject(object3d, start + i * step, wrapMode, left, startOutz, scale);
                AddGlowCycleObject(wrapObject, colorTexture, speed, wrapObject.Transformation, grid);
            }
        }

        public void WrapCubicalGlowCycleMask(bool left, ObjLoaderObject3D object3d, int start, int count, int step, int colorTexture, int wrapMode, float scale, float speed, bool startOutz = false, CollisionGrid grid = null)
        {
            for (int i = 0; i < count; i++)
            {
                TrackWrapObject wrapObject = new TrackWrapObject(object3d, start + i * step, wrapMode, left, startOutz, scale);
                AddGlowCycleMaskObject(wrapObject, colorTexture, speed, wrapObject.Transformation, grid);
            }
        }



        // pfeiler helper :)
        public void AddTrackPost(float bottom, int pos, int colorTexture, int cubeTexture, ObjLoaderObject3D post, ObjLoaderObject3D postBottom, ObjLoaderObject3D postTop)
        {
            DeferredMaterialSettings settings = new DeferredMaterialSettings();
            settings.colorTexture = colorTexture;

            octree.AddEntity(new OctreeEntity(postTop, TrackMaterials.world, settings, TrackAlignment.MidAlign(pos) * Matrix4.CreateTranslation(0, -0.5f, 0)));
            octree.AddEntity(new OctreeEntity(post, TrackMaterials.world, settings, TrackAlignment.MidStraightDownAlign(pos) * Matrix4.CreateTranslation(0, -1.5f, 0)));
            octree.AddEntity(new OctreeEntity(postBottom, TrackMaterials.world, settings, TrackAlignment.MidDownBootmPosAlign(pos, bottom)));
        }

        public void AddObject(BaseObject3D object3d, int colorTexture, int normalTexture, int additionalTexture, bool castShadow, bool receiveShadow, Matrix4 transform, CollisionGrid grid = null)
        {
            DeferredMaterialSettings settings = new DeferredMaterialSettings();
            settings.colorTexture = colorTexture;
            settings.normalTexture = normalTexture;
            settings.additionalTexture = additionalTexture;

            octree.AddEntity(new OctreeEntity(object3d, TrackMaterials.world, settings, transform));
           
            if (castShadow)
            {
                shadowOctree.AddEntity(new OctreeEntity(object3d, TrackMaterials.world, castMaterialSettings, transform));
            }

            if (grid != null)
            {
                object3d.AddToCollisionContainer(grid, transform, 3);
            }
        }


        public void AddGlowObject(BaseObject3D object3d, int colorTexture, Matrix4 transform, CollisionGrid grid = null)
        {
            DeferredMaterialSettings settings = new DeferredMaterialSettings();
            settings.colorTexture = colorTexture;

            octree.AddEntity(new OctreeEntity(object3d, TrackMaterials.world, settings, transform));

            if (grid != null)
            {
                object3d.AddToCollisionContainer(grid, transform, 3);
            }

        }

        public void AddGlowCycleObject(BaseObject3D object3d, int colorTexture, float speed, Matrix4 transform, CollisionGrid grid = null)
        {
            DeferredMaterialSettings settings = new DeferredMaterialSettings();
            settings.colorTexture = colorTexture;
            settings.speed = speed;

            octree.AddEntity(new OctreeEntity(object3d, TrackMaterials.world, settings, transform));

            if (grid != null)
            {
                object3d.AddToCollisionContainer(grid, transform, 3);
            }

        }

 
        public void AddGlowCycleMaskObject(BaseObject3D object3d, int colorTexture, float speed, Matrix4 transform, CollisionGrid grid = null)
        {
            DeferredMaterialSettings settings = new DeferredMaterialSettings();
            settings.colorTexture = colorTexture;
            settings.speed = speed;

            octree.AddEntity(new OctreeEntity(object3d, TrackMaterials.world, settings, transform));

            if (grid != null)
            {
                object3d.AddToCollisionContainer(grid, transform, 3);
            }
        }
        

        public virtual void LoadAndGenerate() { }
        public virtual void DrawShadows() { }
        public virtual void DrawLights() { }
        public virtual void Draw(float speedValue) { }

        public virtual void DrawPostprocessing(float speedValue) { }

    }
}
