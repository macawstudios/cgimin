﻿using OpenTK;
using lv1.track;
using System.Collections.Generic;
using Engine.cgimin.collision;
using Engine.cgimin.texture;
using Engine.cgimin.octree;
using Engine.cgimin.material;
using Engine.cgimin.object3d;
using Engine.cgimin.light;
using Engine.cgimin.camera;
using OpenTK.Graphics.OpenGL;
using Engine.cgimin.helpers;
using System;

namespace lv1.courses
{
    public class FirstCourse : CourseBase
    {
        private int skyCubeReflectionID;

        public FirstCourse()
        {
            
        }

        public override void LoadAndGenerate()
        {
            // Light
            Light.SetDirectionalLight(new Vector3(0.5f, 0.5f, 0.5f), new Vector4(0.15f, 0.15f, 0.15f, 1), new Vector4(0.9f, 0.9f, 0.9f, 0.0f), new Vector4(0.0f, 0.0f, 0.0f, 0.0f));

            TimeMeasure.Start();
            // Textures
            List<string> textures = new List<string>();

            textures.Add("data/textures/skybox/Sky_Day Sun High SummerSky_Small_RT.png");
            textures.Add("data/textures/skybox/Sky_Day Sun High SummerSky_Small_LF.png");
            textures.Add("data/textures/skybox/Sky_Day Sun High SummerSky_Small_UP.png");
            textures.Add("data/textures/skybox/Sky_Day Sun High SummerSky_Small_DN.png");
            textures.Add("data/textures/skybox/Sky_Day Sun High SummerSky_Small_FR.png");
            textures.Add("data/textures/skybox/Sky_Day Sun High SummerSky_Small_BK.png");

            skyCubeReflectionID = TextureManager.LoadCubemap(textures);
            //CourseBase.Cubemap = skyCubeReflectionID;

            streetColor1 = TextureManager.LoadTexture("../Courses/Desert1/street_color.png");
            streetNormal1 = TextureManager.LoadTexture("../Courses/Desert1/street_normal.tif");

            streetColor2 = TextureManager.LoadTexture("../Courses/Desert1/grid_color.png");
            streetNormal2 = TextureManager.LoadTexture("../Courses/Desert1/grid_normals.tif");

            sideColor1 = TextureManager.LoadTexture("../Courses/Desert1/side_color_desert_1.png");
            sideNormal1 = TextureManager.LoadTexture("../Courses/Desert1/side_normals.tif");

            brakeColor = TextureManager.LoadTexture("../Courses/Desert1/brake_color.png");
            brakeNormal = TextureManager.LoadTexture("../Courses/Desert1/brake_normals.tif");

            TimeMeasure.Show("Track Textures:");

            TimeMeasure.Start();
            Octree.MaxIterationDepth = 7;
            octree = new Octree(new Vector3(-1000, -1000, -1000), new Vector3(1000, 1000, 1000));
            shadowOctree = new Octree(new Vector3(-1000, -1000, -1000), new Vector3(1000, 1000, 1000));

            trackParts = new List<TrackPart>();

            float width = 15.0f;
            int cornerLength = 125;

            TrackPart.SetBaseHeight(47);
            TrackPart.SetStartRotation(0);
            TrackPart.SetStartWidth(width);
            TrackPart.SetBrakeFieldTexture(brakeColor, brakeNormal);

            /*
            collisionGrid = new CollisionGrid(100, 100, 100);
            TrackPart.SetDisplayOptions(0.7f, streetColor1, streetNormal1, 15, new bool[] { false, false, false, false, false, false }, TrackPart.BorderMode.border, TrackPart.BorderMode.border);

            // 1
            trackParts.Add(new TrackPart(collisionGrid, 0, 0, 0, width, 150, true));

            // 2
            trackParts.Add(new TrackPart(collisionGrid, -90, 0, 0, width, cornerLength, true ));

            // 3
            TrackPart.SetDisplayOptions(1.7f, streetColor2, streetNormal2, TrackPart.TextureRepeat.six, 4, new bool[] { false, false, false, false, false, false }, TrackPart.BorderMode.border, TrackPart.BorderMode.border, true);
            trackParts.Add(new TrackPart(collisionGrid, -90, 0, 0, width, 50, true, true));

            // 4
            trackParts.Add(new TrackPart(collisionGrid, -90, 0, -90, width, 200, true, false));

            // 5
            trackParts.Add(new TrackPart(collisionGrid, 0, 0, -90, width, cornerLength, true, false));

            // 6
            trackParts.Add(new TrackPart(collisionGrid, 0, 0, -90, width, 150, true, false));

            // 7
            trackParts.Add(new TrackPart(collisionGrid, 90, 0, -90, width, cornerLength, true, false));

            // 8
            trackParts.Add(new TrackPart(collisionGrid, 90, 0, 0, width, 200, true, false, true));

            // 9
            TrackPart.SetDisplayOptions(0.7f, 4, sideColor1, sideNormal1, streetColor1, streetNormal1, TrackPart.TextureRepeat.one, 15, new bool[] { false, false, false, false, false, false }, TrackPart.BorderMode.border, TrackPart.BorderMode.border, true);
            trackParts.Add(new TrackPart(collisionGrid, 90, 0, 0, width, 300, true, true));

            // 10
            trackParts.Add(new TrackPart(collisionGrid, 180, 0, 0, width, cornerLength, true, false));

            // 11
            //trackParts.Add(new TrackPart(collisionGrid, 180, 0, 0, width, 750, true, false));
            trackParts.Add(new TrackPart(collisionGrid, 180, 0, 0, 18, 50, true, false));
            TrackPart.SetDisplayOptions(0.7f, 4, sideColor1, sideNormal1, streetColor1, streetNormal1, TrackPart.TextureRepeat.one, 15, new bool[] { true, false, false, false, false, true }, TrackPart.BorderMode.border, TrackPart.BorderMode.border, true);
            trackParts.Add(new TrackPart(collisionGrid, 180, 0, 0, 18, 100, true, false));
            TrackPart.SetDisplayOptions(0.7f, 4, sideColor1, sideNormal1, streetColor1, streetNormal1, TrackPart.TextureRepeat.one, 15, new bool[] { false, false, false, false, false, false }, TrackPart.BorderMode.border, TrackPart.BorderMode.border, true);
            trackParts.Add(new TrackPart(collisionGrid, 180, 0, 0, 18, 50, true, false));
            TrackPart.SetDisplayOptions(0.7f, 4, sideColor1, sideNormal1, streetColor1, streetNormal1, TrackPart.TextureRepeat.one, 15, new bool[] { true, true, true, false, false, false }, TrackPart.BorderMode.border, TrackPart.BorderMode.border, true);
            trackParts.Add(new TrackPart(collisionGrid, 180, 0, 0, 18, 100, true, false));
            TrackPart.SetDisplayOptions(0.7f, 4, sideColor1, sideNormal1, streetColor1, streetNormal1, TrackPart.TextureRepeat.one, 15, new bool[] { false, false, false, false, false, false }, TrackPart.BorderMode.border, TrackPart.BorderMode.border, true);
            trackParts.Add(new TrackPart(collisionGrid, 180, 0, 0, 18, 50, true, false));
            TrackPart.SetDisplayOptions(0.7f, 4, sideColor1, sideNormal1, streetColor1, streetNormal1, TrackPart.TextureRepeat.one, 15, new bool[] { false, false, false, true, true, true }, TrackPart.BorderMode.border, TrackPart.BorderMode.border, true);
            trackParts.Add(new TrackPart(collisionGrid, 180, 0, 0, 18, 100, true, false));
            TrackPart.SetDisplayOptions(0.7f, 4, sideColor1, sideNormal1, streetColor1, streetNormal1, TrackPart.TextureRepeat.one, 15, new bool[] { false, false, false, false, false, false }, TrackPart.BorderMode.border, TrackPart.BorderMode.border, true);
            trackParts.Add(new TrackPart(collisionGrid, 180, 0, 0, width, 50, true, false));

            trackParts.Add(new TrackPart(collisionGrid, 180, 0, 0, width, 250, true, false));

            // 12
            trackParts.Add(new TrackPart(collisionGrid, 270, 0, -20, width, cornerLength, true, false));

            // 13
            trackParts.Add(new TrackPart(collisionGrid, 270, 0, -20, width, 100, true, false));

            // 14
            trackParts.Add(new TrackPart(collisionGrid, 360, 0,  0, width, cornerLength, true, false));

            // 15
            trackParts.Add(new TrackPart(collisionGrid, 360 , 0, 0, width, 100, true, false, false, -1, true));

            TimeMeasure.Show("Trackpart generation:");

            // jumps, accelerators
            TrackPart.AddAccelerator(475 + cornerLength * 2, 0.25f);
            TrackPart.AddAccelerator(900 + cornerLength * 3, 0.75f);

            TrackPart.AddJump(1225 + cornerLength * 4, 0.25f);
            TrackPart.AddJump(1375 + cornerLength * 4, 0.75f);


            TrackSceneFactory.Init();
            TrackSceneFactory.AddOption(new TrackSceneFactory.TrackSceneObjectOptions() { objectName = "breiter_pfeiler", fresnelValue = 1.0f, castShadow = true  });
            TrackSceneFactory.AddOption(new TrackSceneFactory.TrackSceneObjectOptions() { objectName = "sail_side", fresnelValue = 1.0f, castShadow = true });
            TrackSceneFactory.AddOption(new TrackSceneFactory.TrackSceneObjectOptions() { objectName = "goal", fresnelValue = 1.0f, castShadow = true });
            TrackSceneFactory.AddOption(new TrackSceneFactory.TrackSceneObjectOptions() { objectName = "goal_light_panel", castShadow = true, material = TrackMaterials.worldGlow, postProcessing = true});
            TrackSceneFactory.AddOption(new TrackSceneFactory.TrackSceneObjectOptions() { objectName = "goal_cycle_light", castShadow = true, material = TrackMaterials.worldGlowCycle, speed = 0.01f, postProcessing = true });
            TrackSceneFactory.AddOption(new TrackSceneFactory.TrackSceneObjectOptions() { objectName = "glow_tower_glow", castShadow = false, material = TrackMaterials.worldGlow, postProcessing = true });

            TrackSceneFactory.Build("Desert1", skyCubeReflectionID, octree, shadowOctree, collisionGrid);

            // desert lightmarker
            ObjLoaderObject3D lightOuterObject = new ObjLoaderObject3D("../Courses/Desert1/desert_lightmarker_outer.obj", 1.0f, false, false);
            ObjLoaderObject3D lightInnerObject = new ObjLoaderObject3D("../Courses/Desert1/desert_lightmarker_inner.obj", 1.0f, false, false);
            int lightmarkerColor = TextureManager.LoadTexture("../Courses/Desert1/lightmarker.png", false);
            int lightmarkerNormal = TextureManager.LoadTexture("../Courses/Desert1/lightmarker_normals.tif", false);
            WrapCubical(true, lightOuterObject, 1400, 5, 110, lightmarkerColor, skyCubeReflectionID, lightmarkerNormal, 0.7f, false, false, false, TrackWrapObject.WRAPMODE_SCALE_SIZE, 1.0f, true);
            WrapCubicalGlowCycleMask(true, lightInnerObject, 1400, 5, 110, lightmarkerColor, TrackWrapObject.WRAPMODE_SCALE_SIZE, 1.0f, 0.01f, true);


            ObjLoaderObject3D lightPostOuterObject = new ObjLoaderObject3D("../Courses/Desert1/side_light_outer.obj", 3.0f, false);
            ObjLoaderObject3D lightPostInnerObject = new ObjLoaderObject3D("../Courses/Desert1/side_light_inner.obj", 3.0f, false);
            int sideLightColor = TextureManager.LoadTexture("../Courses/Desert1/side_light_color.png", false);
            int sideLightNormal = TextureManager.LoadTexture("../Courses/Desert1/side_light_normal.tif", false);

            /*
            for (int i = 0; i < 100; i++) {
                AddObject(lightPostOuterObject, sideLightColor, skyCubeReflectionID, sideLightNormal, 0.7f, true, false, false, TrackAlignment.LeftAlign(i * 20 + 150, (float)Math.PI));
                AddObject(lightPostOuterObject, sideLightColor, skyCubeReflectionID, sideLightNormal, 0.7f, true, false, false, TrackAlignment.RightAlign(i * 20 + 150));
                AddGlowObject(lightPostInnerObject, sideLightColor, TrackAlignment.LeftAlign(i * 20 + 150, (float)Math.PI));
                AddGlowObject(lightPostInnerObject, sideLightColor, TrackAlignment.RightAlign(i * 20 + 150));
            }
            */
            
            /*
            // rock
            ObjLoaderObject3D rockObject = new ObjLoaderObject3D("../Courses/Desert1/side_light_outer.obj", 10, false);
            int rockColor = TextureManager.LoadTexture("data/desert_course/big_rock_color.png", false);
            int rockNormal = TextureManager.LoadTexture("data/desert_course/big_rock_normal.tif", false);
            for (int i = 0; i < 10; i++)
            {
                AddObject(rockObject, rockColor, skyCubeReflectionID, rockNormal, 0.7f, true, false, false, TrackAlignment.MidAlign(150 + i * 10));
                AddObject(rockObject, rockColor, skyCubeReflectionID, rockNormal, 0.7f, true, false, false, TrackAlignment.MidAlign(100 + i * 10));
            }
            */
   
            //WriteObjFile("desert_track.obj");

            TimeMeasure.Start();
            foreach (TrackPart trackPart in trackParts) trackPart.AddToOctree(octree, skyCubeReflectionID, 1.0f, TrackMaterials.world);
            TimeMeasure.Show("Add TrackParts to octree");

            //foreach (TrackPart trackPart in trackParts) trackPart.AddToOctreeWithShadow(octree, shadowOctree, skyCubeReflectionID, 1.0f, worldCascaded, trackCastShadow);

            TimeMeasure.Start();
            base.ItemsToOctree();
            TimeMeasure.Show("Items to octree :");

            TimeMeasure.Start();
            collisionGrid.FinalizeCollision();
            TimeMeasure.Show("finalize collision");

        }

        public override void DrawShadows()
        {
            shadowOctree.Draw();
        }


        public override void Draw(float speedValue)
        {
            // Camera settings
            //Camera.SetDynamicFovNearFar(60, 0.1f, 1500.0f);
            Camera.SetupFog(30, 1000, new Vector3(0.7f, 0.5f, 0.0f));

            GL.Enable(EnableCap.CullFace);
            GL.CullFace(CullFaceMode.Front);
            octree.Draw();
            GL.Disable(EnableCap.CullFace);
        }


        



    }
}
