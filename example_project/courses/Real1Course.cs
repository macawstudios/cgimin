﻿using OpenTK;
using lv1.track;
using System.Collections.Generic;
using Engine.cgimin.collision;
using Engine.cgimin.texture;
using Engine.cgimin.octree;
using Engine.cgimin.material;
using Engine.cgimin.material.normalmappingfogshadowcascaded;
using Engine.cgimin.material.cubereflectionfogcascaded;
using Engine.cgimin.water;
using Engine.cgimin.object3d;
using Engine.cgimin.camera;
using Engine.cgimin.light;
using OpenTK.Graphics.OpenGL;
using Engine.cgimin.weather;


namespace lv1.courses
{
    public class Real1Course : CourseBase
    {

        private Water water;

        private int waterNormalTextureID;
        private int waterCubeReflectionID;

        private int rainTextureID;

        private CubeReflectionFogCascaded trackCubeReflectionMaterial;

        private Rain rain;

        public Real1Course()
        {
            
        }

        public override void LoadAndGenerate()
        {

            /*
            TrackPart.StartTrackPos = 0;

            Camera.SetupFog(1, 512, new Vector3(68 / 255.0f, 87 / 255.0f, 114 / 255.0f));

            Light.SetDirectionalLight(new Vector3(0.5f, 0.5f, 1), new Vector4(0.1f, 0.1f, 0.1f, 1), new Vector4(0.9f, 0.9f, 0.9f, 0.0f), new Vector4(0.8f, 0.8f, 0.8f, 0.5f));

            water = new Water();
            rain = new Rain();

            Octree.MaxIterationDepth = 6;
            float octSize = 2000;
            octree = new Octree(new Vector3(-octSize, -octSize, -octSize), new Vector3(octSize, octSize, octSize));
            shadowOctree = new Octree(new Vector3(-octSize, -octSize, -octSize), new Vector3(octSize, octSize, octSize));

            streetColor1 = TextureManager.LoadTexture("data/course1/street_color.tif");
            streetNormal1 = TextureManager.LoadTexture("data/course1/street_normal.tif");

            waterNormalTextureID = TextureManager.LoadTexture("data/textures/water_normal.png");

            rainTextureID = TextureManager.LoadTexture("data/course1/rain.png");

            List<string> textures = new List<string>();
            
            textures.Add("data/textures/skybox/Epic_CentreOfTheStormSmall_LF.png");
            textures.Add("data/textures/skybox/Epic_CentreOfTheStormSmall_RT.png");
            textures.Add("data/textures/skybox/Epic_CentreOfTheStormSmall_UP.png");
            textures.Add("data/textures/skybox/Epic_CentreOfTheStormSmall_DN.png");
            textures.Add("data/textures/skybox/Epic_CentreOfTheStormSmall_FR.png");
            textures.Add("data/textures/skybox/Epic_CentreOfTheStormSmall_BK.png");
            
            waterCubeReflectionID = TextureManager.LoadCubemap(textures);

            Cubemap = waterCubeReflectionID;

            trackParts = new List<TrackPart>();

            int bWidth = 15;

            TrackPart.SetBaseHeight(27);
            TrackPart.SetStartRotation(90);
            TrackPart.SetStartWidth(bWidth);

           
            collisionGrid = new CollisionGrid(100, 100, 100);
            TrackPart.SetDisplayOptions(TrackPart.TextureRepeat.one, 15, 0, 5, new bool[] { false, false, false, false, false, false }, TrackPart.BorderMode.noBorder, TrackPart.BorderMode.border);
            trackParts.Add(new TrackPart(collisionGrid, 90, 0, 0, bWidth, 200, true, false));
            trackParts.Add(new TrackPart(collisionGrid, 120, 0, 0, bWidth, 80, true, false));
            trackParts.Add(new TrackPart(collisionGrid, 120, 0, 0, bWidth, 80, true, false));
            trackParts.Add(new TrackPart(collisionGrid, 90, 0, 0, bWidth,  80, true, false));
            trackParts.Add(new TrackPart(collisionGrid, 90, 0, 15, bWidth,  80, true, false));
            trackParts.Add(new TrackPart(collisionGrid, 0, 0, 0, bWidth,   200, true, false));
            TrackPart.SetDisplayOptions(TrackPart.TextureRepeat.one, 15, 0, 3, new bool[] { true, true, false, false, false, false }, TrackPart.BorderMode.border, TrackPart.BorderMode.border);
            trackParts.Add(new TrackPart(collisionGrid, 0, 0, 0, bWidth,   100, true, false));
            TrackPart.SetDisplayOptions(TrackPart.TextureRepeat.one, 15, 0, 3, new bool[] { false, false, false, false, true, true }, TrackPart.BorderMode.noBorder, TrackPart.BorderMode.border);
            trackParts.Add(new TrackPart(collisionGrid, 0, 0, 0, bWidth,   100, true, false));

            TrackPart.SetDisplayOptions(TrackPart.TextureRepeat.one, 15, 0, 3, new bool[] { false, false, false, false, false, false }, TrackPart.BorderMode.noBorder, TrackPart.BorderMode.border);
            trackParts.Add(new TrackPart(collisionGrid, 90, 0, 0, bWidth,  200, true, false));
            trackParts.Add(new TrackPart(collisionGrid, 90, 30, 0, bWidth,   40, true, false));
            trackParts.Add(new TrackPart(collisionGrid, 180, -30, -15, bWidth,  200, true, false));
            trackParts.Add(new TrackPart(collisionGrid, 180, 0, 0, bWidth,  500, true, false, false, 10));
            trackParts.Add(new TrackPart(collisionGrid, 270, 0, -15, bWidth,  200, true, false));
            trackParts.Add(new TrackPart(collisionGrid, 270, 0, 0, bWidth,  300, true, false));
            trackParts.Add(new TrackPart(collisionGrid, 200, 30, 5, bWidth,  200, true, false));
            trackParts.Add(new TrackPart(collisionGrid, 270, 0, -5, bWidth,  200, true, false));
            trackParts.Add(new TrackPart(collisionGrid, 270, 0, 0, bWidth,  200, true, false));
            trackParts.Add(new TrackPart(collisionGrid, 360, 30, 10, bWidth,  160, true, false));
            trackParts.Add(new TrackPart(collisionGrid, 360, 0, 0, bWidth,  440, true, false));
            trackParts.Add(new TrackPart(collisionGrid, 360 + 90, 0, 0, bWidth, 80, true, false));
            trackParts.Add(new TrackPart(collisionGrid, 360 + 90, 0, 0, bWidth, 10, true, false));
            trackParts.Add(new TrackPart(collisionGrid, 360 + 90, 0, 0, bWidth, 20, true, false, false, -1, true));

            // glow fence
            InitGlowFence(waterCubeReflectionID, 8, 4, -0.001f, "data/course1/glow_fence_planke.obj", "data/course1/glow_fence_pfeiler.obj", "data/course1/glow_fence.png");
            GlowFenceLeft(0, 50);
            GlowFenceRight(10, 50);

            // side elements
            ObjLoaderObject3D sideBridge = new ObjLoaderObject3D("data/course1/sidebridge.obj", 1, false, false, false);
            ObjLoaderObject3D sideBridgeMirror = new ObjLoaderObject3D("data/course1/sidebridge.obj", 1, false, false, true);
            WrapCubical(true, sideBridge, 400, 30, 20, streetColor1ID, waterCubeReflectionID, TrackWrapObject.WRAPMODE_CONST_YUP, 1.0f, 0.1f);
            WrapCubical(true, sideBridgeMirror, 450, 30, 20, streetColor1ID, waterCubeReflectionID, TrackWrapObject.WRAPMODE_CONST_YUP, 1.0f, 0.1f);

            // pfeiler
            ObjLoaderObject3D pfeilerBottom = new ObjLoaderObject3D("data/course1/pfeiler_boden.obj", 1, false, true);
            ObjLoaderObject3D pfeilerOnTrack = new ObjLoaderObject3D("data/course1/pfeiler_track.obj", 1, false, true);
            ObjLoaderObject3D pfeiler = new ObjLoaderObject3D("data/course1/pfeiler.obj", 1, false, true);

            for (int i = 0; i < 52; i++) {
                AddTrackPost(8, i * 70, streetColor1ID, waterCubeReflectionID, pfeiler, pfeilerBottom, pfeilerOnTrack);
            }

            // side wall
            ObjLoaderObject3D sideWall = new ObjLoaderObject3D("data/course1/wall_new.obj", 1, false, false, false);
            WrapCubical(true, sideWall, 1700, 150, 6, streetColor1ID, waterCubeReflectionID, TrackWrapObject.WRAPMODE_CONST_YUP, 1.0f, 0.1f);
            WrapCubical(false, sideWall, 1700, 150, 6, streetColor1ID, waterCubeReflectionID, TrackWrapObject.WRAPMODE_CONST_YUP, 1.0f, 0.1f);

            // accelerators
            TrackPart.AddAccelerator(1050, 0.25f);
            TrackPart.AddAccelerator(2050, 0.25f);
            TrackPart.AddAccelerator(3050, 0.25f);

            // jumps
            TrackPart.AddJump(100, 0.5f);


            // cylinders
            TrackPart.AddSmallCylinder(1206, 0.75f);
            TrackPart.AddSmallCylinder(1203, 0.5f);
            TrackPart.AddSmallCylinder(1200, 0.25f);

            TrackPart.AddSmallCylinder(1600, 0.75f);
            TrackPart.AddSmallCylinder(1600, 0.5f);
            TrackPart.AddSmallCylinder(1600, 0.25f);

            TrackPart.AddSmallCylinder(1800, 0.25f);
            TrackPart.AddSmallCylinder(1803, 0.5f);
            TrackPart.AddSmallCylinder(1806, 0.75f);


            collisionGrid.FinalizeCollision();


            trackSpecularMaterial = new NormalMappingMaterialFogShadowCascaded();
            trackCubeReflectionMaterial = new CubeReflectionFogCascaded();


            MaterialSettings trackMaterialSettings = new MaterialSettings();
            trackMaterialSettings.colorTexture = streetColor1ID;
            trackMaterialSettings.normalTexture = streetNormal1ID;
            trackMaterialSettings.cubeTexture = waterCubeReflectionID;
            trackMaterialSettings.fresnelValue = 0.5f;
            foreach (TrackPart trackPart in trackParts) octree.AddEntity(new OctreeEntity(trackPart, trackCubeReflectionMaterial, trackMaterialSettings, trackPart.Transformation));

            base.ItemsToOctree();

            */
        }

        public override void DrawShadows()
        {
            shadowOctree.Draw();
        }


        public override void Draw(float speedValue)
        {
            water.Draw(10, waterNormalTextureID, waterCubeReflectionID, 10, 10, 10,  100, 100, 200, 0.05f, 0.08f, 1.0f, 0.01f, 0.001f, 0.9f);
            
            GL.Enable(EnableCap.CullFace);
            octree.Draw();
            GL.Disable(EnableCap.CullFace);

            Matrix4 explTransform = Matrix4.Identity;
            explTransform *= Matrix4.CreateTranslation(0, 30, 0);

        }




    }
}
