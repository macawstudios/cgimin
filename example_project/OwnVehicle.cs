﻿using OpenTK;
using Engine.cgimin.material.cubereflectionfogcascaded;
using Engine.cgimin.helpers;
using Engine.cgimin.collision;
using lv1.track;
using System;
using lv1.courses;
using lv1.vehicle;
using System.Collections.Generic;
using lv1.scenes;

namespace lv1
{
    internal class OwnVehicle
    {

        // config vars
        internal static float VELO_FORCE = 5.1f;
        internal static float VELO_SMOOTHOUT = 0.8f;
        internal static float SPEED_CLASS_MUL = 1.0f;

        /* possible config
        internal const float ROTATE_SPEED = 0.05f;
        internal const float MAX_ROTATION = 1.24f;
        internal const float rBackIntens = 1.0f;
        */

        internal const float ROTATE_SPEED = 0.1f;
        internal const float MAX_ROTATION = 1.24f;
        internal const float rBackIntens = 0.75f;

        // fov camera
        public float Fov { get; private set; }

        // acceleration values
        private const float accelerationBoost = 0.05f;
        private const float acceleration = 0.03f;
        private const float deAcceleration = 0.976f;

        // calculated maxSpeed values
        private float maxAccelerationSpeed;

        // Schwebedistanz zum Boden
        private const float vehicleFloatDistance = 0.1f;
        private Vehicle vehicleObject;
        private CollisionGrid collisionContainer;

        // Objekt Position und front, up, left für die Drehung
        internal Vector3 position;
        internal Vector3 front;
        internal Vector3 left;
        internal Vector3 up;

        // Getter für die kamera
        public Vector3 CameraEye { get; private set; }
        public Vector3 CameraTarget { get; private set; }
        public Vector3 CameraUp { get; private set; }
        
        // Fahrlogic, Geschwindigkeit 
        private float rotateValue;
        public float speed;
        private float strideValue;
        private int boostCounter;

        // Abprallen
        private Vector3 bounce;

        // Gravitation
        private bool inAir;
        private float fallingSpeed;

        // On Brake field
        private bool onBrakeField;

        // Animierte states
        private float wipeValueAnimtion;
        private float scewValueAnimation;

        private bool lastScewLeftRight;
        private int rollAnimation;
        private bool rollLeftRight;

        // Update count
        private int update;

        // start counter
        private int startCounter = 120;

        // animation Matrix;
        private Matrix4 animationMatrix1;
        private Matrix4 animationMatrix2;
        private float sideUpAnimation;

        // explosion
        private int explodeCounter;
        private Vector3 explodeCamStart;
        private Vector3 explodeCamEnd;
        private int uncollideBlink;

        // up / down velocity
        private float upDownValue;

        // nearest track position
        public int nearestTrackIndex { get; private set; }

        // energy / health
        private float energy;
        private int subEnergyWait;

        // button logic
        bool turboButton;

        // the current round
        private int currentIntRound;
        private bool roundD1;
        private bool roundD2;

        public static OwnVehicle Instance;

        private List<CameraPreset> cameraPresets = new List<CameraPreset> { new NearCameraPreset(), new FarCameraPreset(), new FPSCameraPreset() };
        internal CameraPreset currentCameraPreset;
        private float currentCrashCamIntensity;
        private float crashCamSmoothing;
        private int crashCamFrameCount;

        // light throttle
        private int lightThrottle;

        //non-public constructor for deserialization. Otherwise JsonDeserializer would call the normal constructor with 0 as int values, causeing division by zero errors.
        private OwnVehicle()
        {            
        }

        public OwnVehicle(CollisionGrid collisionGrid, int startID, int columns)
        {
            update = 0;
            upDownValue = 0;
            lightThrottle = 0;

            float testSpeed = 0;
            for (int i = 0; i < 200; i++) {
                testSpeed += accelerationBoost;
                testSpeed *= deAcceleration;
            }
            maxAccelerationSpeed = testSpeed;            

            vehicleObject = new Vehicle(CourseBase.deferredScreneDescription.SpecularIBL);
            collisionContainer = collisionGrid;
                    
            position = TrackPart.GetPositionFromStartID(startID, columns);
            nearestTrackIndex = TrackPart.GetNearestTrackIndex(position);

            float trackPos = TrackPart.GetTrackPosFromStartID(startID, columns);
            float relX = TrackPart.GetRelXFromStartID(startID, columns);

            front = TrackPart.GetDirectFront(trackPos, trackPos + 1, relX);
            up = TrackPart.GetUp(trackPos, relX);
            left = (Vector3.Cross(up, front)).Normalized();

            animationMatrix1 = Matrix4.Identity;
            animationMatrix2 = Matrix4.Identity;

            rotateValue = 0;
            speed = 0;
            fallingSpeed = 0;
            inAir = false;
            boostCounter = 0;
            strideValue = 0;
            uncollideBlink = 0;
            explodeCounter = 0;
            subEnergyWait = 0;
            currentIntRound = 0;
            if (nearestTrackIndex < TrackPart.StartTrackPos) currentIntRound = 1;
            turboButton = false;
            roundD1 = false;
            roundD2 = false;
            int oneThirdCmp = (int)(TrackPart.TrackInfo.Count * 0.33f);
            if (nearestTrackIndex >= oneThirdCmp) roundD1 = true;
            int twoThirdCmp = (int)(TrackPart.TrackInfo.Count * 0.66f);
            if (nearestTrackIndex >= twoThirdCmp) roundD2 = true;

            energy = 100.0f;

            bounce = new Vector3();

            currentCameraPreset = cameraPresets[0];

            Instance = this;

            Fov = 60;
        }


        public float GetEnergy()
        {
            return energy;
        }

        private void subEnergy(float amount)
        {
            if (subEnergyWait > 0) return;
            subEnergyWait = 10;
            energy -= amount;
            if (energy <= 0) {
                energy = 100;
                DoExplodeAnimation();
            }
        }

        public void SetPosition(float x, float y, float z)
        {
            position.X = x;
            position.Y = y;
            position.Z = z;

            CameraEye = position - front * 2.0f + up * 0.5f;
            CameraTarget = position + up * 0.4f;
            CameraUp = up;
        }

        public void CycleCameraPresets()
        {
            int currentIndex = cameraPresets.IndexOf(currentCameraPreset);
            int newIndex = (currentIndex + 1) % cameraPresets.Count;
            currentCameraPreset = cameraPresets[newIndex];
        }

        public void DoExplodeAnimation() {
            vehicleObject.Explode();
            explodeCounter = 300;
            explodeCamStart = position + up * 1.0f + (TrackPart.TrackInfo[nearestTrackIndex].midPosition - position).Normalized() * 15;
            explodeCamEnd = position + up * 1.0f + (TrackPart.TrackInfo[(nearestTrackIndex + 20) % TrackPart.TrackInfo.Count].midPosition - position).Normalized() * 15;
        }


        private void setOnTrackAfterFail()
        {
            position = TrackPart.TrackInfo[nearestTrackIndex].midPosition;
            front = (TrackPart.TrackInfo[nearestTrackIndex + 1].midPosition - TrackPart.TrackInfo[nearestTrackIndex].midPosition).Normalized();
            left = (TrackPart.TrackInfo[nearestTrackIndex].leftBorderFront - TrackPart.TrackInfo[nearestTrackIndex].rightBorderFront).Normalized();
            speed = 0;
            fallingSpeed = 0;
            bounce = new Vector3(0, 0, 0);
            uncollideBlink = 100;
        }

        public void Update(bool accelerate, float leftRight, bool wipeLeft, bool wipeRight, bool rollLeft, bool rollRight, bool lookBackwards, bool turbo)
        {
            // sub energy
            if (subEnergyWait > 0) subEnergyWait--;

            // red blink when energy is low
            vehicleObject.SetRedBlink(energy < 20);

            // explosion counter logic
            if (explodeCounter > 0)
            {
                vehicleObject.UpdateExplosion();
                vehicleObject.CreatePartTransforms(0, 0);
                explodeCounter--;

                CameraEye = (explodeCamStart - explodeCamEnd) / 300 * explodeCounter + explodeCamEnd;
                CameraTarget = position + (float)Math.Sin(explodeCounter * 0.4f) * Math.Max((explodeCounter - 230) / 530.0f, 0) * up;
                CameraUp = Vector3.UnitY;

                if (explodeCounter == 0)
                {
                    position = TrackPart.TrackInfo[nearestTrackIndex].midPosition;
                    front = (TrackPart.TrackInfo[nearestTrackIndex + 1].midPosition - TrackPart.TrackInfo[nearestTrackIndex].midPosition).Normalized();
                    left = (TrackPart.TrackInfo[nearestTrackIndex].leftBorderFront - TrackPart.TrackInfo[nearestTrackIndex].rightBorderFront).Normalized();
                    speed = 0;
                    bounce = new Vector3(0, 0, 0);
                    uncollideBlink = 100;
                }
                else
                {
                    return;
                }
            }

            // manual turbo
            if (turbo && turboButton == false && energy > 22 && boostCounter == 0)
            {
                turboButton = true;
                boostCounter = 120;
                speed = maxAccelerationSpeed;
                subEnergy(17);
            }
            if (!turbo) turboButton = false;

            // light throttle
            if (accelerate)
            {
                if (lightThrottle < 10) lightThrottle++;
                if (lightThrottle > 10) lightThrottle--;
            }
            if (lightThrottle > 0 && !accelerate) lightThrottle--;
            if (boostCounter > 0) lightThrottle = 20;
            

            // uncollide blink
            if (uncollideBlink > 0) uncollideBlink--;

            // check also for round update, round logic
            int oldTrackPos = nearestTrackIndex;
            nearestTrackIndex = TrackPart.GetNearestTrackIndex(position);
            int oneThirdCmp = (int)(TrackPart.TrackInfo.Count * 0.33f);
            if (oldTrackPos < oneThirdCmp && nearestTrackIndex >= oneThirdCmp) roundD1 = true;
            int twoThirdCmp = (int)(TrackPart.TrackInfo.Count * 0.66f);
            if (oldTrackPos < twoThirdCmp && nearestTrackIndex >= twoThirdCmp) roundD2 = true;
            if (oldTrackPos - TrackPart.TrackInfo.Count / 2 > nearestTrackIndex && roundD1 && roundD2)
            {
                roundD1 = false;
                roundD2 = false;
                currentIntRound++;
            }

            
            // check if too far outside
            if ((TrackPart.TrackInfo[nearestTrackIndex].midPosition - position).LengthSquared > 30 * 30)
            {
                setOnTrackAfterFail();
            }
            

            // start counter logic
            if (startCounter > 0) startCounter--;
            if (startCounter > 0)
            {
                accelerate = false; leftRight = 0; wipeRight = false; rollLeft = false; rollRight = false;
            }

            // bounce
            bounce *= 0.97f;
            if (bounce.Length < 0.01) bounce *= 0;

            // Beschleunigung
            if (boostCounter == 0)
            {
                if (accelerate) speed += acceleration;

                if (Fov > 60) Fov -= 1;
                if (Fov < 60) Fov = 60;
            }
            else
            {
                if (accelerate) speed += accelerationBoost;

                if (Fov < 70) Fov += 1;
                if (Fov > 70) Fov = 70;
            }
            if (boostCounter > 0) boostCounter--;
            speed *= deAcceleration;

            // alte Position wird gespeichert
            Vector3 oldPositon = position;

            // Position wird entsprechend dem speed, in richtung nach vorne (front) verschoben
            position += front * speed * SPEED_CLASS_MUL;
            position += bounce;
            position += left * strideValue;
            position.Y -= fallingSpeed;

            // Prüfen, ob das Fahrzeug auf dem Boden ist
            inAir = true;

            Collision.CollisionReturn colRet = Collision.Linecast(collisionContainer, position + up, position - up);
            onBrakeField = false;

            if (wipeRight) DoExplodeAnimation();

            if (colRet.doesCollide)
            {
                // Fahrzeug ist am Boden
                inAir = false;
                if (fallingSpeed > 0.05)
                {
                    currentCrashCamIntensity = currentCameraPreset.CrashCamIntensity;
                    crashCamSmoothing = currentCameraPreset.CrashCamSmoothing;
                    crashCamFrameCount = 0;                    
                }
                fallingSpeed = 0;

                if (colRet.collisionID == 1) onBrakeField = true;

                if (colRet.collisionID < 2)
                {
                    position = colRet.position + colRet.normal * vehicleFloatDistance;
                    up = colRet.normal;
                }

                // normale genauer über weitere Raycasts
                bool exactFound = false;
                for (int i = 0; i < 2; i++)
                {
                    float lr = 0.3f + i * 0.4f;
                    float fr = 0.3f + i * 0.4f;
                    Collision.CollisionReturn ray1Ret = Collision.Linecast(collisionContainer, position + front * fr + up, position + front * fr - up);
                    Collision.CollisionReturn ray2Ret = Collision.Linecast(collisionContainer, position - front * fr + up, position - front * fr - up);
                    Collision.CollisionReturn ray3Ret = Collision.Linecast(collisionContainer, position + left * lr + up, position + left * lr - up);
                    Collision.CollisionReturn ray4Ret = Collision.Linecast(collisionContainer, position - left * lr + up, position - left * lr - up);

                    if (!exactFound) {
                        if (ray1Ret.doesCollide && ray1Ret.collisionID < 2) {
                            up = ray1Ret.normal;
                            position = ray1Ret.position - front * fr + up * vehicleFloatDistance;
                        }
                        if (ray2Ret.doesCollide && ray2Ret.collisionID < 2)
                        {
                            up = ray2Ret.normal;
                            position = ray2Ret.position + front * fr + up * vehicleFloatDistance;
                        }
                        if (ray3Ret.doesCollide && ray3Ret.collisionID < 2)
                        {
                            up = ray3Ret.normal;
                            position = ray3Ret.position - left * fr + up * vehicleFloatDistance;
                        }
                        if (ray4Ret.doesCollide && ray4Ret.collisionID < 2)
                        {
                            up = ray4Ret.normal;
                            position = ray4Ret.position + left * fr + up * vehicleFloatDistance;
                        }
                    }

                    if (ray1Ret.doesCollide && ray2Ret.doesCollide && ray3Ret.doesCollide && ray4Ret.doesCollide &&
                        ray1Ret.collisionID < 2 && ray2Ret.collisionID < 2 && ray3Ret.collisionID < 2 && ray4Ret.collisionID < 2)
                    {
                        up = ((ray1Ret.normal + ray2Ret.normal + ray3Ret.normal + ray4Ret.normal) / 4.0f).Normalized();
                        position = (ray1Ret.position + ray2Ret.position + ray3Ret.position + ray4Ret.position) / 4.0f + up * vehicleFloatDistance;
                        exactFound = true;
                    }
                }
            }

            // speed logic for brake field
            if (onBrakeField)
            {
                speed *= 0.95f;
            }

            // gravity, wenn Fahrzeug in der Luft ist
            if (inAir)
            {
                fallingSpeed += TrackPart.gravityForce;

                Collision.CollisionReturn borderColRet = Collision.Spherecast(collisionContainer, position, 1.0f, 3);
                if (borderColRet.doesCollide)
                {
                    DoExplodeAnimation();
                }    
                
            }

           

            // Seitliche Kollision mittels Spherecast
            if (!inAir)
            {
                Collision.CollisionReturn sphereColRet = Collision.Spherecast(collisionContainer, position, 2.5f, 2);
                if (sphereColRet.doesCollide)
                {
                    float outMove = Math.Abs(2.5f - (position - sphereColRet.position).Length) * 1.01f;
                    position += sphereColRet.normal * outMove;

                    float d = -Vector3.Dot(sphereColRet.position, left);
                    bool bounceLeft = GeometryHelpers.PlaneDistanceToPoint(left, d, position) < 0;

                    bool wallLeft = (position - TrackPart.TrackInfo[nearestTrackIndex].leftBorderBack).LengthSquared < (position - TrackPart.TrackInfo[nearestTrackIndex].rightBorderBack).LengthSquared;

                    int wallType = 0;
                    if (wallLeft)
                    {
                        wallType = TrackPart.TrackInfo[nearestTrackIndex].leftBorderType;
                    }
                    else {
                        wallType = TrackPart.TrackInfo[nearestTrackIndex].rightBorderType;
                    }

                    if (wallType == 1) // energy fence
                    {
                        energy += 0.7f;
                        if (energy > 100.0f) energy = 100.0f;
                    }

                    if (wallType == 0)
                    {
                        float angleIntens = Math.Abs(Vector3.Dot(-sphereColRet.normal, front));
                        speed *= 1.0f - angleIntens * 0.3f;

                        float bounceIntensity = speed * 0.5f;
                        if (bounceIntensity > 0.5f) bounceIntensity = 0.5f;

                        if (speed > 0.13)
                        {
                            subEnergy(bounceIntensity * 2);

                            var bounceValue = ((reflect(front, sphereColRet.normal) + sphereColRet.normal * 0.5f).Normalized() * bounceIntensity);
                            if (bounce.Length == 0)
                            {
                                bounce = bounceValue;
                            }
                            else
                            {
                                bounce = Vector3.Lerp(bounce, bounceValue, 0.5f);
                            }

                            sideUpAnimation = -Math.Max(bounceValue.Length * 0.5f, 0.6f);


                            if (bounceLeft) sideUpAnimation *= -1;

                        }
                    }              

                }
            }
            // Checken ob über Item
            for (int i = 0; i < TrackPart.TrackItems.Count; i++)
            {
                if ((TrackPart.TrackItems[i].position - position).LengthSquared < 2.0f * 2.0f)
                {
                    if (TrackPart.TrackItems[i].type == 0 && boostCounter < 100) // boost
                    {
                        boostCounter = 120;
                        speed = maxAccelerationSpeed;
                    }
                    if (TrackPart.TrackItems[i].type == 1) // jump
                    {
                        inAir = true;
                        fallingSpeed = -0.33f;
                        position.Y += 0.75f;
                    }
                }
            }

            // animation rotation value
            float aniRotY = 0.0f;
            float sinRotValue2 = CalculateVehicleDirection(leftRight, ROTATE_SPEED, MAX_ROTATION, rBackIntens);

            if (rollLeft && rollAnimation == 0)
            {
                rollLeftRight = true;
                rollAnimation = 20;
            }

            if (rollRight && rollAnimation == 0)
            {
                rollLeftRight = false;
                rollAnimation = 20;
            }

            if (!wipeLeft && !wipeRight && rollAnimation == 0)
            {
                wipeValueAnimtion = wipeValueAnimtion * 0.8f + sinRotValue2 * 0.2f;
                scewValueAnimation = scewValueAnimation * 0.5f + sinRotValue2 * 0.5f;

                if (strideValue < 0) strideValue += 0.01f;
                if (strideValue > 0) strideValue -= 0.01f;
                if (Math.Abs(strideValue) < 0.01f) strideValue = 0.0f;

            }
            else
            {
                float mWipe = 4.0f;
                float mScew = 2.0f;

                if (rollAnimation == 0)
                {
                    if (wipeRight)
                    {
                        lastScewLeftRight = false;

                        strideValue -= 0.02f;
                        if (strideValue < -0.1f) strideValue = -0.2f;

                        if (wipeValueAnimtion < mWipe) wipeValueAnimtion += 1.0f;
                        if (wipeValueAnimtion > mWipe) wipeValueAnimtion = mWipe;

                        if (scewValueAnimation < mScew) scewValueAnimation += 0.5f;
                        if (scewValueAnimation > mScew) scewValueAnimation = mScew;

                    }

                    if (wipeLeft)
                    {
                        lastScewLeftRight = true;

                        strideValue += 0.02f;
                        if (strideValue > 0.1f) strideValue = 0.2f;

                        if (wipeValueAnimtion > -mWipe) wipeValueAnimtion -= 1.0f;
                        if (wipeValueAnimtion < -mWipe) wipeValueAnimtion = -mWipe;

                        if (scewValueAnimation > -mScew) scewValueAnimation -= 0.5f;
                        if (scewValueAnimation < -mScew) scewValueAnimation = -mScew;

                    }
                }


            }

            // roll animation
            if (rollAnimation > 0)
            {
                speed *= 0.99f;
                rollAnimation--;
                scewValueAnimation = 0;
                if (rollLeftRight)
                {
                    wipeValueAnimtion = 4.2f;
                    aniRotY = (float)(Math.PI * 2.0f) / 20.0f * rollAnimation;
                }
                else
                {
                    wipeValueAnimtion = -4.2f;
                    aniRotY = -(float)(Math.PI * 2.0f) / 20.0f * rollAnimation;
                }
            }

            aniRotY -= scewValueAnimation * 0.01f;


            positionCameraToFollowVehicle(lookBackwards);
            updateVehicleGraphics(aniRotY);

            update++;

            checkCollisionWithCylinders();

            checkCollisionWithOthers();

            // animations
            sideUpAnimation *= 0.95f;
            if (Math.Abs(sideUpAnimation) < 0.01f) sideUpAnimation = 0.0f;
            animationMatrix2 = Matrix4.Identity;
            animationMatrix2 *= Matrix4.CreateTranslation(up * ((float)Math.Sin(update * 0.1f) * Math.Max(0, 0.02f * (1.0f - speed)) + Math.Max(upDownValue,-0.10f)));
            animationMatrix1 = Matrix4.Identity;
            animationMatrix1 *= Matrix4.CreateRotationX((float)Math.Sin(update * 0.07f) * (float)Math.Sin(update * 0.03f) * 0.025f + sideUpAnimation);
            vehicleObject.AddTale(position + up * 0.02f, left);

            // up-down velocity handling
            if (inAir == false)
            {
                float dd = -Vector3.Dot(oldPositon, up);
                float velDist = GeometryHelpers.PlaneDistanceToPoint(up, dd, position);
                upDownValue += velDist * VELO_FORCE;
                if (upDownValue < -0.15f) upDownValue = -0.10f;
            }
            upDownValue *= VELO_SMOOTHOUT;


            vehicleObject.Update();
        }

        internal float CalculateVehicleDirection(float leftRight, float ROTATE_SPEED, float MAX_ROTATION, float rBackIntens)
        {
            if (Math.Abs(leftRight) < 0.15f) leftRight = 0;
            leftRight *= 1.15f;
            if (leftRight > 1.0f) leftRight = 1.0f;
            if (leftRight < -1.0f) leftRight = -1.0f;

            float steerToGo = leftRight;

            rotateValue = rotateValue * 0.4f + steerToGo * 0.6f;
            if (Math.Abs(rotateValue) < 0.01f) rotateValue = 0;

            SetNewVehicleDirection(rotateValue * MAX_ROTATION);
            return rotateValue * MAX_ROTATION;
        }

        internal void SetNewVehicleDirection(float sinRotValue)
        {
            Vector3 steerVec = new Vector3((float)Math.Cos(sinRotValue * Math.PI / 180.0f), 0, (float)Math.Sin(sinRotValue * Math.PI / 180.0f));
            Matrix3 rotateMatrix = new Matrix3(left, up, front);
            left = (rotateMatrix.Inverted() * steerVec);

            front = Vector3.Cross(left, up).Normalized();
            left = Vector3.Cross(up, front).Normalized();
            up = Vector3.Cross(front, left).Normalized();
        }

        internal void updateVehicleGraphics(float aniRotY)
        {
            Matrix4 frotateMatrix = Matrix4.Identity;
            frotateMatrix.Row0 = new Vector4(left, 0);
            frotateMatrix.Row1 = new Vector4(up, 0);
            frotateMatrix.Row2 = new Vector4(front, 0);

            Matrix4 vehicleTransform = Matrix4.CreateRotationY(-(float)Math.PI / 2.0f + aniRotY) * frotateMatrix;
            vehicleTransform *= Matrix4.CreateTranslation(position + up * 0.2f);

            vehicleObject.SetTransform(vehicleTransform);
            vehicleObject.CreatePartTransforms(wipeValueAnimtion, scewValueAnimation);
        }

        internal void positionCameraToFollowVehicle(bool lookBackwards)
        {
            Vector3 camEyeDirection = front;
            if (lookBackwards)
            {
                camEyeDirection *= -1;
            }

            Vector3 cameraEyeToGo = position - camEyeDirection * currentCameraPreset.VehicleCameraDistanceCoefficient + up * currentCameraPreset.VehicleCameraHeightCoefficient;
            Vector3 cameraTargetToGo = position + up * currentCameraPreset.VehicleCameraAngleCoefficient;
            Vector3 cameraUpToGo = up;


            float camInertia = currentCameraPreset.VehicleCameraEyeInertiaCoefficient;
                
            CameraEye = CameraEye * (1f - camInertia) + cameraEyeToGo * camInertia;

            float crashCamOffsetValue = 0;
            if (currentCrashCamIntensity > 0)
            {
                float frequency = currentCameraPreset.CrashCamFrequency;
                
                float baseCrashCamOffsetValue = (float)(Math.Cos(2f * Math.PI * frequency * crashCamFrameCount++));
                crashCamOffsetValue = baseCrashCamOffsetValue * -1 * currentCrashCamIntensity;
                currentCrashCamIntensity -= crashCamSmoothing;
            }
            
            CameraTarget = CameraTarget * (1f - currentCameraPreset.VehicleCameraTargetInertiaCoefficient) + cameraTargetToGo * currentCameraPreset.VehicleCameraTargetInertiaCoefficient;
            
            CameraTarget += new Vector3(0, crashCamOffsetValue, 0);
            

            CameraUp = CameraUp * 0.55f + cameraUpToGo * 0.45f;
        }
        
        private Vector3 reflect(Vector3 a, Vector3 n)
        {
            return a - 2 * Vector3.Dot(a, n) * n;
            //r = a - 2 < a, n > n
        }

        private float interpolation(float start, float end, float perOne)
        {
            return (end - start) * (1.0f - ((float)Math.Sin(perOne * Math.PI - Math.PI * 0.5f) * -0.5f + 0.5f)) + start;
        }

        private void checkCollisionWithOthers()
        {
            for (int i = 0; i < OtherVehicle.Instances.Count; i++)
            {
                if (OtherVehicle.Instances[i].IsCollidable())
                {
                    Vehicle other = OtherVehicle.Instances[i].vehicleObject;

                    Vector3 yourPosition;
                    float yourRadius;

                    for (int o = 0; o < vehicleObject.comparePos.Count; o++)
                    {
                        bool found = false;
                        for (int j = 0; j < vehicleObject.comparePos.Count - 2; j++)
                        {
                            if (found == false)
                            {
                                // check mit deiner position
                                yourRadius = vehicleObject.compareRad[j];
                                yourPosition = vehicleObject.comparePos[j];
                                if (other.comparePos.Count > 0 && //the very first time this method runs other.comparePos.Count != vehicleObject.comparePos.Count so otherwise this would crash // after rearranging method call to this method
                                    (yourPosition - other.comparePos[o]).Length < other.compareRad[o] + yourRadius)
                                {
                                    found = true;
                                    float speedDifference = OtherVehicle.Instances[i].Speed - speed;
                                    float speedDifferenceAbs = Math.Abs(speedDifference);
                                    position += (other.comparePos[o] - yourPosition).Normalized() * ((yourPosition - other.comparePos[o]).Length - (other.compareRad[o] + yourRadius));
                                    OtherVehicle.Instances[i].BounceFromPos(position, speedDifferenceAbs * 0.5f);

                                    // Du fährts in seinen Stachel
                                    if (o == other.comparePos.Count - 1)
                                    {
                                        subEnergy(10);
                                        vehicleObject.Blink();
                                        if (rollAnimation == 0) speed *= 0.4f; else speed *= 0.9f;
                                        OtherVehicle.Instances[i].Speed += speedDifferenceAbs;
                                        bounce = -(other.comparePos[o] - yourPosition).Normalized() * speedDifferenceAbs * 0.3f;
                                        bounce = projectBounce(bounce, position, up);
                                    }
                                    else
                                    {
                                        subEnergy(1.5f);
                                        bounce = -(other.comparePos[o] - yourPosition).Normalized() * 0.1f;
                                        bounce = projectBounce(bounce, position, up);
                                    }
                                }
                            }
                        }


                        // Check mit deinem letzten Part (Stachel);
                        float yourLastRadius = vehicleObject.compareRad[vehicleObject.compareRad.Count - 1];
                        yourPosition = vehicleObject.comparePos[vehicleObject.comparePos.Count - 1];
                        if (other.comparePos.Count > 0 && 
                            (yourPosition - other.comparePos[o]).Length < other.compareRad[o] + yourLastRadius)
                        {
                            float speedDifference = OtherVehicle.Instances[i].Speed - speed;
                            float speedDifferenceAbs = Math.Abs(speedDifference);

                            bounce = (other.comparePos[o] - position).Normalized() * -speedDifferenceAbs * 0.3f;
                            OtherVehicle.Instances[i].Speed *= 0.4f;
                            OtherVehicle.Instances[i].BounceFromPos(position, speedDifferenceAbs * 0.4f + 0.2f);
                            OtherVehicle.Instances[i].Blink();

                            //OtherVehicle.Instances[i].Explode();

                        }
                    }


                }

            }

        }

        private Vector3 projectBounce(Vector3 bounce, Vector3 pos, Vector3 up)
        {
            float dd = -Vector3.Dot(pos, up);
            //float velDist = GeometryHelpers.PlaneDistanceToPoint(up, dd, position);
            return GeometryHelpers.NearestPointOnPlane(up, dd, pos + bounce) - pos;
        }


        /// <summary>
        /// Checks collision with cylinders on track
        /// </summary> 
        private void checkCollisionWithCylinders()
        {
            foreach (TrackPart.TrackItem item in TrackPart.TrackItems)
            {
                if (item.type >= 2) // cylinders
                {
                    Vector3 np = GeometryHelpers.NearestPointOnLine(item.position, item.positionUpper, position);
                    float itemCompare = item.radius + 0.7f;
                    if ((np - position).LengthSquared < itemCompare * itemCompare)
                    {
                        Vector3 normal = (position - np).Normalized();

                        position = normal * itemCompare + np;

                        float angleIntens = Math.Abs(Vector3.Dot(normal, front));

                        float bounceIntensity = speed;
                        if (bounceIntensity < 0.2f) bounceIntensity = 0.2f;
                        if (bounceIntensity > 0.5f) bounceIntensity = 0.5f;

                        subEnergy(bounceIntensity * 2);

                        bounce = (reflect(front, normal) + normal * 0.5f).Normalized() * bounceIntensity;
                        speed *= 1.0f - angleIntens * 0.7f;

                    }

                }
            }
        }

        public int getNumTrackIndicesDrivenSinceStart()
        {
            return currentIntRound * TrackPart.TrackInfo.Count + nearestTrackIndex - TrackPart.StartTrackPos;
        }

        public int GetPlacing()
        {
            int ret = 1;
            float compare = currentIntRound * TrackPart.TrackInfo.Count + nearestTrackIndex - TrackPart.StartTrackPos;
            foreach (OtherVehicle v in OtherVehicle.Instances)
            {
                if (compare < v.GetRaceOverallPos()) ret++;
            }
            return ret;
        }


        public int GetCurrentRound()
        {
            float compare = (currentIntRound + 1) * TrackPart.TrackInfo.Count + nearestTrackIndex - TrackPart.StartTrackPos;
            return (int)compare / TrackPart.TrackInfo.Count;
        }


        public void DrawShadow()
        {
            if (uncollideBlink % 20 < 10) vehicleObject.DrawShadow(animationMatrix1, animationMatrix2);
        }

        public void Draw()
        {
            if (!(currentCameraPreset is FPSCameraPreset))
            {
                if (uncollideBlink % 20 < 10) vehicleObject.Draw(animationMatrix1, animationMatrix2);                
            }
        }

        public void DrawLightTale()
        {
            if (explodeCounter == 0) vehicleObject.DrawLightTale(Math.Min(speed * 2, 1.0f) * 0.1f, 2);
        }

        public void DrawExplosion()
        {
            if (explodeCounter > 0) vehicleObject.DrawExplosion();
        }

        public OwnVehicle InitializeFrom(OwnVehicle toBeReplaced)
        {
            vehicleObject = toBeReplaced.vehicleObject;
            toBeReplaced.vehicleObject = null;

            collisionContainer = toBeReplaced.collisionContainer;
            toBeReplaced.collisionContainer = null;
            
            return this;
        }

        public void DrawFire()
        {
            if (explodeCounter == 0) vehicleObject.DrawFire(animationMatrix1, animationMatrix2, Math.Min(lightThrottle, 7));
        }



        // 


        public void DrawLights()
        {
            vehicleObject.DrawLights(animationMatrix1, animationMatrix2, lightThrottle / 10.0f, 3.5f);
        }


    }
}
