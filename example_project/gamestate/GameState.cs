﻿using lv1.vehicle;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace lv1.gamestate
{
    class GameState
    {
        private static volatile GameState instance;
        private static object syncRoot = new Object();

        private string[] saveSlots = new string[5];

        public static GameState Instance
        {
            get
            {
                if (instance == null)
                {
                    lock (syncRoot)
                    {
                        if (instance == null)
                            instance = new GameState();
                    }
                }

                return instance;
            }
        }

        private JsonSerializerSettings jsonSerializerSettings = new JsonSerializerSettings()
        {
            TypeNameHandling = TypeNameHandling.Auto,
            ConstructorHandling = ConstructorHandling.AllowNonPublicDefaultConstructor,
            PreserveReferencesHandling = PreserveReferencesHandling.Objects,
            ObjectCreationHandling = ObjectCreationHandling.Auto,
            ReferenceLoopHandling = ReferenceLoopHandling.Ignore,
            ContractResolver = new MyContractResolver()
            //Converters = new List<JsonConverter> { new }
        };


        private GameState() { }


        public void SaveGameState(int slot, GameStateBundle bundle)
        {
            
            string output = JsonConvert.SerializeObject(bundle, Formatting.Indented, jsonSerializerSettings);
            
            saveSlots[slot] = output;
        }

        public GameStateBundle GetGameState(int slot)
        {
            if (saveSlots[slot] == null)
            {
                return null;                
            } else
            {
                return JsonConvert.DeserializeObject<GameStateBundle>(saveSlots[slot], jsonSerializerSettings);
            }
            
        }

        class VectorConverter : JsonConverter
        {
            public override bool CanConvert(Type objectType)
            {
                return objectType.Equals(typeof(OpenTK.Vector2));
            }

            public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
            {
                throw new NotImplementedException();
            }

            public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
            {
                throw new NotImplementedException();
            }
        }

        class MyContractResolver : DefaultContractResolver
        {
            
            protected override IList<JsonProperty> CreateProperties(Type type, MemberSerialization memberSerialization)
            {
                var props = type.GetFields(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance)
                               .Select(f => base.CreateProperty(f, memberSerialization))
                    .ToList();
                
                props.ForEach(p => 
                {
                    p.Writable = p.Readable = true;
                    
                    if (
                        //don't serialize vehicle, that one contains openGL inits, is copied outside of this GameState class.                    
                        p.PropertyType == typeof(Vehicle)                        
                      ||
                        //also, don't try to write out this million triangles lists, million boxes lists, and million vector lists. it causes the string to be 130 MB in size.
                        p.PropertyType == typeof(Engine.cgimin.collision.CollisionGrid)
                        
                    )
                    {
                        p.Writable = p.Readable = false;
                        p.ShouldSerialize =  pr => false;
                    }
                   

                });
                return props;                
            }

        }

    }
}
