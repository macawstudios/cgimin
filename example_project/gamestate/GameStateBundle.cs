﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lv1.gamestate
{
    class GameStateBundle
    {
        public OwnVehicle vehicle;
        public List<OtherVehicle> otherVehicles;

        public GameStateBundle(OwnVehicle vehicle, List<OtherVehicle> otherVehicles)
        {
            this.vehicle = vehicle;
            this.otherVehicles = otherVehicles;
        }

    }
}
