﻿using Engine.cgimin.gui;
using Engine.cgimin.texture;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lv1.guielements
{
    public class SimpleTextButton : GUIButton
    {



        private GUITextElement textElement;

        public SimpleTextButton(int width, string id, string text, BitmapFont font, int GUITexture, Action<string> clickCallBack) : base(id, clickCallBack)
        {

            GUI9Grid bg = new GUI9Grid(1024, 1024, GUITexture, 0, 0, 46, 46, 15, 15, 15, 15);
            this.AddChild(bg);
            bg.Width = width;
            bg.Height = 70;

            textElement = new GUITextElement(font);
            textElement.Text = text;
            textElement.r = 255;
            textElement.g = 255;
            textElement.b = 255;
            textElement.x = width / 2 - textElement.CalcTextWidth() / 2;

            textElement.y = 0;

            this.AddChild(textElement);
        }

        public override void Update()
        {
            base.Update();

            if (this.IsSelected())
            {
                textElement.r = 255;
                textElement.g = 0;
                textElement.b = 0;
            } else
            {
                textElement.r = 255;
                textElement.g = 255;
                textElement.b = 255;
            }

        }

    }
}
