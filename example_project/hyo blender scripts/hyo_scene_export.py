bl_info = {
    "name": "Export hyo Scene",
    "category": "Object",
}

import bpy, os, sys


class SceneExport(bpy.types.Operator):
        
    """Hyo scene export"""      # blender will use this as a tooltip for menu items and buttons.
    bl_idname = "object.scene_export"        # unique identifier for buttons and menu items to reference.
    bl_label = "hyo scene export"         # display name in the interface.
    bl_options = {'REGISTER', 'UNDO'}  # enable undo for the operator.

    def execute(self, context):        # execute() is called by blender when running the operator.

        # The original script
        scene = context.scene
        
        filepath = bpy.path.abspath("//trackscene.txt")
    
        name = os.path.basename(filepath)
        realpath = os.path.realpath(os.path.expanduser(filepath))
        fp = open(realpath, 'w')    
        print('Exporting %s' % realpath)
        
        # deselect all objects in scene
        for ob in bpy.context.selected_objects:
            ob.select = False
                
        for obj in scene.objects:
            
            scx = obj.scale.x
            scy = obj.scale.y
            scz = obj.scale.z
            
            rx = obj.rotation_euler.x
            ry = obj.rotation_euler.y
            rz = obj.rotation_euler.z
            
            lx = obj.location.x
            ly = obj.location.y
            lz = obj.location.z
            
            obj.scale.x = 1.0
            obj.scale.y = 1.0
            obj.scale.z = 1.0
            
            obj.rotation_euler.x = 0.0
            obj.rotation_euler.y = 0.0
            obj.rotation_euler.z = 0.0
            
            obj.location.x = 0.0
            obj.location.y = 0.0
            obj.location.z = 0.0
            
            obj.select = True
            
            if hasattr(obj.data, 'uv_textures'):
                obj.data.uv_textures[0].active = True
                bpy.ops.export_scene.obj(filepath=bpy.path.abspath("//" + obj.name + ".obj"), axis_forward='-Z', axis_up='Y', use_selection=True, use_materials=False, use_triangles=True, use_edges=False, keep_vertex_order=True)
                
                if len(obj.data.uv_textures) > 1:
                    obj.data.uv_textures[1].active = True
                    bpy.ops.export_scene.obj(filepath=bpy.path.abspath("//" + obj.name + "_shadow.obj"), axis_forward='-Z', axis_up='Y', use_selection=True, use_materials=False, use_triangles=True, use_edges=False, keep_vertex_order=True)
                
            obj.select = False
            
            obj.scale.x = scx
            obj.scale.y = scy
            obj.scale.z = scz
            
            obj.rotation_euler.x = rx
            obj.rotation_euler.y = ry
            obj.rotation_euler.z = rz
            
            obj.location.x = lx
            obj.location.y = ly
            obj.location.z = lz 
        
            me = obj.data
            if hasattr(me, 'uv_textures'):
                if me.uv_textures.active is not None:
                    st = ""
                    tf = me.uv_textures[0].data[0] 
                    if tf.image:
                        st = st + tf.image.name
                    
                    if len(me.uv_textures) > 1:
                        tf = me.uv_textures[1].data[0] 
                        if tf.image:
                            st = st + " " + tf.image.name
                        
                    if st != "":
                        fp.write("tex %s %s\n"% (obj.name, st))
                    
                
            
        
            fp.write("obj %s %f %f %f %f %f %f %f %f %f\n"% (obj.name, obj.location.x, obj.location.y, obj.location.z, obj.scale.x, obj.scale.y, obj.scale.z, obj.rotation_euler.x, obj.rotation_euler.y, obj.rotation_euler.z));            
        
                    
        print('Exported!')
        fp.close()

        return {'FINISHED'}            # this lets blender know the operator finished successfully.

def register():
    bpy.utils.register_class(SceneExport)


def unregister():
    bpy.utils.unregister_class(SceneExport)


# This allows you to run the script directly from blenders text editor
# to test the addon without having to install it.
if __name__ == "__main__":
    register() 