bl_info = {
    "name": "hyo Auto Shadow Map 128",
    "category": "Object",
}

import bpy, os, sys


class ShadowAuto(bpy.types.Operator):
        
    """hyo Auto Shadow Map"""      # blender will use this as a tooltip for menu items and buttons.
    bl_idname = "object.shadow_auto_128"        # unique identifier for buttons and menu items to reference.
    bl_label = "hyo auto shadow 128"         # display name in the interface.
    bl_options = {'REGISTER', 'UNDO'}  # enable undo for the operator.

    def execute(self, context):        # execute() is called by blender when running the operator.

        # The original script
        scene = context.scene
        
        selectedlist = []

        for obj in context.selected_objects:
            selectedlist.append(obj)
            obj.select = False
            
        for obj in selectedlist:
            
            if hasattr(obj.data, 'uv_textures'):    
                
                obj.select = True
                
                if "shadowmapUV" in obj.data.uv_textures:              
                    obj.data.uv_textures["shadowmapUV"].active = True
                else:
                    obj.data.uv_textures.new("shadowmapUV")
                     
                obj.data.uv_textures["shadowmapUV"].active = True        
                
                bpy.ops.uv.smart_project(island_margin = 0.2)
                                
                bpy.ops.object.mode_set(mode='EDIT', toggle=False)
                
                bpy.ops.mesh.reveal()              
                bpy.ops.mesh.select_all(action='SELECT')
                                
                imagename = obj.name + "_shadow"
                
                if imagename in bpy.data.images:
                    img = bpy.data.images[imagename]
                    img.user_clear() 
                    bpy.data.images.remove(img)
                
                image = bpy.data.images.new(name=imagename, width=128, height=128)

                image = bpy.data.images[imagename]
                
                bpy.data.screens['UV Editing'].areas[1].spaces[0].image = image
                
                bpy.ops.object.bake_image()

                image.filepath_raw = bpy.path.abspath("//" + imagename + ".png")
                image.file_format = 'PNG'
                image.save()        
                                
                bpy.ops.object.mode_set(mode='OBJECT', toggle=False)
                
                obj.select = False
                

            
             
        return {'FINISHED'}            # this lets blender know the operator finished successfully.

def register():
    bpy.utils.register_class(ShadowAuto)


def unregister():
    bpy.utils.unregister_class(ShadowAuto)


# This allows you to run the script directly from blenders text editor
# to test the addon without having to install it.
if __name__ == "__main__":
    register() 