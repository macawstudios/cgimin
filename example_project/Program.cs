﻿

#region --- Using Directives ---

using System;
using System.Diagnostics;
using Engine.cgimin.camera;
using Engine.cgimin.shadowmappingcascaded;
using Engine.cgimin.helpers;
using Engine.cgimin.gui;
using OpenTK;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;
using lv1.scenes;
using Engine.cgimin.material;


#endregion --- Using Directives ---

namespace lv1
{

    public class MainGame : GameWindow
    {
        public static int MSForUpdate;
        public static int MSForDraw;

        private const int screenWidth = 1920;
        private const int screenHeigth = 1080;
        private const float fov = 60;

        public enum  ScenesEnum { 
            MenuScene = 0,
            GameScene 
        }

        private ScenesEnum currentScene;

        public MainGame()
            : base(screenWidth, screenHeigth, new GraphicsMode(32, 24, 8, 0), "Silverfish Racing", GameWindowFlags.Fullscreen, DisplayDevice.Default, 3, 2, GraphicsContextFlags.Default /* | GraphicsContextFlags.Default */)
        {
           

        }


        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

           

            TimeMeasure.Start();

            // Kamera initialisieren
            Camera.Init();
            Camera.SetWidthHeightFov(screenWidth, screenHeigth, fov, 0.1f, 1000);
            Camera.SetDynamicFovNearFar(90.0f, 0.1f, 1000);
            Camera.SetupFog (1, 512, new Vector3 (0.545f, 0.545f, 0.0f));

            GameScene.LoadAndInit(screenWidth, screenHeigth);
            MenuScene.LoadAndInit(StartRace);

            currentScene = ScenesEnum.MenuScene;

            // Tiefenpuffer einschalten
            GL.Enable(EnableCap.DepthTest);

            // Kameraposition setzen
            Camera.SetLookAt(new Vector3(0, 0, 3), new Vector3(0, 0, 0), new Vector3(0, 1, 0));

            // Cascaded Shadow-Mapping initialisieren
            ShadowMappingCascaded.Init(2048,         // Texturgröße vorn 
                                       2048,         // Texturgröße mitte
                                       1024,         // Texturgröße hinten
                                       10, 70, 350,  // Cascade Grenzen
                                       40);          // minimale Cascade-Größe

            GL.BindFramebuffer(FramebufferTarget.Framebuffer, 0);
            GL.Enable(EnableCap.CullFace);
            GL.CullFace(CullFaceMode.Front);

        }


        private void StartRace()
        {
            currentScene = ScenesEnum.GameScene;
        }


        protected override void OnUpdateFrame(FrameEventArgs e)
        {
            Stopwatch uStartTime = Stopwatch.StartNew();

            if (Keyboard[OpenTK.Input.Key.Escape])
                this.Exit();

            if (Keyboard[OpenTK.Input.Key.F11])
                if (WindowState != WindowState.Fullscreen)
                    WindowState = WindowState.Fullscreen;
                else
                    WindowState = WindowState.Normal;

            switch (currentScene) {
                case ScenesEnum.GameScene:
                    GameScene.Update(this);
                    break;
                case ScenesEnum.MenuScene:
                    MenuScene.Update(this);
                    break;
            }

            GUISystem.Update(this);
            BaseMaterial.Update();

            MSForUpdate = (int)uStartTime.ElapsedMilliseconds;
        }


        protected override void OnRenderFrame(FrameEventArgs e)
        {

            Stopwatch rStartTime = Stopwatch.StartNew();

            switch (currentScene)
            {
                case ScenesEnum.GameScene:
                    GameScene.Draw();
                    break;
                case ScenesEnum.MenuScene:
                    MenuScene.Draw();
                    break;
            }

            GUISystem.Draw();
            SwapBuffers();

            MSForDraw = (int)rStartTime.ElapsedMilliseconds;
        }



        protected override void OnUnload(EventArgs e)
        {
          // to do...
        }


        protected override void OnResize(EventArgs e)
        {
            GL.Viewport(0, 0, Width, Height);
            Camera.SetWidthHeightForResize(Width, Height);
        }


        [STAThread]
        public static void Main()
        {
            using (MainGame example = new MainGame())
            {
                example.Run(60.0/*, 60.0*/);
            }
        }

      
    }
}

