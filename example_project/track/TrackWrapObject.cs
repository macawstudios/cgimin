﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using OpenTK;
using Engine.cgimin.object3d;

namespace lv1.track
{
    class TrackWrapObject : BaseObject3D
    {
        public static int WRAPMODE_SCALE_SIZE = 0;
        public static int WRAPMODE_CONST_YUP = 1;
        public static int WRAPMODE_LEAVE_Y = 2;


        public TrackWrapObject(BaseObject3D sourceObj, int start, int wrapMode, bool mirror, bool zOutZero = false, float heightMul = 1.0f, float yHeight = -10000)
        {

            List<Vector3> intPositions = new List<Vector3>();
            List<Vector3> intNormals = new List<Vector3>();
            List<Vector2> intUVs = new List<Vector2>();

            Positions = new List<Vector3>();
            Normals = new List<Vector3>();
            UVs = new List<Vector2>();
            Tangents = new List<Vector3>();
            BiTangents = new List<Vector3>();
            Indices = new List<int>();

            int len = sourceObj.Positions.Count;

            // min max z heraussuchen
            float minZ = float.MaxValue;
            float maxZ = float.MinValue;
            for (int i = 0; i < len; i++)
            {
                if (sourceObj.Positions[i].Z < minZ) minZ = sourceObj.Positions[i].Z;
                if (sourceObj.Positions[i].Z > maxZ) maxZ = sourceObj.Positions[i].Z;
            }

            if (zOutZero) minZ = 0;

            for (int i = 0; i < len; i++)
            {
                int index1 = (int)Math.Floor(sourceObj.Positions[i].Z - minZ) + start;
                //int index2 = index1 + 1;

                float subZPos = (sourceObj.Positions[i].Z - minZ) - (float)Math.Floor(sourceObj.Positions[i].Z - minZ);

                Vector3 dirY = new Vector3();
                Vector3 dirX = new Vector3();

                // Backpos
                Vector3 startPosition = (TrackPart.TrackInfo[index1].rightBorderBack + TrackPart.TrackInfo[index1].leftBorderBack) * 0.5f;
                if (wrapMode == WRAPMODE_SCALE_SIZE)
                {
                    dirX = (TrackPart.TrackInfo[index1].rightBorderBack - TrackPart.TrackInfo[index1].leftBorderBack) * 0.05f;
                    dirY = TrackPart.TrackInfo[index1].normalBack * dirX.Length;
                }

                if (wrapMode == WRAPMODE_CONST_YUP)
                {
                    dirX = (TrackPart.TrackInfo[index1].rightBorderBack - TrackPart.TrackInfo[index1].leftBorderBack) * 0.05f;
                    dirY = TrackPart.TrackInfo[index1].normalBack * heightMul;
                }

                if (wrapMode == WRAPMODE_LEAVE_Y)
                {
                    if (yHeight > -10000) startPosition.Y = yHeight;
                    dirX = (TrackPart.TrackInfo[index1].rightBorderBack - TrackPart.TrackInfo[index1].leftBorderBack);
                    dirX.Normalize();
                    dirX.Y = 0;
                    dirX *= 0.5f;
                    dirY = TrackPart.TrackInfo[index1].normalBack * heightMul;
                }
                Vector3 posBack = new Vector3();
                if (!mirror) posBack = startPosition + dirX * sourceObj.Positions[i].X + dirY * sourceObj.Positions[i].Y;
                if (mirror) posBack = startPosition + dirX * (-sourceObj.Positions[i].X) + dirY * sourceObj.Positions[i].Y;

                Vector3 dirX1 = dirX.Normalized();
                if (mirror) dirX1 = -dirX1;
                Vector3 dirY1 = dirY.Normalized();
                Vector3 dirZ1 = Vector3.Cross(dirX1, dirY1).Normalized();


                startPosition = (TrackPart.TrackInfo[index1].rightBorderFront + TrackPart.TrackInfo[index1].leftBorderFront) * 0.5f;
                if (wrapMode == WRAPMODE_SCALE_SIZE)
                {
                    dirX = (TrackPart.TrackInfo[index1].rightBorderFront - TrackPart.TrackInfo[index1].leftBorderFront) * 0.05f;
                    dirY = TrackPart.TrackInfo[index1].normalFront * dirX.Length;
                }

                if (wrapMode == WRAPMODE_CONST_YUP)
                {
                    dirX = (TrackPart.TrackInfo[index1].rightBorderFront - TrackPart.TrackInfo[index1].leftBorderFront) * 0.05f;
                    dirY = TrackPart.TrackInfo[index1].normalFront * heightMul;
                }

                if (wrapMode == WRAPMODE_LEAVE_Y)
                {
                    if (yHeight > -10000) startPosition.Y = yHeight;
                    dirX = (TrackPart.TrackInfo[index1].rightBorderFront - TrackPart.TrackInfo[index1].leftBorderFront);
                    dirX.Normalize();
                    dirX.Y = 0;
                    dirX *= 0.5f;
                    dirY = TrackPart.TrackInfo[index1].normalFront * heightMul;
                }

                Vector3 posFront = new Vector3();
                if (!mirror) posFront = startPosition + dirX * sourceObj.Positions[i].X + dirY * sourceObj.Positions[i].Y;
                if (mirror) posFront = startPosition + dirX * (-sourceObj.Positions[i].X) + dirY * sourceObj.Positions[i].Y;

                Vector3 dirX2 = dirX.Normalized();
                if (mirror) dirX2 = -dirX2;
                Vector3 dirY2 = dirY.Normalized();
                Vector3 dirZ2 = Vector3.Cross(dirX2, dirY2).Normalized();

                // Interpolierte normalen Matrix
                Vector3 lerpDirX = ((dirX1 - dirX2) * subZPos + dirX2).Normalized();
                Vector3 lerpDirY = ((dirY1 - dirY2) * subZPos + dirY2).Normalized();
                Vector3 lerpDirZ = ((dirZ1 - dirZ2) * subZPos + dirZ2).Normalized();

                Matrix3 dirMatrix = new Matrix3(lerpDirX, lerpDirY, lerpDirZ);
                Vector3 calculatedNormal = sourceObj.Normals[i] * dirMatrix;

                intNormals.Add(sourceObj.Normals[i] * dirMatrix);

                // Den finalen Vertex schreiben
                intPositions.Add((posFront - posBack) * subZPos + posBack);

                // UVs
                intUVs.Add(sourceObj.UVs[i]);

            }

            len = len / 3;

            if (!mirror)
            {
                for (int i = 0; i < len; i++)
                {
                    int index1 = i * 3;
                    int index2 = i * 3 + 1;
                    int index3 = i * 3 + 2;
                    addTriangle(intPositions[index2], intPositions[index1], intPositions[index3],
                                intNormals[index2], intNormals[index1], intNormals[index3], 
                                intUVs[index2], intUVs[index1], intUVs[index3]);
                }
            }
            else {
                for (int i = 0; i < len; i++)
                {
                    int index1 = i * 3;
                    int index2 = i * 3 + 1;
                    int index3 = i * 3 + 2;
                    addTriangle(intPositions[index2], intPositions[index3], intPositions[index1],
                                intNormals[index2], intNormals[index3], intNormals[index1],
                                intUVs[index2], intUVs[index3], intUVs[index1]);
                }
            }

            MidPointToCenter();
            CreateVAO();

        }




    }
}
