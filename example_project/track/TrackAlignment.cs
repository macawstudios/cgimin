﻿using System;
using System.Collections.Generic;
using Engine.cgimin.object3d;
using Engine.cgimin.collision;
using Engine.cgimin.helpers;
using OpenTK;


namespace lv1.track
{
    class TrackAlignment
    {

        public static Matrix4 MidAlign(int trackPos)
        {
            Matrix4 returnMatrix = Matrix4.Identity;
            Vector3 posFront = (TrackPart.TrackInfo[trackPos].leftBorderFront + TrackPart.TrackInfo[trackPos].rightBorderFront) * 0.5f;
            Vector3 posBack = (TrackPart.TrackInfo[trackPos].leftBorderBack + TrackPart.TrackInfo[trackPos].rightBorderBack) * 0.5f;
            returnMatrix *= Matrix4.LookAt(posBack, posFront, TrackPart.TrackInfo[trackPos].normalFront).Inverted();
            return returnMatrix;
        }

        public static Matrix4 MidStraightDownAlign(int trackPos)
        {
            Matrix4 returnMatrix = Matrix4.Identity;
            Vector3 posFront = (TrackPart.TrackInfo[trackPos].leftBorderFront + TrackPart.TrackInfo[trackPos].rightBorderFront) * 0.5f;
            Vector3 posBack = (TrackPart.TrackInfo[trackPos].leftBorderBack + TrackPart.TrackInfo[trackPos].rightBorderBack) * 0.5f;
            posFront.Y = posBack.Y;
            returnMatrix *= Matrix4.LookAt(posBack, posFront, new Vector3(0,1,0)).Inverted();
            return returnMatrix;
        }

        public static Matrix4 MidDownBootmPosAlign(int trackPos, float bottomY)
        {
            Matrix4 returnMatrix = Matrix4.Identity;
            Vector3 posFront = (TrackPart.TrackInfo[trackPos].leftBorderFront + TrackPart.TrackInfo[trackPos].rightBorderFront) * 0.5f;
            Vector3 posBack = (TrackPart.TrackInfo[trackPos].leftBorderBack + TrackPart.TrackInfo[trackPos].rightBorderBack) * 0.5f;
            posFront.Y = bottomY;
            posBack.Y = bottomY;
            returnMatrix *= Matrix4.LookAt(posBack, posFront, new Vector3(0, 1, 0)).Inverted();
            return returnMatrix;
        }


        public static Matrix4 LeftAlign(int trackPos, float rotation = 0)
        {
            Matrix4 returnMatrix = Matrix4.Identity;
            returnMatrix *= Matrix4.CreateRotationY(rotation);
            Vector3 posFront = TrackPart.TrackInfo[trackPos].leftBorderFront;
            Vector3 posBack = TrackPart.TrackInfo[trackPos].leftBorderBack;
            returnMatrix *= Matrix4.LookAt(posBack, posFront, TrackPart.TrackInfo[trackPos].normalFront).Inverted();
            
            return returnMatrix;
        }

        public static Matrix4 RightAlign(int trackPos, float rotation = 0)
        {
            Matrix4 returnMatrix = Matrix4.Identity;
            returnMatrix *= Matrix4.CreateRotationY(rotation);
            Vector3 posFront = TrackPart.TrackInfo[trackPos].rightBorderFront;
            Vector3 posBack = TrackPart.TrackInfo[trackPos].rightBorderBack;
            returnMatrix *= Matrix4.LookAt(posBack, posFront, TrackPart.TrackInfo[trackPos].normalFront).Inverted();
            return returnMatrix;
        }

        public static Matrix4 Beside(int trackPos, float leftRight, float rotation)
        {
            Vector3 mid = TrackPart.GetPosition(trackPos, 0.5f);
            Vector3 left = TrackPart.GetPosition(trackPos, 1.0f);
            left.Y = mid.Y;

            Vector3 dir = (mid - left).Normalized();
            Vector3 position = mid + dir * leftRight;

            Matrix4 transform = Matrix4.Identity;
            transform *= Matrix4.CreateRotationY(rotation);
            transform *= Matrix4.CreateTranslation(position);

            return transform;
        }

        public static Matrix4 BesideAligned(int trackPos, float leftRight, float rotationAdd)
        {
            Vector3 mid = TrackPart.GetPosition(trackPos, 0.5f);
            Vector3 left = TrackPart.GetPosition(trackPos, 1.0f);
            left.Y = mid.Y;

            Vector3 dir = (mid - left).Normalized();
            Vector3 position = mid + dir * leftRight;

            Matrix4 transform = Matrix4.Identity;
            transform *= Matrix4.CreateRotationY(rotationAdd * 180.0f / (float)Math.PI * + (float)(Math.Atan2(mid.Z - left.Z, mid.X - left.X)/* * 180.0f / Math.PI*/));
            transform *= Matrix4.CreateTranslation(position);

            return transform;
        }



    }
}
