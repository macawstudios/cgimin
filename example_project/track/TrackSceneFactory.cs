﻿using Engine.cgimin.collision;
using Engine.cgimin.object3d;
using Engine.cgimin.octree;
using Engine.cgimin.texture;
using OpenTK;
using System;
using System.Collections.Generic;
using System.Globalization;
using Engine.cgimin.deferred;
using OpenTK.Graphics.OpenGL;
using System.IO;
using System.Drawing;

namespace lv1.track
{
    class TrackSceneFactory
    {
        public class TrackSceneObjectOptions
        {
            public string objectName;

            // Material, wenn == null -> standard
            public BaseDeferredMaterial material = null;

            public bool receiveShadow = false;
            public bool castShadow = false;

            // Speculare Intensity
            public float fresnelValue = 0.5f;

            // Werte für das Blending 
            public bool transparent = false;

            public BlendingFactorSrc blendFactorSource = BlendingFactorSrc.SrcAlpha;
            public BlendingFactorDest blendFactorDest = BlendingFactorDest.OneMinusSrcAlpha;

            // speed für animation etc.
            public float speed = 1.0f;

            // soll in den collision-container?
            public bool addToCollision = true;

            // Postprocessing Flag
            public bool isLightObject = false;


        }

        private static List<TrackSceneObjectOptions> objectOptions = new List<TrackSceneObjectOptions>();

        private static int colorTextureFallBack;
        private static int normalTextureFallBack;
        private static int shadowTextureFallBack;
        private static int additionalTextureFallBack;

        private static ObjLoaderObject3D sphereLightObject;
        
        private class TexturePackEntity
        {
            public int colorTexture;
            public int normalTexture;
            public int shadowTexture;
            public int additionalTexture;
        }
        

        private static float toFloat(string st)
        {
            return float.Parse(st, CultureInfo.InvariantCulture);
        }


        public static void Init()
        {
            objectOptions.Clear();
        }

        public static void AddOption(TrackSceneObjectOptions option)
        {
            objectOptions.Add(option);
        }


        public static void Build(string sceneFolderName, int cubeTexture, Octree octree, Octree shadowOctree, Octree lightOctree, CollisionGrid grid)
        {
            string line;
            Char delimiterSpace = ' ';
            Char delimiterPoint = '.';

            string path = "../../data/Courses/" + sceneFolderName + "/";

            colorTextureFallBack = TextureManager.LoadTexture(path + "color_fallback.png");
            normalTextureFallBack = TextureManager.LoadTexture(path + "normal_fallback.tif");
            shadowTextureFallBack = TextureManager.LoadTexture(path + "shadow_fallback.png");
            additionalTextureFallBack = TextureManager.LoadTexture(path + "additional_fallback.tif");

            sphereLightObject = new ObjLoaderObject3D("../../data/objects/0_light.obj", 10.0f);

            Dictionary <string, TexturePackEntity> objTexDictionary = new Dictionary<string, TexturePackEntity>();
            Dictionary<string, int> texDictionary = new Dictionary<string, int>();

            // Erster durchlauf: Texture Infos und zuweisungen herausziehen
            System.IO.StreamReader file = new System.IO.StreamReader(path + "trackscene.txt");
            List<String> lines = new List<string>();
            while ((line = file.ReadLine()) != null)
            {
                lines.Add(line);
                String[] elements = line.Split(delimiterSpace);

                if (elements[0] == "tex")
                {
                    bool shadowDefinedOnly = elements[2].Contains("_shadow");
                    if (shadowDefinedOnly) Console.WriteLine("WARNING, only shadow texture defined for " + elements[1]);

                   string shadowTexName = "";
                    if ((elements.Length > 3 && elements[3] != "") || shadowDefinedOnly)
                    {
                        if (shadowDefinedOnly) shadowTexName = elements[2]; else shadowTexName = elements[3];

                        if (File.Exists(path + shadowTexName + ".png"))
                        {
                            if (!texDictionary.ContainsKey(shadowTexName + ".png"))
                                texDictionary.Add(shadowTexName + ".png", TextureManager.LoadTexture(path + shadowTexName + ".png"));
                        }
                        else
                        {
                            Console.WriteLine("MISSING SHADOW Texture: " + path + shadowTexName);
                            texDictionary.Add(shadowTexName, shadowTextureFallBack);
                        }
                    }


                    string texName = elements[2].Split(delimiterPoint)[0];
                    if (!texDictionary.ContainsKey(texName + ".tif") && !shadowDefinedOnly)
                    {
                        if (File.Exists(path + texName + ".tif"))
                        {
                            texDictionary.Add(texName + ".tif", TextureManager.LoadTexture(path + texName + ".tif"));
                        }
                        else
                        {
                            Console.WriteLine("MISSING COLOR Texture: " + path + texName + ".tif");
                            texDictionary.Add(texName + ".tif", colorTextureFallBack);
                        }

                        if (File.Exists(path + texName + "_normal.tif"))
                        {
                            texDictionary.Add(texName + "_normal.tif", TextureManager.LoadTexture(path + texName + "_normal.tif"));
                        }
                        else
                        {
                            Console.WriteLine("MISSING NORMAL Texture: " + path + texName + "_normal.tif");
                            texDictionary.Add(texName + "_normal.tif", normalTextureFallBack);
                        }

                        if (File.Exists(path + texName + "_additional.tif"))
                        {
                            texDictionary.Add(texName + "_additional.tif", TextureManager.LoadTexture(path + texName + "_additional.tif"));
                        }
                        else
                        {
                            Console.WriteLine("MISSING ADDITIONAL Texture: " + path + texName + "_additional.tif");
                            texDictionary.Add(texName + "_additional.tif", additionalTextureFallBack);
                        }


                    }

                    TexturePackEntity texturePack = new TexturePackEntity();

                    if (!shadowDefinedOnly)
                    {
                        texturePack.colorTexture = texDictionary[texName + ".tif"];
                        texturePack.normalTexture = texDictionary[texName + "_normal.tif"];
                        texturePack.additionalTexture = texDictionary[texName + "_additional.tif"];
                    }
                    else {
                        texturePack.colorTexture = colorTextureFallBack;
                        texturePack.normalTexture = normalTextureFallBack;
                        texturePack.additionalTexture = additionalTextureFallBack;
                    }

                    if (shadowTexName != "")
                    {
                        texturePack.shadowTexture = texDictionary[shadowTexName + ".png"];
                    }
                    else {
                        texturePack.shadowTexture = shadowTextureFallBack;
                    }

                    objTexDictionary.Add(elements[1], texturePack);

                }
            }
            file.Close();


            // Zweiter durchlauf: Objekte
            foreach (string singleLine in lines) {
                String[] elements = singleLine.Split(delimiterSpace);

                if (elements[0] == "obj")
                {
                    String objName = elements[1];
                    String cmpName = objName.Split(delimiterPoint)[0];
                    String colorComponent = "";
                    if (cmpName == "0_light") colorComponent = objName.Split(delimiterPoint)[1];

                    Matrix4 transform = Matrix4.Identity;
                    transform *= Matrix4.CreateRotationX(toFloat(elements[8]));
                    transform *= Matrix4.CreateRotationY(toFloat(elements[10]));
                    transform *= Matrix4.CreateRotationZ(-toFloat(elements[9]));
                    transform *= Matrix4.CreateScale(new Vector3(toFloat(elements[5]), toFloat(elements[7]), toFloat(elements[6])));
                    transform *= Matrix4.CreateTranslation(new Vector3(toFloat(elements[2]) * 10.0f, toFloat(elements[4]) * 10.0f, -toFloat(elements[3]) * 10.0f));

                    if (cmpName != "Sun" && cmpName != "Camera" && cmpName != "Light" && cmpName != "0_light")
                    {                      
                        Console.WriteLine(objName);
                        ObjLoaderObject3D obj = new ObjLoaderObject3D(path + objName + ".obj", 10.0f, false, false, false);
                        if (File.Exists(path + objName + "_shadow.obj")) {
                            ObjLoaderObject3D objShadow = new ObjLoaderObject3D(path + objName + "_shadow.obj", 10.0f, false, false, false);
                            obj.FillSecondUVLayer(objShadow);
                        }
                        obj.CreateVAO();
                  
                        DeferredMaterialSettings settings = new DeferredMaterialSettings();
                        settings.colorTexture = colorTextureFallBack;
                        settings.normalTexture = normalTextureFallBack;
                        settings.shadowTexture = shadowTextureFallBack;
                        settings.additionalTexture = additionalTextureFallBack;

                        if (objTexDictionary.ContainsKey(objName))
                        {
                            settings.colorTexture = objTexDictionary[objName].colorTexture;
                            settings.normalTexture = objTexDictionary[objName].normalTexture;
                            settings.shadowTexture = objTexDictionary[objName].shadowTexture;
                            settings.additionalTexture = objTexDictionary[objName].additionalTexture;
                        }
                        else
                        {
                            Console.WriteLine("NO TEXTURES ASSIGNED FOR OBJECT: " + objName);
                        }

                        bool found = false;
                        // schauen, ob in den Objekt-Optionen der Name vorhanden ist
                        foreach (TrackSceneObjectOptions option in objectOptions)
                        {
                            if (option.objectName == objName)
                            {
                                found = true;

                                settings.blendFactorSource = option.blendFactorSource;
                                settings.blendFactorDest = option.blendFactorDest;
                                settings.speed = option.speed;

                                if (option.material == null)
                                {
                                    if (option.receiveShadow)
                                    {
                                        octree.AddEntity(new OctreeEntity(obj, TrackMaterials.world, settings, transform));
                                    } else {
                                        octree.AddEntity(new OctreeEntity(obj, TrackMaterials.world, settings, transform));
                                    }
                                } else {
                                    octree.AddEntity(new OctreeEntity(obj, option.material, settings, transform));
                                }

                                if (option.castShadow) shadowOctree.AddEntity(new OctreeEntity(obj, TrackMaterials.world, settings, transform));

                                if (option.addToCollision)
                                {
                                    obj.AddToCollisionContainer(grid, transform, 3);
                                }
                            }
                        }
                        if (found == false)
                        {
                            octree.AddEntity(new OctreeEntity(obj, TrackMaterials.world, settings, transform));
                        }
                        
                
                       
                    }

                    if (cmpName == "0_light")
                    {
                        DeferredMaterialSettings settings = new DeferredMaterialSettings();
                        settings.lightRadius = toFloat(elements[5]) * 9.0f;

                        if (colorComponent != "")
                        {
                            Color color = ColorTranslator.FromHtml(colorComponent);
                            settings.lightColor = new Vector3(color.R / 255.0f, color.G / 255.0f, color.B / 255.0f);
                        }
                        lightOctree.AddEntity(new OctreeEntity(sphereLightObject, DeferredRendering.SphereLightMaterial, settings, transform));
                    }


                }
            }

    


        }


    }
}
