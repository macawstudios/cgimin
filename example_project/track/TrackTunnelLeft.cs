﻿using System.Collections.Generic;
using Engine.cgimin.object3d;
using OpenTK;

namespace lv1.track
{
    class TrackTunnelLeft : BaseObject3D
    {

        public TrackTunnelLeft(int start, int count, List<Vector2> form, int skip, int textureRepeat, float size)
        {
            List<float> us = new List<float>();

            float uLength = 0;
            for (int i = 0; i < form.Count - 1; i++) uLength += (form[i] - form[i + 1]).Length;

            float uIn = 0.0f;
            for (int i = 0; i < form.Count - 1; i++)
            {
                us.Add(uIn / uLength);
                uIn += (form[i] - form[i + 1]).Length;
            }
            us.Add(1.0f);

            int c = 0;
            for (int i = start; i <= start + count; i++)
            {
                c++;
                int index = i * skip;
                int indexFront = (i + 1) * skip;

                float vFront = 1.0f / textureRepeat * (c * skip);
                float vBack = 1.0f / textureRepeat * ((c + 1) * skip);

                Vector3 dir = (TrackPart.TrackInfo[index].rightBorderBack - TrackPart.TrackInfo[index].leftBorderBack).Normalized() * size;
                Vector3 dir2 = (TrackPart.TrackInfo[indexFront].rightBorderBack - TrackPart.TrackInfo[indexFront].leftBorderBack).Normalized() * size;
               
                for (int o = 0; o < form.Count - 1; o++)
                {

                    Vector3 p1 = TrackPart.TrackInfo[index].leftBorderBack + dir * form[o].X + TrackPart.TrackInfo[index].normalBack * form[o].Y * size;
                    Vector3 p2 = TrackPart.TrackInfo[index].leftBorderBack + dir * form[o + 1].X + TrackPart.TrackInfo[index].normalBack * form[o + 1].Y * size;
                    Vector3 p3 = TrackPart.TrackInfo[indexFront].leftBorderBack + dir2 * form[o + 1].X + TrackPart.TrackInfo[indexFront].normalBack * form[o + 1].Y * size;
                    Vector3 p4 = TrackPart.TrackInfo[indexFront].leftBorderBack + dir2 * form[o].X + TrackPart.TrackInfo[indexFront].normalBack * form[o].Y * size;

                    addTriangle(p1, p3, p2, new Vector2(us[o], vFront), new Vector2(us[o + 1], vBack), new Vector2(us[o + 1], vFront));
                    addTriangle(p1, p4, p3, new Vector2(us[o], vFront), new Vector2(us[o], vBack), new Vector2(us[o + 1], vBack));
                }



            }

            MidPointToCenter();

            CreateVAO();

        }

    }
}
