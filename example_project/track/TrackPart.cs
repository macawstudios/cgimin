﻿using System;
using System.Collections.Generic;
using Engine.cgimin.object3d;
using Engine.cgimin.collision;
using Engine.cgimin.helpers;
using Engine.cgimin.octree;
using Engine.cgimin.deferred;
using OpenTK;

namespace lv1.track
{
    public class TrackPart
    {
        // Beschreibung der Geometry für die Tunnel-Elemente, Seitenobjekte etc
        public struct TrackSectionInfo
        {
            public int startTrackPos;
            public int endTrackPos;
            public Vector3 midPoint;
        }

        // Beschreibung für die Items
        public struct ItemOnTrack
        {
            public int type;
            public float relX;
        }


        public class TrackPartInfo {

            public bool hasYGap;

            public bool hasLeftBorder;
            public bool hasRightBorder;

            public int leftBorderType;
            public int rightBorderType;

            public Vector3 normalFront;
            public Vector3 normalBack;

            public Vector3 leftStreetBack;
            public Vector3 leftStreetFront;
            public Vector3 rightStreetBack;
            public Vector3 rightStreetFront;

            public Vector3 leftBorderBack;
            public Vector3 leftBorderFront;
            public Vector3 rightBorderBack;
            public Vector3 rightBorderFront;

            public Vector3 midPosition;

            public bool[] brakeFields;
            public bool containsBrakeField;

            public List<ItemOnTrack> fieldSpecial;

        }

        // accelerators / jumps
        public struct TrackItem {
            public Vector3 position;
            public Matrix4 transform;
            public int type;

            // only used for cylinders
            public float radius;
            public Vector3 positionUpper;
        }

        // dataset
        public static float gravityForce = 0.005f;
        public static List<TrackPartInfo> TrackInfo = new List<TrackPartInfo>();
        public static List<TrackItem> TrackItems = new List<TrackItem>();
        public static List<TrackSectionInfo> TrackSections = new List<TrackSectionInfo>();

        // Enum für den Border Modus
        public enum BorderMode
        {
            border,
            noBorder,
            borderButNotShown
        }

        // Kalkulation für den Texture Repeat in X-Richtung
        private float[] uLeft1 = new float[6]  { 128.0f / 2048.0f, (128.0f + 256.0f) / 2048.0f, (128.0f + 512.0f) / 2048.0f, (128.0f + 768.0f) / 2048.0f, (128.0f + 1024.0f) / 2048.0f, (128.0f + 1280.0f) / 2048.0f };
        private float[] uRight1 = new float[6] { (128.0f + 256.0f) / 2048.0f, (128.0f + 512.0f) / 2048.0f, (128.0f + 768.0f) / 2048.0f, (128.0f + 1024.0f) / 2048.0f, (128.0f + 1280.0f) / 2048.0f, (128.0f + 1536.0f) / 2048.0f };

        // die Einzelteile des Tracks
        public BaseObject3D streetPart { get; private set; }

        //public BaseObject3D sidePart { get; private set; }

        public BaseObject3D brakeFieldPart { get; private set; }

        private bool containsBrakeField;
        private bool containsSidePart;

        // Die für das TrackObject gespeicherten texturen
        private int streetColor;
        private int streetNormal;


        // Die aktuellen Werte -> zum weiterzeichnen der Strecke
        private static Vector3 CurrentPosition = new Vector3(0, 0, 0);
        private static float CurrentAngle = 0;
        private static float CurrentScrewAngle = 0;
        private static float CurrentWidth = 5;

        // aktuelle Track Darstellungs-Einstellungen
        private static int currentTextureLoop;
        private static bool[] currentBrakeField;
        private static bool currentContainsBrakeField;
        private static BorderMode currentLeftBorder;
        private static BorderMode currentRightBorder;
        private static float currentSideHeight;

        // street textures
        private static int currentStreetTexture;
        private static int currentStreetNormal;


        // brake field texture
        private static int currentBrakeTexture;
        private static int currentBrakeNormal;

        // texture sub index count
        private static int bottomTexCount = 0;
        private static int sideTexCount = 0;

        // collision grid
        private CollisionGrid collision;

        // basis höhe
        private static float baseHeight = 0;

        // startpositionen
        public static int StartTrackPos = 0;

        public static float GetRelXFromStartID(int id, int columns)
        {
            return (1.0f / (columns + 1)) * (id % columns + 1);
        }

        public static float GetTrackPosFromStartID(int id, int columns)
        {
            float tr = StartTrackPos - (id / columns) * 12 + (id % columns);
            tr += TrackInfo.Count;
            return tr;
        }

        public static Vector3 GetPositionFromStartID(int id, int columns)
        {
            return GetPosition(GetTrackPosFromStartID(id, columns), GetRelXFromStartID(id, columns));
        }


        public static void SetDisplayOptions(float sideHeigh, int streetColorTex, int streetNormalTex, int textureLoop, bool[] brakeField, BorderMode leftBorder = BorderMode.border, BorderMode rightBorder = BorderMode.border)
        {
            currentSideHeight = sideHeigh;

            currentStreetTexture = streetColorTex;
            currentStreetNormal = streetNormalTex;

            sideTexCount = currentStreetNormal;

            currentTextureLoop = textureLoop;
            currentBrakeField = brakeField;
            currentLeftBorder = leftBorder;
            currentRightBorder = rightBorder;

            // check if there is a brake field, to prevent calculation at different places
            currentContainsBrakeField = false;
            for (int i = 0; i < currentBrakeField.Length; i++)
            {
                if (currentBrakeField[i]) currentContainsBrakeField = true;
            }

        }

        public static void SetBrakeFieldTexture(int brakeColor, int brakeNormals)
        {
            currentBrakeTexture = brakeColor;
            currentBrakeNormal = brakeNormals;
        }


        public static void resetBottomSubTexIndex()
        {
            bottomTexCount = 0;
        }

        public static void resetSideSubTexIndex()
        {
            sideTexCount = 0;
        }

        public static void SetBaseHeight(float height)
        {
            baseHeight = height;
            CurrentPosition.Y = height;
        }

        public static void SetStartRotation(float rotation)
        {
            CurrentAngle = rotation;
        }

        public static void SetStartWidth(float width)
        {
            CurrentWidth = width;
        }

        public static int GetCurrentLength()
        {
            return TrackInfo.Count - 1;
        }

        public static void AddAccelerator(int pos, float relX)
        {
            TrackItem accelerator = new TrackItem();
            accelerator.position = (TrackInfo[pos].leftStreetBack - TrackInfo[pos].rightStreetBack) * relX + TrackInfo[pos].rightStreetBack;
            Vector3 pos2 = (TrackInfo[pos].leftStreetFront - TrackInfo[pos].rightStreetFront) * relX + TrackInfo[pos].rightStreetFront;
            Matrix4 transform = new Matrix4();
            transform = Matrix4.CreateRotationY(-(float)Math.PI / 2.0f);
            transform *= Matrix4.LookAt(accelerator.position, pos2, Vector3.Cross(accelerator.position - pos2, (TrackInfo[pos].leftStreetBack - TrackInfo[pos].rightStreetBack)).Normalized()).Inverted();
            accelerator.type = 0;    
            accelerator.transform = transform;
            TrackItems.Add(accelerator);

            if (TrackInfo[pos].fieldSpecial == null) TrackInfo[pos].fieldSpecial = new List<ItemOnTrack>();

            ItemOnTrack itemOnTrack = new ItemOnTrack();
            itemOnTrack.relX = relX;
            itemOnTrack.type = 0;
            TrackInfo[pos].fieldSpecial.Add(itemOnTrack);
        }

        public static void AddJump(int pos, float relX)
        {
            TrackItem jump = new TrackItem();
            jump.position = (TrackInfo[pos].leftStreetBack - TrackInfo[pos].rightStreetBack) * relX + TrackInfo[pos].rightStreetBack;
            Vector3 pos2 = (TrackInfo[pos].leftStreetFront - TrackInfo[pos].rightStreetFront) * relX + TrackInfo[pos].rightStreetFront;
            Matrix4 transform = new Matrix4();
            transform = Matrix4.CreateRotationY(-(float)Math.PI / 2.0f);
            transform *= Matrix4.LookAt(jump.position, pos2, Vector3.Cross(jump.position - pos2, (TrackInfo[pos].leftStreetBack - TrackInfo[pos].rightStreetBack)).Normalized()).Inverted();
            jump.type = 1;
            jump.transform = transform;
            TrackItems.Add(jump);

            if (TrackInfo[pos].fieldSpecial == null) TrackInfo[pos].fieldSpecial = new List<ItemOnTrack>();

            ItemOnTrack itemOnTrack = new ItemOnTrack();
            itemOnTrack.relX = relX;
            itemOnTrack.type = 1;
            TrackInfo[pos].fieldSpecial.Add(itemOnTrack);
        }


        public static void AddSmallCylinder(int pos, float relX)
        {
            TrackItem smallCylinder = new TrackItem();
            smallCylinder.radius = 1.0f;
            smallCylinder.position = (TrackInfo[pos].leftStreetBack - TrackInfo[pos].rightStreetBack) * relX + TrackInfo[pos].rightStreetBack;
            Vector3 pos2 = (TrackInfo[pos].leftStreetFront - TrackInfo[pos].rightStreetFront) * relX + TrackInfo[pos].rightStreetFront;
            Matrix4 transform = new Matrix4();
            transform = Matrix4.CreateRotationY(-(float)Math.PI / 2.0f);

            Vector3 upNormal = Vector3.Cross(smallCylinder.position - pos2, (TrackInfo[pos].leftStreetBack - TrackInfo[pos].rightStreetBack)).Normalized();
            transform *= Matrix4.LookAt(smallCylinder.position, pos2, upNormal).Inverted();

            smallCylinder.positionUpper = smallCylinder.position + upNormal * 10.0f;
            smallCylinder.type = 2;
            smallCylinder.transform = transform;
            TrackItems.Add(smallCylinder);

            if (TrackInfo[pos].fieldSpecial == null) TrackInfo[pos].fieldSpecial = new List<ItemOnTrack>();

            ItemOnTrack itemOnTrack = new ItemOnTrack();
            itemOnTrack.relX = relX;
            itemOnTrack.type = 1;
            TrackInfo[pos].fieldSpecial.Add(itemOnTrack);
        }



        public TrackPart(CollisionGrid collisionGrid, float endAngle, float endHeight, float endScrew, float endWidth, int count, bool linear, float yGap = -1, bool endPart = false)
        {
            containsBrakeField = false;
            containsSidePart = false;

            streetColor = currentStreetTexture;
            streetNormal = currentStreetNormal;

            if (endPart) count = (int)CurrentPosition.Length;

            // Einzelteile
            streetPart = new BaseObject3D();
            //sidePart = new BaseObject3D();
            brakeFieldPart = new BaseObject3D();

            // track section info
            TrackSectionInfo trackSectionInfo = new TrackSectionInfo();
            trackSectionInfo.startTrackPos = TrackInfo.Count;
            trackSectionInfo.endTrackPos = trackSectionInfo.startTrackPos + count;

            collision = collisionGrid;
            if (yGap > 0) CurrentPosition.Y -= yGap;
            endHeight += baseHeight;

            Vector3 position = CurrentPosition;
            float angleDelta = (endAngle - CurrentAngle) / count;
            Matrix3 rotMat = new Matrix3();
            float height = position.Y;

           
            for (int i = 0; i < count; i++) {

                Vector3 front = new Vector3(0, 0, 1.0f);
                Vector3 left = new Vector3(1.0f, 0, 0);
                Vector3 left2 = new Vector3(1.0f, 0, 0);
                Vector3 normal = new Vector3(0, 1.0f, 0);
                Vector3 normal2 = new Vector3(0, 1.0f, 0);
                Vector3 upStep = new Vector3(0, 0, 0);

                Vector3 nextPosForEnd = new Vector3();

                if (endPart == false)
                {
                    rotMat = Matrix3.CreateRotationZ(interpolation(CurrentScrewAngle, endScrew, i, count, linear) * (float)Math.PI / 180.0f);
                    left *= rotMat;
                    normal *= rotMat;

                    rotMat = Matrix3.CreateRotationZ(interpolation(CurrentScrewAngle, endScrew, i + 1, count, linear) * (float)Math.PI / 180.0f);
                    left2 *= rotMat;
                    normal2 *= rotMat;

                    rotMat = Matrix3.CreateRotationY((CurrentAngle + angleDelta * i) * (float)Math.PI / 180.0f);
                    left *= rotMat;
                    front *= rotMat;
                    normal *= rotMat;

                    rotMat = Matrix3.CreateRotationY((CurrentAngle + angleDelta * (i + 1)) * (float)Math.PI / 180.0f);
                    left2 *= rotMat;
                    normal2 *= rotMat;

                    upStep = new Vector3(0, interpolation(height, endHeight, i + 1, count, linear) -
                                            interpolation(height, endHeight, i, count, linear), 0);

                }
                else {
                    Vector3 eStart = CurrentPosition;
                    Vector3 eEnd = new Vector3(0, baseHeight, 0);

                    rotMat = Matrix3.CreateRotationY(CurrentAngle * (float)Math.PI / 180.0f);
                    left  *= rotMat;
                    left2 *= rotMat;
                    front *= rotMat;

                    Vector3 planeNormal = Vector3.Cross(left, normal).Normalized();

                    float planeD = Vector3.Dot(eEnd, left);
                    float leftDist = -GeometryHelpers.PlaneDistanceToPoint(left, planeD, eStart);

                    Vector3 leftStep = interpolation(0, leftDist, i, count, true) * left;

                    upStep = new Vector3(0, interpolation(eStart.Y, eEnd.Y, i + 1, count, true) -
                                            interpolation(eStart.Y, eEnd.Y, i,     count, true), 0);

                    planeD = Vector3.Dot(eEnd, front);
                    float toGoDistance = GeometryHelpers.PlaneDistanceToPoint(front, planeD, eStart);
                    nextPosForEnd = (new Vector3(eStart.X, 0, eStart.Z)) - front * (toGoDistance / count) * (i + 1) + leftStep + (new Vector3(0, interpolation(eStart.Y, eEnd.Y, i + 1 , count, true), 0));

                    front = new Vector3(nextPosForEnd.X, 0, nextPosForEnd.Z) - new Vector3(position.X, 0, position.Z);
                }

                float left1W = interpolation(CurrentWidth, endWidth, i, count, linear) * 0.5f;
                float left2W = interpolation(CurrentWidth, endWidth, i + 1, count, linear) * 0.5f;

                addPart(position + left * left1W, position + left2 * left2W + front + upStep, position - left2 * left2W + front + upStep, position - left * left1W, Vector3.Cross(front, left).Normalized(), bottomTexCount);

                TrackInfo.Add(new TrackPartInfo());
      
                int infoIndex = TrackInfo.Count - 1;
                TrackPartInfo info = TrackInfo[infoIndex];
                if (yGap > 0 && i == 0) info.hasYGap = true; else info.hasYGap = false;
                if (i == count / 2) trackSectionInfo.midPoint = position;
                info.midPosition = position;
                info.leftStreetBack = position - left * left1W;
                info.leftStreetFront = position - left2 * left2W + front + upStep;
                info.rightStreetBack = position + left * left1W;
                info.rightStreetFront = position + left2 * left2W + front + upStep;
                info.normalBack = normal;
                info.normalFront = normal2;
                info.brakeFields = currentBrakeField;
                info.containsBrakeField = currentContainsBrakeField;
                info.hasLeftBorder = (currentLeftBorder != BorderMode.noBorder);
                info.hasRightBorder = (currentRightBorder != BorderMode.noBorder);
                info.leftBorderType = 0;
                info.rightBorderType = 0;

                TrackInfo[infoIndex] = info;
            
                if (currentLeftBorder == BorderMode.border) addSide(position + left * left1W, position + left2 * left2W + front + upStep, left, left2, true, normal, normal2, bottomTexCount);
                if (currentRightBorder == BorderMode.border) addSide(position - left * left1W, position - left2 * left2W + front + upStep, -left, -left2, false, normal, normal2, bottomTexCount);
                if (currentLeftBorder == BorderMode.borderButNotShown) addInvisibleSide(position + left * left1W, position + left2 * left2W + front + upStep, left, left2, true, normal, normal2);
                if (currentRightBorder == BorderMode.borderButNotShown) addInvisibleSide(position - left * left1W, position - left2 * left2W + front + upStep, -left, -left2, false, normal, normal2);

                if (currentLeftBorder == BorderMode.borderButNotShown || currentLeftBorder == BorderMode.noBorder) noBorder(position + left * left1W, position + left2 * left2W + front + upStep, true, normal, normal2);
                if (currentRightBorder == BorderMode.borderButNotShown || currentRightBorder == BorderMode.noBorder) noBorder(position - left * left1W, position - left2 * left2W + front + upStep, false, normal, normal2);

                if (endPart == false)
                {
                    position += front;
                    position += upStep;
                }
                else {
                    position = nextPosForEnd;
                }

                bottomTexCount++;
                sideTexCount++;

            }

            CurrentPosition = position;
            CurrentAngle = endAngle;
            CurrentScrewAngle = endScrew;
            CurrentWidth = endWidth;

            streetPart.MidPointToCenter();
            streetPart.CreateVAO();

            if (containsSidePart)
            {
                streetPart.MidPointToCenter();
                streetPart.CreateVAO();
            }

            if (containsBrakeField)
            {
                brakeFieldPart.MidPointToCenter();
                brakeFieldPart.CreateVAO();
            }
            
            TrackSections.Add(trackSectionInfo);

        }



        public void AddToOctree(Octree octree, int cubeReflectionTex, float fresnelValue, BaseDeferredMaterial material)
        {
            DeferredMaterialSettings streetMaterialSettings = new DeferredMaterialSettings();
            streetMaterialSettings.colorTexture = streetColor;
            streetMaterialSettings.normalTexture = streetNormal;
       
            octree.AddEntity(new OctreeEntity(streetPart, material, streetMaterialSettings, streetPart.Transformation));

            if (containsBrakeField)
            {
                DeferredMaterialSettings brakeMaterialSettings = new DeferredMaterialSettings();
                brakeMaterialSettings.colorTexture = currentBrakeTexture;
                brakeMaterialSettings.normalTexture = currentBrakeNormal;         
                TimeMeasure.Start();
                octree.AddEntity(new OctreeEntity(brakeFieldPart, material, brakeMaterialSettings, brakeFieldPart.Transformation));
                TimeMeasure.Show("BRAKE FIELD Octree Part to Octree :");
            }
        }


        public void AddToOctreeWithShadow(Octree octree, Octree shadowOctree, int cubeReflectionTex, float fresnelValue, BaseDeferredMaterial material, BaseDeferredMaterial shadowMaterial)
        {
            DeferredMaterialSettings streetMaterialSettings = new DeferredMaterialSettings();
            streetMaterialSettings.colorTexture = streetColor;
            streetMaterialSettings.normalTexture = streetNormal;
            octree.AddEntity(new OctreeEntity(streetPart, material, streetMaterialSettings, streetPart.Transformation));

            DeferredMaterialSettings shadowMaterialSettings = new DeferredMaterialSettings();
            shadowOctree.AddEntity(new OctreeEntity(streetPart, shadowMaterial, shadowMaterialSettings, streetPart.Transformation));

            //shadowOctree.AddEntity(new OctreeEntity(streetPart, shadowMaterial, shadowMaterialSettings, streetPart.Transformation));
        }


        private void addCut(Vector3 posLeft, Vector3 posRight, Vector3 normal, Vector3 left, bool front) {

            float nDown = -0.2f;
            float nUp =0.5f;
            float nIn = 0.25f;
            float halfLeft = 0.5f;

            Vector3 startPosLeft = posLeft + nDown * normal + left * nIn;
            Vector3 startPosRight = posRight + nDown * normal - left * nIn;
            if (front)
            {
                streetPart.addTriangle(startPosLeft - left * halfLeft, startPosLeft + normal * nUp, startPosLeft - normal * nUp, new Vector2(0.63f, 0.255f), new Vector2(0.64f, 0.255f), new Vector2(0.63f, 0.253f));
                streetPart.addTriangle(startPosLeft + left * halfLeft, startPosLeft - normal * nUp, startPosLeft + normal * nUp, new Vector2(0.63f, 0.255f), new Vector2(0.64f, 0.255f), new Vector2(0.63f, 0.253f));
                streetPart.addTriangle(startPosRight - left * halfLeft, startPosRight + normal * nUp, startPosRight - normal * nUp, new Vector2(0.63f, 0.255f), new Vector2(0.64f, 0.255f), new Vector2(0.63f, 0.253f));
                streetPart.addTriangle(startPosRight + left * halfLeft, startPosRight - normal * nUp, startPosRight + normal * nUp, new Vector2(0.63f, 0.255f), new Vector2(0.64f, 0.255f), new Vector2(0.63f, 0.253f));
                streetPart.addTriangle(posRight, posLeft, posRight + normal * -0.4f, new Vector2(0.63f, 0.255f), new Vector2(0.64f, 0.255f), new Vector2(0.63f, 0.253f));
                streetPart.addTriangle(posRight + normal * -0.4f, posLeft, posLeft + normal * -0.4f, new Vector2(0.63f, 0.255f), new Vector2(0.64f, 0.255f), new Vector2(0.63f, 0.253f));
            }
            else {
                streetPart.addTriangle(startPosLeft + normal * nUp, startPosLeft - left * halfLeft, startPosLeft - normal * nUp, new Vector2(0.63f, 0.255f), new Vector2(0.64f, 0.255f), new Vector2(0.63f, 0.253f));
                streetPart.addTriangle(startPosLeft - normal * nUp, startPosLeft + left * halfLeft, startPosLeft + normal * nUp, new Vector2(0.63f, 0.255f), new Vector2(0.64f, 0.255f), new Vector2(0.63f, 0.253f));
                streetPart.addTriangle(startPosRight + normal * nUp, startPosRight - left * halfLeft, startPosRight - normal * nUp, new Vector2(0.63f, 0.255f), new Vector2(0.64f, 0.255f), new Vector2(0.63f, 0.253f));
                streetPart.addTriangle(startPosRight - normal * nUp, startPosRight + left * halfLeft, startPosRight + normal * nUp, new Vector2(0.63f, 0.255f), new Vector2(0.64f, 0.255f), new Vector2(0.63f, 0.253f));
                streetPart.addTriangle(posLeft, posRight, posRight + normal * -0.4f, new Vector2(0.63f, 0.255f), new Vector2(0.64f, 0.255f), new Vector2(0.63f, 0.253f));
                streetPart.addTriangle(posLeft, posRight + normal * -0.4f, posLeft + normal * -0.4f, new Vector2(0.63f, 0.255f), new Vector2(0.64f, 0.255f), new Vector2(0.63f, 0.253f));
            }
        }

        private float interpolation(float start, float end, int i, int count, bool smooth)
        {
            if (!smooth) return(end - start) / count * i + start;
            return (end - start) * (1.0f - ((float)Math.Sin((float)i / count * Math.PI - Math.PI * 0.5f) * -0.5f + 0.5f)) + start;
        }

        private Vector3 interpolationVector(Vector3 start, Vector3 end, int i, int count, bool smooth)
        {
            if (!smooth) return (end - start) / count * i + start;
            return (end - start) * (1.0f - ((float)Math.Sin((float)i / count * Math.PI - Math.PI * 0.5f) * -0.5f + 0.5f)) + start;
        }

        private void addPart(Vector3 p1, Vector3 p2, Vector3 p3, Vector3 p4, Vector3 normal, int part)
        {
            int loop = currentTextureLoop;
            float startV = 1.0f - (float)(part % loop) / loop;
            float partAdd = 1.0f / loop;

            float upY = startV ;
            float downY = startV + partAdd;

            for (int i = 0; i < 6; i++)
            {
                Vector3 subP3 = (p2 - p3) / 6 * i + p3;
                Vector3 subP2 = (p2 - p3) / 6 * (i+1) + p3;
                Vector3 subP4 = (p1 - p4) / 6 * i + p4;
                Vector3 subP1 = (p1 - p4) / 6 * (i+1) + p4;

                if (currentBrakeField[5-i])
                {
                    containsBrakeField = true;

                    collision.AddTriangle(subP3, subP2, subP1, 1);
                    collision.AddTriangle(subP3, subP1, subP4, 1);

                    brakeFieldPart.addTriangle(subP3, subP2, subP1, new Vector2(0, 0), new Vector2(1, 0), new Vector2(1, 1));
                    brakeFieldPart.addTriangle(subP3, subP1, subP4, new Vector2(0, 0), new Vector2(1, 1), new Vector2(0, 1));

                    // addTriangleSub(subP3, subP2, subP1, normal, normal, normal, new Vector2(0.0f, 0.0f), new Vector2(0.25f, 0.0f), new Vector2(0.25f, 0.25f));
                    // addTriangleSub(subP3, subP1, subP4, normal, normal, normal, new Vector2(0.0f, 0.0f), new Vector2(0.25f, 0.25f), new Vector2(0.0f, 0.25f));

                }
                else
                {
                    collision.AddTriangle(subP3, subP2, subP1, 0);
                    collision.AddTriangle(subP3, subP1, subP4, 0);

                    streetPart.addTriangle(subP3, subP2, subP1, normal, normal, normal, new Vector2(uLeft1[i], upY), new Vector2(uRight1[i], upY), new Vector2(uRight1[i], downY));
                    streetPart.addTriangle(subP3, subP1, subP4, normal, normal, normal, new Vector2(uLeft1[i], upY), new Vector2(uRight1[i], downY), new Vector2(uLeft1[i], downY));
   
                }
            }

        }

        private void addSide(Vector3 pos1, Vector3 pos2, Vector3 left1, Vector3 left2, Boolean isLeft, Vector3 normal1, Vector3 normal2, int count)
        {
            containsSidePart = true;

            Vector3 pos1En = pos1 + left1 * 2.0f;
            Vector3 pos2En = pos2 + left2 * 2.0f;

            int infoIndex = TrackInfo.Count - 1;
            TrackPartInfo info = TrackInfo[infoIndex];

            float nDown = -1.0f;
            float nUp = currentSideHeight;
            float halfLeft = 0.5f;

            Vector3 startPos1 = pos1;
            Vector3 startPos2 = pos2;

            Vector3 startPosDown1 = pos1 + nDown * normal1;
            Vector3 startPosDown2 = pos2 + nDown * normal2;

            float vSide1 = 1.0f - (float)((count-1) % currentTextureLoop) / currentTextureLoop;
            float vSide2 = vSide1 - (1.0f / currentTextureLoop);


            if (isLeft == false)
            {
                
                collision.AddTriangle(pos1En - normal1 * 0.5f, pos1En + normal1 * 0.5f, pos2En + normal2 * 0.5f, 2);
                collision.AddTriangle(pos1En - normal1 * 0.5f, pos2En + normal2 * 0.5f, pos2En - normal2 * 0.5f, 2);
                collision.AddTriangle(pos1En, pos2, pos1, 0);
                collision.AddTriangle(pos1En, pos2En, pos2, 0);

                streetPart.addTriangle(startPos1 + left1 * halfLeft - normal1 * nUp,
                            startPosDown1,
                            startPosDown2, new Vector2(1.0f, vSide1), new Vector2(0.9375f, vSide1), new Vector2(0.9375f, vSide2));

                collision.AddTriangle(streetPart.Positions[streetPart.Positions.Count - 3], streetPart.Positions[streetPart.Positions.Count - 2], streetPart.Positions[streetPart.Positions.Count - 1], 3);


                streetPart.addTriangle(startPosDown2,
                            startPos2 + left2 * halfLeft - normal2 * nUp,
                            startPos1 + left1 * halfLeft - normal1 * nUp, new Vector2(0.9375f, vSide2), new Vector2(1.0f, vSide2), new Vector2(1.0f, vSide1));

                collision.AddTriangle(streetPart.Positions[streetPart.Positions.Count - 3], streetPart.Positions[streetPart.Positions.Count - 2], streetPart.Positions[streetPart.Positions.Count - 1], 3);



                streetPart.addTriangle(startPos1 + left1 * halfLeft * 2,
                            startPos1 + left1 * halfLeft - normal1 * nUp,
                            startPos2 + left2 * halfLeft * 2, new Vector2(1.0f, vSide1), new Vector2(0.9375f, vSide1), new Vector2(1.0f, vSide2));

                collision.AddTriangle(streetPart.Positions[streetPart.Positions.Count - 3], streetPart.Positions[streetPart.Positions.Count - 2], streetPart.Positions[streetPart.Positions.Count - 1], 3);

                streetPart.addTriangle(startPos2 + left2 * halfLeft - normal2 * nUp,
                            startPos2 + left2 * halfLeft * 2,
                            startPos1 + left1 * halfLeft - normal1 * nUp, new Vector2(0.9375f, vSide2), new Vector2(1.0f, vSide2), new Vector2(0.9375f, vSide1));

                collision.AddTriangle(streetPart.Positions[streetPart.Positions.Count - 3], streetPart.Positions[streetPart.Positions.Count - 2], streetPart.Positions[streetPart.Positions.Count - 1], 3);

                // direkte seite an der strecke
                info.rightBorderBack = startPos1 + left1 * halfLeft + normal1 * nUp;
                info.rightBorderFront = startPos2 + left2 * halfLeft + normal2 * nUp;

                streetPart.addTriangle(startPos1,
                            startPos1 + left1 * halfLeft + normal1 * nUp,
                            startPos2, new Vector2(0.0625f, vSide1), new Vector2(0, vSide1), new Vector2(0.0625f, vSide2));

                collision.AddTriangle(streetPart.Positions[streetPart.Positions.Count - 3], streetPart.Positions[streetPart.Positions.Count - 2], streetPart.Positions[streetPart.Positions.Count - 1], 3);

                streetPart.addTriangle(startPos2 + left2 * halfLeft + normal2 * nUp,
                            startPos2,
                            startPos1 + left1 * halfLeft + normal1 * nUp, new Vector2(0, vSide2), new Vector2(0.0625f, vSide2), new Vector2(0, vSide1));

                collision.AddTriangle(streetPart.Positions[streetPart.Positions.Count - 3], streetPart.Positions[streetPart.Positions.Count - 2], streetPart.Positions[streetPart.Positions.Count - 1], 3);

                streetPart.addTriangle(startPos1 + left1 * halfLeft + normal1 * nUp,
                            startPos1 + left1 * halfLeft * 2,
                            startPos2 + left2 * halfLeft * 2, new Vector2(0.875f, vSide1), new Vector2(0.9375f, vSide1), new Vector2(0.9375f, vSide2));

                collision.AddTriangle(streetPart.Positions[streetPart.Positions.Count - 3], streetPart.Positions[streetPart.Positions.Count - 2], streetPart.Positions[streetPart.Positions.Count - 1], 3);

                streetPart.addTriangle(startPos2 + left2 * halfLeft * 2,
                            startPos2 + left2 * halfLeft + normal2 * nUp,
                            startPos1 + left1 * halfLeft + normal1 * nUp, new Vector2(0.9375f, vSide2), new Vector2(0.875f, vSide2), new Vector2(0.875f, vSide1));

                collision.AddTriangle(streetPart.Positions[streetPart.Positions.Count - 3], streetPart.Positions[streetPart.Positions.Count - 2], streetPart.Positions[streetPart.Positions.Count - 1], 3);
                
            }
            else {

                collision.AddTriangle(pos1En - normal1 * 0.5f, pos2En + normal2 * 0.5f, pos1En + normal1 * 0.5f, 2);
                collision.AddTriangle(pos1En - normal1 * 0.5f, pos2En - normal2 * 0.5f, pos2En + normal2 * 0.5f, 2);
                collision.AddTriangle(pos1En, pos1, pos2, 0);
                collision.AddTriangle(pos1En, pos2, pos2En, 0);

                streetPart.addTriangle(startPosDown1,
                            startPos1 + left1 * halfLeft - normal1 * nUp,                            
                            startPosDown2, new Vector2(0.9375f, vSide1), new Vector2(1.0f, vSide1), new Vector2(0.9375f, vSide2));

                collision.AddTriangle(streetPart.Positions[streetPart.Positions.Count - 3], streetPart.Positions[streetPart.Positions.Count - 2], streetPart.Positions[streetPart.Positions.Count - 1], 3);

                streetPart.addTriangle(startPos2 + left2 * halfLeft - normal2 * nUp,
                            startPosDown2,
                            startPos1 + left1 * halfLeft - normal1 * nUp, new Vector2(1.0f, vSide2), new Vector2(0.9375f, vSide2), new Vector2(1.0f, vSide1));

                collision.AddTriangle(streetPart.Positions[streetPart.Positions.Count - 3], streetPart.Positions[streetPart.Positions.Count - 2], streetPart.Positions[streetPart.Positions.Count - 1], 3);


                streetPart.addTriangle(startPos1 + left1 * halfLeft - normal1 * nUp,
                            startPos1 + left1 * halfLeft * 2,
                            startPos2 + left2 * halfLeft * 2, new Vector2(0.9375f, vSide1), new Vector2(1.0f, vSide1), new Vector2(1.0f, vSide2));

                collision.AddTriangle(streetPart.Positions[streetPart.Positions.Count - 3], streetPart.Positions[streetPart.Positions.Count - 2], streetPart.Positions[streetPart.Positions.Count - 1], 3);

                streetPart.addTriangle(startPos2 + left2 * halfLeft * 2,
                            startPos2 + left2 * halfLeft - normal2 * nUp,
                            startPos1 + left1 * halfLeft - normal1 * nUp, new Vector2(1.0f, vSide2), new Vector2(0.9375f, vSide2), new Vector2(0.9375f, vSide1));

                collision.AddTriangle(streetPart.Positions[streetPart.Positions.Count - 3], streetPart.Positions[streetPart.Positions.Count - 2], streetPart.Positions[streetPart.Positions.Count - 1], 3);

                // direkte Seite an der strecke
                info.leftBorderBack = startPos1 + left1 * halfLeft + normal1 * nUp;
                info.leftBorderFront = startPos2 + left2 * halfLeft + normal2 * nUp;

                streetPart.addTriangle(startPos1 + left1 * halfLeft + normal1 * nUp,
                            startPos1,
                            startPos2, new Vector2(0.875f, vSide1), new Vector2(0.8125f, vSide1), new Vector2(0.8125f, vSide2));

                collision.AddTriangle(streetPart.Positions[streetPart.Positions.Count - 3], streetPart.Positions[streetPart.Positions.Count - 2], streetPart.Positions[streetPart.Positions.Count - 1], 3);

                streetPart.addTriangle(startPos2,
                            startPos2 + left2 * halfLeft + normal2 * nUp,
                            startPos1 + left1 * halfLeft + normal1 * nUp, new Vector2(0.8125f, vSide2), new Vector2(0.875f, vSide2), new Vector2(0.875f, vSide1));

                collision.AddTriangle(streetPart.Positions[streetPart.Positions.Count - 3], streetPart.Positions[streetPart.Positions.Count - 2], streetPart.Positions[streetPart.Positions.Count - 1], 3);



                streetPart.addTriangle(startPos1 + left1 * halfLeft * 2,
                            startPos1 + left1 * halfLeft + normal1 * nUp,
                            startPos2 + left2 * halfLeft * 2, new Vector2(0.9375f, vSide1), new Vector2(0.875f, vSide1), new Vector2(0.9375f, vSide2));

                collision.AddTriangle(streetPart.Positions[streetPart.Positions.Count - 3], streetPart.Positions[streetPart.Positions.Count - 2], streetPart.Positions[streetPart.Positions.Count - 1], 3);

                streetPart.addTriangle(startPos2 + left2 * halfLeft + normal2 * nUp,
                            startPos2 + left2 * halfLeft * 2,
                            startPos1 + left1 * halfLeft + normal1 * nUp, new Vector2(0.875f, vSide2), new Vector2(0.9375f, vSide2), new Vector2(0.875f, vSide1));

                collision.AddTriangle(streetPart.Positions[streetPart.Positions.Count - 3], streetPart.Positions[streetPart.Positions.Count - 2], streetPart.Positions[streetPart.Positions.Count - 1], 3);
                

            }
            TrackInfo[infoIndex] = info;
        }


        private void noBorder(Vector3 pos1, Vector3 pos2, bool isLeft, Vector3 normal1, Vector3 normal2)
        {

            int infoIndex = TrackInfo.Count - 1;
            TrackPartInfo info = TrackInfo[infoIndex];

            if (isLeft)
            {
                streetPart.addTriangle(pos1, pos2, pos1 - normal1 * 0.4f, new Vector2(0, 0), new Vector2(1, 0), new Vector2(1, 1));
                streetPart.addTriangle(pos1 - normal1 * 0.4f, pos2, pos2 - normal2 * 0.4f, new Vector2(0, 0), new Vector2(1, 0), new Vector2(1, 1));
                info.leftBorderBack = pos1;
                info.leftBorderFront = pos2;

            }
            else {
                streetPart.addTriangle(pos2, pos1, pos1 - normal1 * 0.4f, new Vector2(0, 0), new Vector2(1, 0), new Vector2(1, 1));
                streetPart.addTriangle(pos1 - normal1 * 0.4f, pos2 - normal2 * 0.4f, pos2, new Vector2(0, 0), new Vector2(1, 0), new Vector2(1, 1));
                info.rightBorderBack = pos1;
                info.rightBorderFront = pos2;
            }
        }


        private Vector2 vLerp(Vector2 v1, Vector2 v2, float lerp) {
            return (v2 - v1) * lerp + v1;
        }


        private void addInvisibleSide(Vector3 pos1, Vector3 pos2, Vector3 left1, Vector3 left2, Boolean isLeft, Vector3 normal1, Vector3 normal2)
        {

            Vector3 pos1En = pos1 + left1 * 2.0f;
            Vector3 pos2En = pos2 + left2 * 2.0f;

            int infoIndex = TrackInfo.Count - 1;
            TrackPartInfo info = TrackInfo[infoIndex];

            if (isLeft == false)
            {
                collision.AddTriangle(pos1En - normal1, pos1En + normal1, pos2En + normal2, 2);
                collision.AddTriangle(pos1En - normal1, pos2En + normal2, pos2En - normal2, 2);
                collision.AddTriangle(pos1En, pos2, pos1, 0);
                collision.AddTriangle(pos1En, pos2En, pos2, 0);

                info.rightBorderBack = pos1;// + left1;// * halfLeft + normal1 * nUp;
                info.rightBorderFront = pos2;// + left2;// * halfLeft + normal2 * nUp;

            }
            else
            {
                collision.AddTriangle(pos1En - normal1, pos2En + normal2, pos1En + normal1, 2);
                collision.AddTriangle(pos1En - normal1, pos2En - normal2, pos2En + normal2, 2);
                collision.AddTriangle(pos1En, pos1, pos2, 0);
                collision.AddTriangle(pos1En, pos2, pos2En, 0); 

                info.leftBorderBack = pos1;
                info.leftBorderFront = pos2;
            }

            TrackInfo[infoIndex] = info;
        }

        // ---
        public static Vector3 GetPosition(float mTrackPos, float mRelXPos)
        {
            float gTrackPos = mTrackPos;
            if (gTrackPos < 0) gTrackPos += TrackPart.TrackInfo.Count;
            int trPos = (int)Math.Floor(gTrackPos) % TrackPart.TrackInfo.Count;
            Vector3 back = (TrackPart.TrackInfo[trPos].rightStreetBack - TrackPart.TrackInfo[trPos].leftStreetBack) * mRelXPos + TrackPart.TrackInfo[trPos].leftStreetBack;
            Vector3 front = (TrackPart.TrackInfo[trPos].rightStreetFront - TrackPart.TrackInfo[trPos].leftStreetFront) * mRelXPos + TrackPart.TrackInfo[trPos].leftStreetFront;
            float subPos = (gTrackPos % TrackPart.TrackInfo.Count) - trPos;
            return (front - back) * subPos + back;
        }

        public static Vector3 GetDirectFront(float mTrackPos, float mTrackPosFront, float mRelXPos)
        {
            return (GetPosition(mTrackPosFront, mRelXPos) - GetPosition(mTrackPos, mRelXPos)).Normalized();
        }

        public static Vector3 GetUp(float mTrackPos, float mRelXPos)
        {
            Vector3 p1 = GetPosition(mTrackPos, mRelXPos - 0.1f);
            Vector3 p2 = GetPosition(mTrackPos + 0.1f, mRelXPos);
            Vector3 p3 = GetPosition(mTrackPos, mRelXPos + 0.1f);
            return Vector3.Cross((p2 - p1), (p3 - p2)).Normalized();
        }

        public static int GetNearestTrackIndex(Vector3 p)
        {
            float maxDist = float.MaxValue;
            int index = -1;

            for (int i = 0; i < TrackSections.Count; i++)
            {
                float l = (TrackSections[i].midPoint - p).LengthSquared;
                if (l < maxDist)
                {
                    index = i;
                    maxDist = l;
                }
            }

            maxDist = float.MaxValue;
            int infoIndex = -1;
            for (int i = -5; i < 5; i++)
            {
                int subIndex = index + i;
                if (subIndex < 0) subIndex += TrackSections.Count;
                subIndex = subIndex % TrackSections.Count;

                for (int o = TrackSections[subIndex].startTrackPos; o < TrackSections[subIndex].endTrackPos; o++) {
                    float l = (TrackInfo[o].midPosition - p).LengthSquared;
                    if (l < maxDist)
                    {
                        infoIndex = o;
                        maxDist = l;
                    }
                }
            }

            return infoIndex;

        }


    }
}
