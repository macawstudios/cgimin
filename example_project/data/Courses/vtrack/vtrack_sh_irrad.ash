# Generated with Lys by Knald Technologies, LLC - http://knaldtech.com
# The following triplets are spherical harmonic coefficients for RGB in linear color space.

l=0:
m=0: 1.430959 2.443947 4.266936

l=1:
m=-1: 0.307365 0.772442 1.633374
m=0: 0.158996 0.217047 0.303772
m=1: -0.000000 -0.000000 0.000000

l=2:
m=-2: -0.000000 0.000000 0.000000
m=-1: 0.055733 0.080227 0.121879
m=0: 0.150291 0.236858 0.312770
m=1: -0.000000 -0.000000 -0.000000
m=2: 0.127965 0.222750 0.300704
