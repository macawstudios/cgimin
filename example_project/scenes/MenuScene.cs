﻿using Engine.cgimin.gui;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenTK;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;
using Engine.cgimin.texture;
using Engine.cgimin.skybox;
using Engine.cgimin.camera;
using Engine.cgimin.shadowmappingcascaded;
using lv1.guielements;

namespace lv1.scenes
{
    class MenuScene
    {
        private static BitmapFont testFont;
        private static GUIContainer mainMenuContainer;

        // callbacks
        private static Action startRaceCB;

        public static void LoadAndInit(Action startRaceCallBack)
        {
            startRaceCB = startRaceCallBack;

            testFont = new BitmapFont("data/font/abel_normal.fnt", "data/font/abel_normal.png");
            int GUITex = TextureManager.LoadTexture("data/gui/gui.png");

            GUITextElement text1 = new GUITextElement(testFont);
            text1.y = 400;
            text1.Text = "Main Menu";
            text1.x = -text1.CalcTextWidth() / 2;

            SimpleTextButton button1 = new SimpleTextButton(300, "b1", "button1", testFont, GUITex, buttonClicked);
            button1.y = 140;
            button1.x = -150;

            SimpleTextButton button2 = new SimpleTextButton(300, "b2", "button2", testFont, GUITex, buttonClicked);
            button2.y = 70;
            button2.x = -150;

            SimpleTextButton button3 = new SimpleTextButton(300, "b3", "button3", testFont, GUITex, buttonClicked);
            button3.y = 0;
            button3.x = -150;

            button1.SetNavigation(null, "b2", null, null);
            button2.SetNavigation("b1", "b3", null, null);
            button3.SetNavigation("b2", null, null, null);
 
            mainMenuContainer = new GUIContainer();
            mainMenuContainer.AddChild(text1);

            mainMenuContainer.AddChild(button1);
            mainMenuContainer.AddChild(button2);
            mainMenuContainer.AddChild(button3);

        }

        private static void buttonClicked(string id)
        {
            if (id == "b1")
            {
                GUISystem.container.RemoveChild(mainMenuContainer);
                startRaceCB();
            }
        }


        public static void Update(GameWindow gameWindow)
        {
            if (!GUISystem.container.Contains(mainMenuContainer))
            {
                GUISystem.container.RemoveAllChildren();
                GUISystem.container.AddChild(mainMenuContainer);
            }

        }

        public static void Draw()
        {
            GL.ClearColor(Color4.Blue);
            GL.Clear(ClearBufferMask.DepthBufferBit | ClearBufferMask.ColorBufferBit);
        }
    }
}
