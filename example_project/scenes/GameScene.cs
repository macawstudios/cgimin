﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Engine.cgimin.light;
using Engine.cgimin.skybox;
using OpenTK;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;
using lv1.courses;
using Engine.cgimin.camera;
using Engine.cgimin.shadowmappingcascaded;
using Engine.cgimin.gui;
using Engine.cgimin.texture;
using Engine.cgimin.postprocessing;
using Engine.cgimin.deferred;
using lv1.gamestate;
using OpenTK.Input;
using lv1.track;
using Engine.cgimin.helpers;

namespace lv1.scenes
{
    public class GameScene
    {
        public static float GeneralDebugFloatValue = 1.2f;
        public static float GeneralDebugFloatIncrementValue = 0.01f;
        public static float GeneralDebugFloat2Value = 0.08f;
        public static float GeneralDebugFloat2IncrementValue = 0.001f;

        public static float GeneralDebugFloat3Value = 0.4f;
        public static float GeneralDebugFloat3IncrementValue = 0.01f;

        public static bool GeneralDebugBooleanValue;

        // Vehicle Class
        private static OwnVehicle vehicle;

        // Course
        private static CourseBase demoCourse;

        private static BitmapFont testFont;
        private static GUIContainer hud;
        private static GUI9Grid bar;
        private static GUITextElement roundText;
        private static GUITextElement placeText;
        private static GUITextElement trackPosText;
        private static GUITextElement drawSpeedText;
        private static GUITextElement drawFPSText;
        private static GUITextElement updateSpeedText;
        private static bool guiAdded;

        private static bool paused = false;

        public static void LoadAndInit(int width, int height)
        {
            // Init track Materials
            TrackMaterials.InitMaterials();

            // Init deferred rendering
            DeferredRendering.Init(width, height);

            // Gui
            testFont = new BitmapFont("data/font/abel_normal.fnt", "data/font/abel_normal.png");
            int barBackTexture = TextureManager.LoadTexture("data/gui/bar_back.png");
            int barFrontTexture = TextureManager.LoadTexture("data/gui/bar_front.png");
            hud = new GUIContainer();
            bar = new GUI9Grid(64, 64, barFrontTexture, 0, 0, 64, 64, 10, 10, 10, 10);
            bar.Width = 200;
            bar.x = -950;
            bar.y = -350;
            GUI9Grid barBack = new GUI9Grid(64, 64, barBackTexture, 0, 0, 64, 64, 10, 10, 10, 10);
            barBack.Width = 200;
            barBack.x = -950;
            barBack.y = -350;
            hud.AddChild(barBack);
            hud.AddChild(bar);
            roundText = new GUITextElement(testFont);
            roundText.x = -950;
            roundText.y = 300;
            roundText.Text = "0";
            hud.AddChild(roundText);
            placeText = new GUITextElement(testFont);
            placeText.x = -950;
            placeText.y = 220;
            placeText.Text = "-";
            hud.AddChild(placeText);
            drawSpeedText = new GUITextElement(testFont);
            drawSpeedText.x = 650;
            drawSpeedText.y = 200;
            drawSpeedText.Text = "-";
            hud.AddChild(drawSpeedText);
            drawFPSText = new GUITextElement(testFont);
            drawFPSText.x = 650;
            drawFPSText.y = 120;
            drawFPSText.Text = "-";
            hud.AddChild(drawFPSText);
            updateSpeedText = new GUITextElement(testFont);
            updateSpeedText.x = 650;
            updateSpeedText.y = 40;
            updateSpeedText.Text = "-";
            hud.AddChild(updateSpeedText);
            trackPosText = new GUITextElement(testFont);
            trackPosText.x = -950;
            trackPosText.y = 10;
            trackPosText.Text = "-";
            hud.AddChild(trackPosText);

            guiAdded = false;

            GL.Enable(EnableCap.CullFace);

            demoCourse = new SecondCourse();
            demoCourse.LoadAndGenerate();

            
     
            // Eiegenes Fahrzeug
            int startColumns = 2;
            int startID = 29;
            vehicle = new OwnVehicle(demoCourse.collisionGrid, startID, startColumns);

            TimeMeasure.Start();
            // Andere Fahrzeuge            
            for (int i = 0; i < 30; i++)
            {
                int skill = 2;
                if (i > 10 && i < 20) skill = 1;
                if (i >= 20) skill = 1;

                if (i != startID)
                {
                    new OtherVehicle(i, startColumns, skill);
                }
            }
            TimeMeasure.Show("other vehicles loading..:");

            // Skybox
            SkyBox.Init("data/textures/skybox/Sky_Day Sun High SummerSky_Cam_FR.png", "data/textures/skybox/Sky_Day Sun High SummerSky_Cam_BK.png",
                        "data/textures/skybox/Sky_Day Sun High SummerSky_Cam_RT.png", "data/textures/skybox/Sky_Day Sun High SummerSky_Cam_LF.png",
                        "data/textures/skybox/Sky_Day Sun High SummerSky_Cam_UP.png", "data/textures/skybox/Sky_Day Sun High SummerSky_Cam_DN.png");
        }

        // test 
        private static int lrclimb = 0;
        private static void tick(GameWindow gameWindow)
        {

            float leftRight = GamePad.GetState(0).ThumbSticks.Left.X;

            
            
            int lrClimbToGo = 0;
            if (gameWindow.Keyboard[KeyMapping.SteerLeft]) lrClimbToGo = -10;
            if (gameWindow.Keyboard[KeyMapping.SteerRight]) lrClimbToGo = 10;
            if (lrclimb > lrClimbToGo) lrclimb--;
            if (lrclimb < lrClimbToGo) lrclimb++;
            leftRight = lrclimb / 10.0f;
            

            bool accel = gameWindow.Keyboard[KeyMapping.Throttle];
            if (GamePad.GetState(0).Buttons.X == ButtonState.Pressed) accel = true;

            bool roundhouseLeft = gameWindow.Keyboard[KeyMapping.StrikeRoundhouseLeft];
            if (GamePad.GetState(0).Buttons.Y == ButtonState.Pressed) roundhouseLeft = true;

            bool roundhouseRight = gameWindow.Keyboard[KeyMapping.StrikeRoundhouseRight];
            if (GamePad.GetState(0).Buttons.Y == ButtonState.Pressed) roundhouseRight = true;

            bool turbo = gameWindow.Keyboard[KeyMapping.UseTurboBooster];
            if (GamePad.GetState(0).Buttons.A == ButtonState.Pressed) turbo = true;

            bool lookBack = gameWindow.Keyboard[KeyMapping.LookBackwards];
            if (GamePad.GetState(0).Buttons.B == ButtonState.Pressed) lookBack = true;

            bool strikeLeft = gameWindow.Keyboard[KeyMapping.StrikeLeft];
            if (GamePad.GetState(0).Buttons.LeftShoulder == ButtonState.Pressed) strikeLeft = true;

            bool strikeRight = gameWindow.Keyboard[KeyMapping.StrikeRight];
            if (GamePad.GetState(0).Buttons.RightShoulder == ButtonState.Pressed) strikeRight = true;

            vehicle.Update(accel, leftRight, strikeLeft, strikeRight, roundhouseLeft, roundhouseRight, lookBack, turbo);

            for (int i = 0; i < OtherVehicle.Instances.Count; i++)
            {
                OtherVehicle.Instances[i].Update();
            }
        }

        public static void Update(GameWindow gameWindow)
        {
            keyboardState = gameWindow.Keyboard.GetState();

            processDebugKeys();

            if (!paused)
            {
                if (keyPressed(KeyMapping.Pause))
                {
                    paused = true;
                }
                tick(gameWindow);
            }
            else if (keyPressed(KeyMapping.Pause))
            {
                paused = false;
            }
            else
            {
                processPauseDebugKeys();
            }

            //if (keyPress(KeyMapping.StrikeRoundhouse)) OwnVehicle.Instance.DoExplodeAnimation();


            if (keyPressed(KeyMapping.LoadSaveSlot1))
            {
                LoadState(1);
                tick(gameWindow);
            }
            else if (keyPressed(KeyMapping.LoadSaveSlot2))
            {
                LoadState(2);
                tick(gameWindow);
            }
            else if (keyPressed(KeyMapping.LoadSaveSlot3))
            {
                LoadState(3);
                tick(gameWindow);
            } else if (keyPressed(KeyMapping.CycleCamera))
            {
                OwnVehicle.Instance.CycleCameraPresets();
            }
            processCameraKeys();

            lastKeyboardState = keyboardState;
        }
        
        private static void LoadState(int slot)
        {
            var bundle = GameState.Instance.GetGameState(slot);
            if (bundle != null)
            {
                vehicle = bundle.vehicle.InitializeFrom(vehicle);
                OwnVehicle.Instance = vehicle;
                var oldInstances = new List<OtherVehicle>(OtherVehicle.Instances);
                OtherVehicle.Instances.Clear();
                int i = 0;
                //currently relies on number of vehicles being the same
                foreach (OtherVehicle vehicle in bundle.otherVehicles)
                {
                    OtherVehicle.Instances.Add(vehicle.InitializeFrom(oldInstances[i++]));
                }
            }
        }

        static KeyboardState keyboardState, lastKeyboardState;

        private static bool keyPressed(Key key)
        {
            
            var nowPressed = keyboardState[key];
            var sameState = keyboardState[key] == lastKeyboardState[key];
            return (nowPressed && !sameState);
        }

        private static bool keyDown(Key key)
        {
            return keyboardState[key];
        }


        public static void Draw()
        {
            if (!guiAdded) {
                guiAdded = true;
                GUISystem.container.AddChild(hud);
            }

            bar.Width = (int)(vehicle.GetEnergy() * 2.0f);
            roundText.Text = vehicle.GetCurrentRound().ToString();
            placeText.Text = vehicle.GetPlacing().ToString();

            // Schatten 
            ShadowMappingCascaded.StartShadowMapping();
            for (int i = 0; i < 3; i++)
            {
                ShadowMappingCascaded.SetDepthTextureTarget(i);
                vehicle.DrawShadow();

                for (int o = 0; o < OtherVehicle.Instances.Count; o++)
                {
                    OtherVehicle.Instances[o].DrawShadow();
                }
                demoCourse.DrawShadows();
            }
            ShadowMappingCascaded.EndShadowMapping();

            Camera.SetDynamicFov(vehicle.Fov);
            Camera.SetLookAt(vehicle.CameraEye, vehicle.CameraTarget, vehicle.CameraUp);

            DeferredRendering.StartGBufferRendering();

            demoCourse.Draw(vehicle.speed * 0.35f);
            vehicle.Draw();

            for (int i = 0; i < OtherVehicle.Instances.Count; i++)
            {
                OtherVehicle.Instances[i].Draw();
            }


            DeferredRendering.DrawScreen(CourseBase.deferredScreneDescription, demoCourse.lightOctree, GlowRender, ColorRender, LightRender);

            drawSpeedText.Text = MainGame.MSForDraw.ToString() + " ms draw";
            if (MainGame.MSForDraw > 0) drawFPSText.Text = (1000 / MainGame.MSForDraw).ToString() + " fps draw";
            updateSpeedText.Text = MainGame.MSForUpdate.ToString() + " ms Update";

            trackPosText.Text = OwnVehicle.Instance.nearestTrackIndex.ToString() + " track pos.";

        }

        private static void LightRender()
        {
            
            for (int i = 0; i < OtherVehicle.Instances.Count; i++)
            {
                OtherVehicle.Instances[i].DrawLights();
            }

            vehicle.DrawLights();
        }

        private static void GlowRender()
        {
            // Explosion
            //if (!GeneralDebugBooleanValue)
            //{
                for (int i = 0; i < OtherVehicle.Instances.Count; i++)
                {
                    OtherVehicle.Instances[i].DrawExplosion();
                    OtherVehicle.Instances[i].DrawFire();
                }
                OwnVehicle.Instance.DrawExplosion();
                OwnVehicle.Instance.DrawFire();
            //}

            /*
            GL.Enable(EnableCap.Blend);
            GL.BlendFunc(BlendingFactorSrc.One, BlendingFactorDest.One);
            for (int i = 0; i < OtherVehicle.Instances.Count; i++)
            {
                OtherVehicle.Instances[i].vehicleObject.DrawLightTale(1.0f, i);
            }
            GL.Enable(EnableCap.Blend);
            */
        }

        private static void ColorRender()
        {


            // skybox
            GL.Disable(EnableCap.Blend);
            SkyBox.Draw();


            // Explosion
            //if (GeneralDebugBooleanValue)
            //{
                for (int i = 0; i < OtherVehicle.Instances.Count; i++)
                {
                    OtherVehicle.Instances[i].DrawExplosion();
                }
                OwnVehicle.Instance.DrawExplosion();
            //}

            // tales of vehicles
            GL.Enable(EnableCap.Blend);
            GL.BlendFunc(BlendingFactorSrc.One, BlendingFactorDest.One);
            for (int i = 0; i < OtherVehicle.Instances.Count; i++)
            {
                OtherVehicle.Instances[i].vehicleObject.DrawLightTale(1.0f, i);
                OtherVehicle.Instances[i].DrawFire();
            }
            OwnVehicle.Instance.DrawFire();
            OwnVehicle.Instance.DrawLightTale();
            GL.Enable(EnableCap.Blend);
            
        }


        private static void processDebugKeys()
        {
            if (keyDown(KeyMapping.DebugActionFloatVarPlus)) {
                GeneralDebugFloatValue += GeneralDebugFloatIncrementValue;
                Console.WriteLine(String.Format("GeneralDebugFloatValue: {0}", GeneralDebugFloatValue));                
            }
            else if (keyDown(KeyMapping.DebugActionFloatVarMinus))
            {
                GeneralDebugFloatValue -= GeneralDebugFloatIncrementValue;
                Console.WriteLine(String.Format("GeneralDebugFloatValue: {0}", GeneralDebugFloatValue));                
            }
            else if (keyDown(KeyMapping.DebugActionFloat2VarPlus))
            {
                GeneralDebugFloat2Value += GeneralDebugFloat2IncrementValue;
                Console.WriteLine(String.Format("GeneralDebugFloat2Value: {0}", GeneralDebugFloat2Value));
            }
            else if (keyDown(KeyMapping.DebugActionFloat2VarMinus))
            {
                GeneralDebugFloat2Value -= GeneralDebugFloat2IncrementValue;
                Console.WriteLine(String.Format("GeneralDebugFloat2Value: {0}", GeneralDebugFloat2Value));
            }
            else if (keyDown(KeyMapping.DebugActionFloat3VarPlus))
            {
                GeneralDebugFloat3Value += GeneralDebugFloat3IncrementValue;
                Console.WriteLine(String.Format("GeneralDebugFloat3Value: {0}", GeneralDebugFloat3Value));
            }
            else if (keyDown(KeyMapping.DebugActionFloat3VarMinus))
            {
                GeneralDebugFloat3Value -= GeneralDebugFloat3IncrementValue;
                Console.WriteLine(String.Format("GeneralDebugFloat3Value: {0}", GeneralDebugFloat3Value));
            }
            else if (keyDown(KeyMapping.DebugActionBoolVarTrue))
            {
                GeneralDebugBooleanValue = true;
                Console.WriteLine(String.Format("GeneralDebugBooleanValue: {0}", GeneralDebugBooleanValue));
            }
            else if (keyDown(KeyMapping.DebugActionBoolVarFalse))
            {
                GeneralDebugBooleanValue = false;
                Console.WriteLine(String.Format("GeneralDebugBooleanValue: {0}", GeneralDebugBooleanValue));
            }
        }

        private static void processPauseDebugKeys()
        {
            if (keyPressed(KeyMapping.SaveSaveSlot1))
            {
                GameState.Instance.SaveGameState(1, new GameStateBundle(vehicle, OtherVehicle.Instances));
            }
            else if (keyPressed(KeyMapping.SaveSaveSlot2))
            {
                GameState.Instance.SaveGameState(2, new GameStateBundle(vehicle, OtherVehicle.Instances));
            }
            else if (keyPressed(KeyMapping.SaveSaveSlot3))
            {
                GameState.Instance.SaveGameState(3, new GameStateBundle(vehicle, OtherVehicle.Instances));
            }
            else if (keyDown(KeyMapping.DebugVehiclePositionLeft))
            {
                OwnVehicle.Instance.TickDebugLeft();
            }
            else if (keyDown(KeyMapping.DebugVehiclePositionRight))
            {
                OwnVehicle.Instance.TickDebugRight();
            }
            else if (keyDown(KeyMapping.DebugVehiclePositionForward))
            {
                OwnVehicle.Instance.TickDebugForward();
            }
            else if (keyDown(KeyMapping.DebugVehiclePositionBackward))
            {
                OwnVehicle.Instance.TickDebugBackward();
            }
            else if (keyDown(KeyMapping.DebugVehiclePositionUp))
            {
                OwnVehicle.Instance.TickDebugUp();
            }
            else if (keyDown(KeyMapping.DebugVehiclePositionDown))
            {
                OwnVehicle.Instance.TickDebugDown();
            }
            else if (keyDown(KeyMapping.DebugVehicleSpeedIncrease))
            {
                OwnVehicle.Instance.TickDebugSpeedIncrease();
            }
            else if (keyDown(KeyMapping.DebugVehicleSpeedDecrease))
            {
                OwnVehicle.Instance.TickDebugSpeedDecrease();
            }
            else if (keyDown(KeyMapping.DebugVehicleRotateRight))
            {
                OwnVehicle.Instance.TickDebugRotateRight();
            }
            else if (keyDown(KeyMapping.DebugVehicleRotateLeft))
            {
                OwnVehicle.Instance.TickDebugRotateLeft();
            }
        }

        private static void processCameraKeys()
        {
            if (keyDown(KeyMapping.DebugCameraNearer))
            {
                OwnVehicle.Instance.TickDebugCameraNearer();
            }
            else if (keyDown(KeyMapping.DebugCameraFarther))
            { OwnVehicle.Instance.TickDebugCameraFarther(); }
            else if (keyDown(KeyMapping.DebugCameraAngleHigher))
            { OwnVehicle.Instance.TickDebugCameraAngleHigher(); }
            else if (keyDown(KeyMapping.DebugCameraAngleLower))
            { OwnVehicle.Instance.TickDebugCameraAngleLower(); }
            else if (keyDown(KeyMapping.DebugCameraHigher))
            { OwnVehicle.Instance.TickDebugCameraHigher(); }
            else if (keyDown(KeyMapping.DebugCameraLower))
            { OwnVehicle.Instance.TickDebugCameraLower(); }
            else if (keyDown(KeyMapping.DebugCameraInertiaHigher))
            { OwnVehicle.Instance.TickDebugEyeCameraInertiaHigher(); }
            else if (keyDown(KeyMapping.DebugCameraInertiaLower))
            { OwnVehicle.Instance.TickDebugEyeCameraInertiaLower(); }
            else if (keyDown(KeyMapping.DebugCameraTargetInertiaHigher))
            { OwnVehicle.Instance.TickDebugTargetCameraInertiaHigher(); }
            else if (keyDown(KeyMapping.DebugCameraTargetInertiaLower))
            { OwnVehicle.Instance.TickDebugTargetCameraInertiaLower(); }
        }

    }
}
