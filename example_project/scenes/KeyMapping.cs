﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenTK.Input;

namespace lv1.scenes
{
    class KeyMapping
    {
        public static Key Throttle { get; internal set; } = Key.Up;
        public static Key SteerLeft { get; internal set; } = Key.Left;
        public static Key SteerRight { get; internal set; } = Key.Right;
        public static Key StrikeLeft { get; internal set; } = Key.Q;
        public static Key StrikeRight { get; internal set; } = Key.E;
        public static Key StrikeRoundhouseLeft { get; internal set; } = Key.A;
        public static Key StrikeRoundhouseRight { get; internal set; } = Key.D;
        public static Key UseTurboBooster { get; internal set; } = Key.AltLeft;

        public static Key LookBackwards { get; internal set; } = Key.S;
        public static Key CycleCamera { get; internal set; } = Key.C;

        public static Key Pause { get; internal set; } = Key.F10;
        public static Key SaveSaveSlot1 { get; internal set; } = Key.F1;
        public static Key SaveSaveSlot2 { get; internal set; } = Key.F3;
        public static Key SaveSaveSlot3 { get; internal set; } = Key.F5;
        public static Key LoadSaveSlot1 { get; internal set; } = Key.F2;
        public static Key LoadSaveSlot2 { get; internal set; } = Key.F4;
        public static Key LoadSaveSlot3 { get; internal set; } = Key.F6;


        public static Key DebugActionBoolVarTrue { get; internal set; } = Key.Number1;
        public static Key DebugActionBoolVarFalse { get; internal set; } = Key.Number2;
        public static Key DebugActionFloatVarPlus { get; internal set; } = Key.Number3;
        public static Key DebugActionFloatVarMinus { get; internal set; } = Key.Number4;
        public static Key DebugActionFloat2VarPlus { get; internal set; } = Key.Number5;
        public static Key DebugActionFloat2VarMinus { get; internal set; } = Key.Number6;
        public static Key DebugActionFloat3VarPlus { get; internal set; } = Key.Number7;
        public static Key DebugActionFloat3VarMinus { get; internal set; } = Key.Number8;

        public static Key DebugVehiclePositionLeft { get; internal set; } = Key.H;
        public static Key DebugVehiclePositionRight { get; internal set; } = Key.K;
        public static Key DebugVehiclePositionForward { get; internal set; } = Key.U;
        public static Key DebugVehiclePositionBackward { get; internal set; } = Key.J;
        public static Key DebugVehiclePositionUp { get; internal set; } = Key.O;
        public static Key DebugVehiclePositionDown { get; internal set; } = Key.L;
        public static Key DebugVehicleRotateLeft { get; internal set; } = Key.Y;
        public static Key DebugVehicleRotateRight { get; internal set; } = Key.I;

        public static Key DebugVehicleSpeedIncrease { get; internal set; } = Key.F35;
        public static Key DebugVehicleSpeedDecrease { get; internal set; } = Key.F35;
        
        public static Key DebugCameraNearer { get; internal set; } = Key.F35;
        public static Key DebugCameraFarther { get; internal set; } = Key.F35;
        public static Key DebugCameraAngleHigher { get; internal set; } = Key.F35;
        public static Key DebugCameraAngleLower { get; internal set; } = Key.F35;
        public static Key DebugCameraHigher { get; internal set; } = Key.F35;
        public static Key DebugCameraLower { get; internal set; } = Key.F35;
        public static Key DebugCameraInertiaHigher { get; internal set; } = Key.F35;
        public static Key DebugCameraInertiaLower { get; internal set; } = Key.F35;
        public static Key DebugCameraTargetInertiaHigher { get; internal set; } = Key.F35;
        public static Key DebugCameraTargetInertiaLower { get; internal set; } = Key.F35;
    }

}
