﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lv1.vehicle
{
    public class CameraPreset
    {
        public float VehicleCameraInertiaCoefficient;
        public float VehicleCameraDistanceCoefficient;
        public float VehicleCameraHeightCoefficient;
        public float VehicleCameraAngleCoefficient;

        public float VehicleCameraEyeInertiaCoefficient;
        public float VehicleCameraTargetInertiaCoefficient;

        public float CrashCamFrequency;
        public float CrashCamIntensity;
        public float CrashCamSmoothing;
    }


    public class FPSCameraPreset : CameraPreset
    {
        public FPSCameraPreset()
        {
            VehicleCameraDistanceCoefficient= 0.55f;
            VehicleCameraHeightCoefficient = 0.5f;
            VehicleCameraAngleCoefficient = 0.45f;

            VehicleCameraEyeInertiaCoefficient = 1;
            VehicleCameraTargetInertiaCoefficient = 1;

            CrashCamIntensity = 0.08f;
            CrashCamSmoothing = 0.004f;
            CrashCamFrequency = 0.4f;
        }
    }

    public class NearCameraPreset : CameraPreset
    {
        public NearCameraPreset()
        {
            VehicleCameraDistanceCoefficient = 3.5f;
            VehicleCameraHeightCoefficient = 1.3f;
            VehicleCameraAngleCoefficient = 1.3f;

            VehicleCameraEyeInertiaCoefficient = 0.8f;
            VehicleCameraTargetInertiaCoefficient = 0.8f;

            CrashCamIntensity = 1.2f;
            CrashCamSmoothing = 0.08f;
            CrashCamFrequency = 0.4f;
        }
    }

    public class FarCameraPreset : CameraPreset
    {
        public FarCameraPreset()
        {
            VehicleCameraDistanceCoefficient = 4.675f;
            VehicleCameraHeightCoefficient = 2.85f;
            VehicleCameraAngleCoefficient = 1.725f;

            VehicleCameraEyeInertiaCoefficient = 0.225f;
            VehicleCameraTargetInertiaCoefficient = 0.645f;

            CrashCamIntensity = 1.2f;
            CrashCamSmoothing = 0.08f;
            CrashCamFrequency = 0.4f;
        }
    }

    public class ActionCameraPreset : CameraPreset
    {
        //Action-Effekt:
        public ActionCameraPreset()
        {
            VehicleCameraTargetInertiaCoefficient = 0.29f;
        }        
    }

}
