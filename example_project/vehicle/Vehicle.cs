﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Engine.cgimin.light;
using Engine.cgimin.object3d;
//using Engine.cgimin.material.cubereflectionfogcascaded;
//using Engine.cgimin.material.worldcascaded;
using Engine.cgimin.material.castshadow;
using Engine.cgimin.deferred.world;
using Engine.cgimin.deferred;
using Engine.cgimin.texture;
using OpenTK;
using Engine.cgimin.effects;
using lv1.track;

namespace lv1.vehicle
{
    class Vehicle
    {

        public struct TaleEntry {
            public Vector3 position;
            public Vector3 leftOut;
        }

        // tmp static

        private static BaseObject3D front = null;
        private static BaseObject3D mid = null;
        private static BaseObject3D back = null;
        private static BaseObject3D fire = null;
        private static int colorTextureID;
        private static int normalTextureID;

        private static int fireColorID;
        private static int fireAdditionalID;

        public Matrix4 frontTransform { get; private set; }
        public Matrix4 mid1Transform { get; private set; }
        public Matrix4 mid2Transform { get; private set; }
        public Matrix4 mid3Transform { get; private set; }
        public Matrix4 mid4Transform { get; private set; }
        public Matrix4 mid5Transform { get; private set; }
        public Matrix4 backTransform { get; private set; }

        public List<Vector3> comparePos { get; private set; }  = new List<Vector3>();
        public List<float> compareRad { get; private set; } = new List<float>();

        private DeferredWorldMaterial material;
        private CastShadowMaterial castShadowMaterial;

        private int cubeTextureID;

        private Matrix4 transform;

        private float baseScale;

        private int TALE_LENGTH = 25;
        private List<TaleEntry> lightTale = new List<TaleEntry>();

        private List<Vector3> taleColors;

        // base light Settings for Blink
        private Vector4 baseAmbient;
        private Vector4 baseDiffuse;

        // blink count animation
        private int blinkCount;

        // redBlink
        private bool isRedBlinking;
        private int redUpdate;

        // fire update
        private int fireUpdate;

        // explosion data static
        private static int explosionID = -1;
        private static int firelineID = -1;
        private static Explosion explosion;
        private static List<ExplosionLineData> ExplosionLines;
        private static List<ExplosionFireLine> ExplosionFireLines;

        private static ExplosionFireLine MainFireLine;


        // explosion update var
        private int explosionUpdate;
        private Matrix4 explosionTransform;
        private Vector3 explosionPosition;

        public Vehicle(int cubeTexture)
        {

            redUpdate = 0;
            fireUpdate = 0;

            if (explosionID == -1) {
                ExplosionLineData mainLineData = new ExplosionLineData(Vector3.UnitY * 0.015f, 0);
                MainFireLine = new ExplosionFireLine(mainLineData.Positions, 50);

                explosion = new Explosion();
                explosionID = TextureManager.LoadTexture("data/course1/explosion.tif");
                firelineID = TextureManager.LoadTexture("data/course1/fireline.tif");



                ExplosionLines = new List<ExplosionLineData>();
                ExplosionFireLines = new List<ExplosionFireLine>();
                Random rnd = new Random();
                for (int i = 0; i < 10; i++){
                    Vector3 dir = new Vector3(rnd.Next(-1000, 1000), rnd.Next(0, 1500), rnd.Next(-1000, 1000));
                    dir.Normalize();
                    dir *= 0.3f;
                    ExplosionLines.Add(new ExplosionLineData(dir, TrackPart.gravityForce));

                    ExplosionFireLines.Add(new ExplosionFireLine(ExplosionLines[i].Positions, 40));

                }



            }
            explosionUpdate = 0;

            // Ligh tale
            LighTale.Initialize(TALE_LENGTH);

            blinkCount = 0;

            taleColors = new List<Vector3>();
            taleColors.Add(new Vector3(1.0f, 0.0f, 0.0f));
            taleColors.Add(new Vector3(0.0f, 1.0f, 0.0f));
            taleColors.Add(new Vector3(0.0f, 0.0f, 1.0f));
            taleColors.Add(new Vector3(1.0f, 0.0f, 1.0f));
            taleColors.Add(new Vector3(0.0f, 1.0f, 1.0f));
            taleColors.Add(new Vector3(1.0f, 1.0f, 0.0f));
            

            cubeTextureID = cubeTexture;

            material = new DeferredWorldMaterial();
            castShadowMaterial = new CastShadowMaterial();

            baseScale = 0.4f;

            if (front == null)
            {
                front = new ObjLoaderObject3D("data/vehicles/head1.obj", baseScale, true, true, false);
                mid = new ObjLoaderObject3D("data/vehicles/mid1.obj", baseScale, true, true, false);
                back = new ObjLoaderObject3D("data/vehicles/back1.obj", baseScale, true, true, false);
                fire = new ObjLoaderObject3D("data/vehicles/fire_single.obj", baseScale, true, true, false);

                colorTextureID = TextureManager.LoadTexture("data/vehicles/fish1color.tif");
                normalTextureID = TextureManager.LoadTexture("data/vehicles/fish1normal.tif");

                fireColorID = TextureManager.LoadTexture("data/vehicles/fire_color.tif");
                fireAdditionalID = TextureManager.LoadTexture("data/vehicles/fire_additional.tif");
            }

            for (int i = 0; i < TALE_LENGTH; i++)
            {
                TaleEntry entry = new TaleEntry();
                entry.position = new Vector3();
                entry.leftOut = new Vector3();
                lightTale.Add(entry);
            }


        }


        public void SetTransform(Matrix4 vTransform)
        {
            transform = vTransform;
        }

        public void CreatePartTransforms(float wipeValue, float scewValue)
        {
            if (explosionUpdate > 0)
            {
                frontTransform = Matrix4.CreateRotationX(explosionUpdate * 0.1f) * Matrix4.CreateRotationZ(explosionUpdate * 0.1f) * Matrix4.CreateTranslation(ExplosionLines[0].Positions[300 - explosionUpdate]) * transform;
                mid1Transform = Matrix4.CreateRotationX(explosionUpdate * 0.1f) * Matrix4.CreateRotationZ(explosionUpdate * 0.1f) * Matrix4.CreateTranslation(ExplosionLines[1].Positions[300 - explosionUpdate]) * transform;
                mid2Transform = Matrix4.CreateRotationX(explosionUpdate * 0.1f) * Matrix4.CreateRotationZ(explosionUpdate * 0.1f) * Matrix4.CreateTranslation(ExplosionLines[2].Positions[300 - explosionUpdate]) * transform;
                mid3Transform = Matrix4.CreateRotationX(explosionUpdate * 0.1f) * Matrix4.CreateRotationZ(explosionUpdate * 0.1f) * Matrix4.CreateTranslation(ExplosionLines[3].Positions[300 - explosionUpdate]) * transform;
                mid4Transform = Matrix4.CreateRotationX(explosionUpdate * 0.1f) * Matrix4.CreateRotationZ(explosionUpdate * 0.1f) * Matrix4.CreateTranslation(ExplosionLines[4].Positions[300 - explosionUpdate]) * transform;
                mid5Transform = Matrix4.CreateRotationX(explosionUpdate * 0.1f) * Matrix4.CreateRotationZ(explosionUpdate * 0.1f) * Matrix4.CreateTranslation(ExplosionLines[5].Positions[300 - explosionUpdate]) * transform;
                backTransform = Matrix4.CreateRotationX(explosionUpdate * 0.1f) * Matrix4.CreateRotationZ(explosionUpdate * 0.1f) * Matrix4.CreateTranslation(ExplosionLines[6].Positions[300 - explosionUpdate]) * transform;
            }
            else
            {


                float subWipe = wipeValue * 0.03f;
                float subScew = scewValue * 0.1f;

                frontTransform = Matrix4.CreateRotationX(subScew) * transform;
                Matrix4 bTrans = Matrix4.CreateRotationY(subWipe) * Matrix4.CreateTranslation(-1.13065f * baseScale, 0, 0);
                mid1Transform = Matrix4.CreateRotationX(subScew * 0.8f) * bTrans * transform;
                bTrans *= Matrix4.CreateRotationY(subWipe) * Matrix4.CreateTranslation((-1.641f + 1.13065f) * baseScale, 0, 0);
                mid2Transform = Matrix4.CreateRotationX(subScew * 0.6f) * Matrix4.CreateScale(0.824f) * bTrans * transform;
                bTrans *= Matrix4.CreateRotationY(subWipe) * Matrix4.CreateTranslation((-2.0582f + 1.641f) * baseScale, 0, 0);
                mid3Transform = Matrix4.CreateRotationX(subScew * 0.4f) * Matrix4.CreateScale(0.656f) * bTrans * transform;
                bTrans *= Matrix4.CreateRotationY(subWipe) * Matrix4.CreateTranslation((-2.3977f + 2.0582f) * baseScale, 0, 0);
                mid4Transform = Matrix4.CreateRotationX(subScew * 0.2f) * Matrix4.CreateScale(0.488f) * bTrans * transform;
                bTrans *= Matrix4.CreateRotationY(subWipe) * Matrix4.CreateTranslation((-2.64681f + 2.3977f) * baseScale, 0, 0);
                mid5Transform = Matrix4.CreateScale(0.369f) * bTrans * transform;
                bTrans *= Matrix4.CreateRotationY(subWipe) * Matrix4.CreateTranslation((-2.9981f + 2.64681f) * baseScale, -0.01884f * baseScale, 0);
                backTransform = bTrans * transform;

                comparePos = new List<Vector3>();
                compareRad = new List<float>();

                Vector3 posFront = frontTransform.ExtractTranslation();
                Vector3 posMid1 = mid1Transform.ExtractTranslation();
                Vector3 posMid2 = mid2Transform.ExtractTranslation();
                Vector3 posMid3 = mid3Transform.ExtractTranslation();
                Vector3 posMid4 = mid4Transform.ExtractTranslation();
                Vector3 posMid5 = mid5Transform.ExtractTranslation();
                Vector3 posBack = backTransform.ExtractTranslation();

                comparePos.Add(posFront); compareRad.Add(0.6f);
                comparePos.Add(posMid1); compareRad.Add(0.5f);
                comparePos.Add(posMid2); compareRad.Add(0.5f);
                comparePos.Add(posMid3); compareRad.Add(0.5f);
                comparePos.Add(posMid4); compareRad.Add(0.5f);
                comparePos.Add(posMid5); compareRad.Add(0.4f);
                comparePos.Add(posBack); compareRad.Add(0.3f);
            }
        }

        public void Update()
        {
            if (isRedBlinking) redUpdate++;
            if (blinkCount > 0) blinkCount--;
            if (explosionUpdate > 0) explosionUpdate--;
            fireUpdate++;
        }

        public void UpdateExplosion()
        {
            if (explosionUpdate > 0) explosionUpdate--;
        }

        public void Blink()
        {
            blinkCount = 10;
        }

        public void Explode()
        {
            Vector4 tmp = new Vector4(0, 0, 0, 1.0f);
            tmp = Vector4.Transform(tmp, frontTransform);

            explosionPosition = new Vector3(tmp.X, tmp.Y, tmp.Z);
            explosionTransform = frontTransform;
            explosionUpdate = 300;
        }

        public void SetRedBlink(bool redBlinking)
        {
            isRedBlinking = redBlinking;
        } 

        public void DrawShadow(Matrix4 aniTransform1, Matrix4 aniTransform2)
        {

            fire.Transformation = aniTransform1 * frontTransform * aniTransform2;
            castShadowMaterial.Draw(fire);

            front.Transformation = aniTransform1 * frontTransform * aniTransform2;
            castShadowMaterial.Draw(front);

            mid.Transformation = aniTransform1 * mid1Transform * aniTransform2;
            castShadowMaterial.Draw(mid);

            mid.Transformation = aniTransform1 * mid2Transform * aniTransform2;
            castShadowMaterial.Draw(mid);

            mid.Transformation = aniTransform1 * mid3Transform * aniTransform2;
            castShadowMaterial.Draw(mid);

            mid.Transformation = aniTransform1 * mid4Transform * aniTransform2;
            castShadowMaterial.Draw(mid);

            mid.Transformation = aniTransform1 * mid5Transform * aniTransform2;
            castShadowMaterial.Draw(mid);

            back.Transformation = aniTransform1 * backTransform * aniTransform2;
            castShadowMaterial.Draw(back);
        }


        public void DrawExplosion()
        {
            if (explosionUpdate > 0)
            {
                MainFireLine.Draw(explosionTransform, firelineID, (300 - explosionUpdate), 0.015f);

                for (int i = 0; i < 7; i++)
                {
                    ExplosionFireLines[i].Draw(explosionTransform, firelineID, (300 - explosionUpdate), 0.006f);
                }

                explosion.Draw(explosionTransform, explosionID, (300 - explosionUpdate) / 10.0f);
            }
        }


        public void Draw(Matrix4 aniTransform1, Matrix4 aniTransform2)
        {

            /*
            if (blinkCount > 0)
            {
                baseAmbient = Light.lightAmbient;
                baseDiffuse = Light.lightDiffuse;
                Light.lightAmbient = (new Vector4(1, 1, 1, 1) - baseAmbient) / 10 * blinkCount + baseAmbient;
                Light.lightDiffuse = (new Vector4(1, 1, 1, 1) - baseDiffuse) / 10 * blinkCount + baseDiffuse;
            } else if (isRedBlinking == true) {
                baseAmbient = Light.lightAmbient;
                baseDiffuse = Light.lightDiffuse;
                Light.lightAmbient = (new Vector4(1, 0.5f, 0.5f, 1) - baseAmbient) * (float)Math.Sin(redUpdate * 0.3f) + baseAmbient;
                Light.lightDiffuse = (new Vector4(1, 0.5f, 0.5f, 1) - baseDiffuse) * (float)Math.Sin(redUpdate * 0.3f) + baseDiffuse;
            }
            */


            fire.Transformation = aniTransform1 * frontTransform * aniTransform2;
            material.Draw(fire, fireColorID, DeferredRendering.NormalFallbackTexture, DeferredRendering.NoShadowTextureID, fireAdditionalID);

            front.Transformation = aniTransform1 * frontTransform * aniTransform2;
            material.Draw(front, colorTextureID, normalTextureID, DeferredRendering.NoShadowTextureID, DeferredRendering.AdditionalFallbackTexture);

            mid.Transformation = aniTransform1 * mid1Transform * aniTransform2;
            material.Draw(mid, colorTextureID, normalTextureID, DeferredRendering.NoShadowTextureID, DeferredRendering.AdditionalFallbackTexture);

            mid.Transformation = aniTransform1 * mid2Transform * aniTransform2;
            material.Draw(mid, colorTextureID, normalTextureID, DeferredRendering.NoShadowTextureID, DeferredRendering.AdditionalFallbackTexture);

            mid.Transformation = aniTransform1 * mid3Transform * aniTransform2;
            material.Draw(mid, colorTextureID, normalTextureID, DeferredRendering.NoShadowTextureID, DeferredRendering.AdditionalFallbackTexture);

            mid.Transformation = aniTransform1 * mid4Transform * aniTransform2;
            material.Draw(mid, colorTextureID, normalTextureID, DeferredRendering.NoShadowTextureID, DeferredRendering.AdditionalFallbackTexture);

            mid.Transformation = aniTransform1 * mid5Transform * aniTransform2;
            material.Draw(mid, colorTextureID, normalTextureID, DeferredRendering.NoShadowTextureID, DeferredRendering.AdditionalFallbackTexture);

            back.Transformation = aniTransform1 * backTransform * aniTransform2;
            material.Draw(back, colorTextureID, normalTextureID, DeferredRendering.NoShadowTextureID, DeferredRendering.AdditionalFallbackTexture);

            /*
            if (blinkCount > 0 || isRedBlinking == true)
            {
                Light.lightAmbient = baseAmbient;
                Light.lightDiffuse = baseDiffuse;
            }
            */
        }

        public void DrawFire(Matrix4 aniTransform1, Matrix4 aniTransform2, float length)
        {
            if (explosionUpdate > 0) return;

            fire.Transformation = Matrix4.CreateRotationX(fireUpdate * 15) * Matrix4.CreateScale(length * (0.8f + ((fireUpdate * 3.714f) % 0.2f)), 1, 1) * Matrix4.CreateTranslation(-1.19036f * baseScale, 0.14909f * baseScale, 0.68239f * baseScale) * aniTransform1 * frontTransform * aniTransform2;
            material.Draw(fire, fireColorID, DeferredRendering.NormalFallbackTexture, DeferredRendering.NoShadowTextureID, fireAdditionalID);

            fire.Transformation = Matrix4.CreateRotationX(fireUpdate * 15 + 37) * Matrix4.CreateScale(length * (0.8f + ((fireUpdate * 3.714f + 4) % 0.2f)), 1, 1) * Matrix4.CreateTranslation(-1.19036f * baseScale, 0.14909f * baseScale, -0.68239f * baseScale) * aniTransform1 * frontTransform * aniTransform2;
            material.Draw(fire, fireColorID, DeferredRendering.NormalFallbackTexture, DeferredRendering.NoShadowTextureID, fireAdditionalID);

        }


        // Ligh Tale logic
        public void DrawLightTale(float intensity, int taleColorID)
        {
            float tWidth = 0.6f;

            List<float> data = new List<float>();
            for (int i = 0; i < lightTale.Count; i++)
            {
                data.Add(lightTale[i].position.X - lightTale[i].leftOut.X * tWidth);
                data.Add(lightTale[i].position.Y - lightTale[i].leftOut.Y * tWidth);
                data.Add(lightTale[i].position.Z - lightTale[i].leftOut.Z * tWidth);

                data.Add(0.0f);
                data.Add(1.0f - 1.0f / (lightTale.Count-1) * i);

                data.Add(lightTale[i].position.X + lightTale[i].leftOut.X * tWidth);
                data.Add(lightTale[i].position.Y + lightTale[i].leftOut.Y * tWidth);
                data.Add(lightTale[i].position.Z + lightTale[i].leftOut.Z * tWidth);

                data.Add(1.0f);
                data.Add(1.0f - 1.0f / (lightTale.Count-1) * i);
            }

            LighTale.Draw(data.ToArray(), intensity * 0.1f, /*taleColors[taleColorID % taleColors.Count]*/ new Vector3(1,1,1));
        }

        public void AddTale(Vector3 position, Vector3 left)
        {
            TaleEntry toAdd = new TaleEntry();
            toAdd.position = position;
            toAdd.leftOut = left;


            for (int i = 0; i < lightTale.Count - 1; i++) {
                lightTale[i] = lightTale[i + 1];
            }
            lightTale[lightTale.Count-1] = toAdd;

        }

        public void DrawLights(Matrix4 aniTransform1, Matrix4 aniTransform2, float intense, float radius)
        {
            
            Vector4 left = new Vector4(-0.4f, 0.1f, 0.26f, 1.0f);
            left = Vector4.Transform(left, aniTransform1 * frontTransform * aniTransform2);

            Vector4 right = new Vector4(-0.4f, 0.1f, -0.26f, 1.0f);
            right = Vector4.Transform(right, aniTransform1 * frontTransform * aniTransform2);

            DeferredRendering.DrawSphereLight(new Vector3(right.X, right.Y, right.Z), radius, new Vector3(0.6f * intense, 0.6f * intense, 0.1f * intense));
            DeferredRendering.DrawSphereLight(new Vector3(left.X, left.Y, left.Z), radius, new Vector3(0.6f * intense, 0.6f * intense, 0.1f * intense));

            if (explosionUpdate > 100)
            {
                float exploIntense = (explosionUpdate - 100) / 20.0f;
                DeferredRendering.DrawSphereLight(explosionPosition, 25, new Vector3(1.0f * exploIntense, 0.5f * exploIntense, 0.1f *exploIntense));
            }
        }


    }
}
