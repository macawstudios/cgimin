#version 330
precision highp float;

uniform sampler2D sampler;
uniform vec4 color;

in vec2 texcoord;

// output
out vec4 outputColor;

void main()
{
    vec4 colsource = texture(sampler, texcoord);
	vec4 fcolor = vec4(colsource.a, colsource.a, colsource.a, 0) * color;


	outputColor = fcolor;

}