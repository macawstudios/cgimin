﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenTK;

namespace lv1.vehicle
{
    class ExplosionLineData
    {

        public List<Vector3> Positions;

        public ExplosionLineData(Vector3 dir, float gravity) {

            Positions = new List<Vector3>();

            Vector3 position = new Vector3(0, 0, 0);
            float fallingSpeed = dir.Y;

            for (int i = 0; i < 300; i++)
            {
                position.X += dir.X;
                position.Z += dir.Z;
                position.Y += fallingSpeed;

                fallingSpeed -= gravity;

                Positions.Add(position);
            }

        }

    }
}
