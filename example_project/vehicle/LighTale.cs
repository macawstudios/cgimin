﻿using System;
using System.Collections.Generic;
using Engine.cgimin.helpers;
using OpenTK;
using OpenTK.Graphics.OpenGL;
using Engine.cgimin.camera;
using Engine.cgimin.texture;

namespace lv1.vehicle
{
    class LighTale
    {

        private static bool initialized = false;


        private static List<int> indices = new List<int>();

        // texture
        private static int texture;

        // shader program
        private static int program;
        private static int modelviewProjectionMatrixLocation;
        private static int colorLocation;
        private static int samplerLocation;

        // buffers
        private static int vao;
        private static int bufferVBO;


        public static void Initialize(int taleLength)
        {
            if (initialized) return;
            initialized = true;

            program = ShaderCompiler.CreateShaderProgram("vehicle/GlowTale_VS.glsl", "vehicle/GlowTale_FS.glsl");
            GL.BindAttribLocation(program, 0, "in_position");
            GL.BindAttribLocation(program, 1, "in_uv");
            GL.LinkProgram(program);
            modelviewProjectionMatrixLocation = GL.GetUniformLocation(program, "modelview_projection_matrix");
            colorLocation = GL.GetUniformLocation(program, "color");
            samplerLocation = GL.GetUniformLocation(program, "sampler");

            texture = TextureManager.LoadTexture("data/vehicles/glow_tale.png");


            List<float> allData = new List<float>();
            for (int i = 0; i < taleLength; i++)
            {
                allData.Add(0.0f);
                allData.Add(0.0f);
                allData.Add(0.0f);

                allData.Add(0);
                allData.Add(1.0f / (float)taleLength * i);

                allData.Add(0.0f);
                allData.Add(0.0f);
                allData.Add(0.0f);

                allData.Add(1.0f);
                allData.Add(1.0f / (float)taleLength * i);
            }

            
            GL.GenBuffers(1, out bufferVBO);
            GL.BindBuffer(BufferTarget.ArrayBuffer, bufferVBO);
            GL.BufferData(BufferTarget.ArrayBuffer, (IntPtr)(allData.Count * sizeof(float)), allData.ToArray(), BufferUsageHint.DynamicDraw);
            GL.BindBuffer(BufferTarget.ArrayBuffer, 0);


            // Generierung des Index Buffer

            for (int i = 0; i < taleLength - 1; i++)
            {
                indices.Add(i * 2); indices.Add(i * 2 + 1); indices.Add(i * 2 + 2);
                indices.Add(i * 2 + 1); indices.Add(i * 2 + 2); indices.Add(i * 2 + 3);
            }


            int IndexBuffer;
            GL.GenBuffers(1, out IndexBuffer);
            GL.BindBuffer(BufferTarget.ElementArrayBuffer, IndexBuffer);
            GL.BufferData(BufferTarget.ElementArrayBuffer, new IntPtr(sizeof(uint) * indices.Count), indices.ToArray(), BufferUsageHint.DynamicDraw);
            GL.BindBuffer(BufferTarget.ElementArrayBuffer, 0);

            GL.GenVertexArrays(1, out vao);
            GL.BindVertexArray(vao);
            GL.BindBuffer(BufferTarget.ElementArrayBuffer, IndexBuffer);
            GL.BindBuffer(BufferTarget.ArrayBuffer, bufferVBO);

            GL.EnableVertexAttribArray(0);
            GL.EnableVertexAttribArray(1);

            int strideSize = Vector3.SizeInBytes + Vector2.SizeInBytes;

            GL.VertexAttribPointer(0, 3, VertexAttribPointerType.Float, true, strideSize, 0);
            GL.VertexAttribPointer(1, 2, VertexAttribPointerType.Float, true, strideSize, Vector3.SizeInBytes);

            GL.BindVertexArray(0);

        }

        public static void Draw(float[] data, float intensity, Vector3 taleColor)
        {
            Vector4 color = new Vector4(taleColor.X, taleColor.Y, taleColor.Z, intensity);

            GL.Enable(EnableCap.Blend);
            //GL.BlendFunc(BlendingFactorSrc.SrcAlpha, BlendingFactorDest.OneMinusSrcAlpha);
            GL.BlendFunc(BlendingFactorSrc.One, BlendingFactorDest.One);
            GL.DepthMask(false);
            GL.Disable(EnableCap.CullFace);

            GL.BindBuffer(BufferTarget.ArrayBuffer, bufferVBO);
            GL.BufferData(BufferTarget.ArrayBuffer, (IntPtr)(data.Length * sizeof(float)), data, BufferUsageHint.DynamicDraw);
            GL.BindBuffer(BufferTarget.ArrayBuffer, 0);

            GL.BindVertexArray(vao);
            GL.UseProgram(program);

            GL.Uniform1(samplerLocation, 0);
            GL.ActiveTexture(TextureUnit.Texture0);
            GL.BindTexture(TextureTarget.Texture2D, texture);

            Matrix4 modelviewProjection = Camera.Transformation * Camera.PerspectiveProjection;
            GL.UniformMatrix4(modelviewProjectionMatrixLocation, false, ref modelviewProjection);
            GL.Uniform4(colorLocation, color);
            GL.DrawElements(PrimitiveType.Triangles, indices.Count, DrawElementsType.UnsignedInt, IntPtr.Zero);
            GL.BindVertexArray(0);

            GL.DepthMask(true);
            GL.Disable(EnableCap.Blend);
            GL.Enable(EnableCap.CullFace);
        }


    }
}
