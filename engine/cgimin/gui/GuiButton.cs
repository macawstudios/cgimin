﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Engine.cgimin.gui
{
    public class GUIButton : GUIContainer
    {
        public string ID;
        public static string CurrentSelection = null;


        public static void NavigateUP()
        {
            GUIButton found = find(CurrentSelection);
            if (found != null && found.upID != null) CurrentSelection = found.upID;
        }

        public static void NavigateDown()
        {
            GUIButton found = find(CurrentSelection);
            if (found != null && found.downID != null) CurrentSelection = found.downID;
        }

        public static void NavigateLeft()
        {
            GUIButton found = find(CurrentSelection);
            if (found != null && found.leftID != null) CurrentSelection = found.leftID;
        }

        public static void NavigateRight()
        {
            GUIButton found = find(CurrentSelection);
            if (found != null && found.rightID != null) CurrentSelection = found.rightID;
        }

        public static void NavigateEnter()
        {
            GUIButton found = find(CurrentSelection);
            if (found != null) {
                found.click();
            }            
        }
      

        public static GUIButton find (string buttonID)
        {
            return find(buttonID, GUISystem.container);
        }

        private static GUIButton find(string buttonID, GUIContainer currentNode)
        {
            if (currentNode is GUIButton && (currentNode as GUIButton).ID.Equals(buttonID)){
                return currentNode as GUIButton;
            }
            foreach (var child in currentNode.children)
            {
                if (child is GUIContainer)
                {
                    var button = find(buttonID, child as GUIContainer);
                    if (button != null) return button;
                }
            }
            return null;

        }


        internal string upID = null;
        internal string downID = null;
        internal string leftID = null;
        internal string rightID = null;

        private Action<String> clickCB;

        public bool IsSelected()
        {
            return CurrentSelection.Equals(ID);
        }

        public void Select()
        {
            CurrentSelection = ID;
        }

        public GUIButton(string id, Action<String> clickCallBack) : base()
        {
            clickCB = clickCallBack;
            ID = id;
            if (CurrentSelection == null) CurrentSelection = ID;
        }

        public void SetNavigation(string up, string down, string left, string right)
        {
            upID = up;
            downID = down;
            leftID = left;
            rightID = right;
        }

        public void click()
        {
            clickCB(ID);
        }


    }
}
