﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenTK;
using OpenTK.Graphics.OpenGL;
using Engine.cgimin.helpers;
using Engine.cgimin.camera;


namespace Engine.cgimin.gui
{
    public class GUI9Grid : GUIElement
    {

        private int tWidth;
        private int tHeight;
        private int tID;
        private int indexCount;
        private int graphicVOA;


        public int Width { get; set; }
        public int Height { get; set; }


        // static
        private static int program = -1;
        private static int colorTextureLocation;
        private static int modelViewProjectionLocation;
        private static int alphaLocation;
        private static int widthheightLocation;




        public GUI9Grid(int txtWidth, int txtHeight, int textureID, int u1, int v1, int u2, int v2, int top, int left, int bottom, int right) : base()
        {

            tWidth = txtWidth;
            tHeight = txtHeight;

            tID = textureID;

            List<float> gridData = new List<float>();
            List<int> indices = new List<int>();

            Width = u2 - u1;
            Height = v2 - v1;

            // oben links erste vier, uhreigersinn
            gridData.Add(0); gridData.Add(0); gridData.Add(0); // pos up l
            gridData.Add(0); gridData.Add(0); // w inc, h inc
            gridData.Add((float)u1 / txtWidth); gridData.Add((float)v1 / txtHeight); // uv

            gridData.Add(left); gridData.Add(0); gridData.Add(0); // pos up l
            gridData.Add(0); gridData.Add(0); // w inc, h inc
            gridData.Add((float)(u1 + left) / txtWidth); gridData.Add((float)v1 / txtHeight); // uv

            gridData.Add(left); gridData.Add(-top); gridData.Add(0); // pos up l
            gridData.Add(0); gridData.Add(0); // w inc, h inc
            gridData.Add((float)(u1 + left) / txtWidth); gridData.Add((float)(v1 + top) / txtHeight); // uv

            gridData.Add(0); gridData.Add(-top); gridData.Add(0); // pos up l
            gridData.Add(0); gridData.Add(0); // w inc, h inc
            gridData.Add((float)u1 / txtWidth); gridData.Add((float)(v1 + top) / txtHeight); // uv


            // oben rechts zeite vier, gegen uhreigersinn
            gridData.Add(0); gridData.Add(0); gridData.Add(0); // pos up l
            gridData.Add(1.0f); gridData.Add(0); // w inc, h inc
            gridData.Add((float)u2 / txtWidth); gridData.Add((float)v1 / txtHeight); // uv

            gridData.Add(-right); gridData.Add(0); gridData.Add(0); // pos up l
            gridData.Add(1.0f); gridData.Add(0); // w inc, h inc
            gridData.Add((float)(u2-right) / txtWidth); gridData.Add((float)v1 / txtHeight); // uv

            gridData.Add(-right); gridData.Add(-top); gridData.Add(0); // pos up l
            gridData.Add(1.0f); gridData.Add(0); // w inc, h inc
            gridData.Add((float)(u2 - right) / txtWidth); gridData.Add((float)(v1 + top) / txtHeight); // uv

            gridData.Add(0); gridData.Add(-top); gridData.Add(0); // pos up l
            gridData.Add(1.0f); gridData.Add(0); // w inc, h inc
            gridData.Add((float)u2 / txtWidth); gridData.Add((float)(v1 + top) / txtHeight); // uv


            // unten rechts zeite vier, gegen uhreigersinn
            gridData.Add(0); gridData.Add(0); gridData.Add(0); // pos up l
            gridData.Add(1.0f); gridData.Add(-1.0f); // w inc, h inc
            gridData.Add((float)u2 / txtWidth); gridData.Add((float)v2 / txtHeight); // uv

            gridData.Add(-right); gridData.Add(0); gridData.Add(0); // pos up l
            gridData.Add(1.0f); gridData.Add(-1.0f); // w inc, h inc
            gridData.Add((float)(u2 - right) / txtWidth); gridData.Add((float)v2 / txtHeight); // uv

            gridData.Add(-right); gridData.Add(bottom); gridData.Add(0); // pos up l
            gridData.Add(1.0f); gridData.Add(-1.0f); // w inc, h inc
            gridData.Add((float)(u2 - right) / txtWidth); gridData.Add((float)(v2 - bottom) / txtHeight); // uv

            gridData.Add(0); gridData.Add(bottom); gridData.Add(0); // pos up l
            gridData.Add(1.0f); gridData.Add(-1.0f); // w inc, h inc
            gridData.Add((float)u2 / txtWidth); gridData.Add((float)(v2 - bottom) / txtHeight); // uv


            // unten links erste vier, uhreigersinn
            gridData.Add(0); gridData.Add(0); gridData.Add(0); // pos up l
            gridData.Add(0); gridData.Add(-1.0f); // w inc, h inc
            gridData.Add((float)u1 / txtWidth); gridData.Add((float)v2 / txtHeight); // uv

            gridData.Add(left); gridData.Add(0); gridData.Add(0); // pos up l
            gridData.Add(0); gridData.Add(-1.0f); // w inc, h inc
            gridData.Add((float)(u1 + left) / txtWidth); gridData.Add((float)v2 / txtHeight); // uv

            gridData.Add(left); gridData.Add(bottom); gridData.Add(0); // pos up l
            gridData.Add(0); gridData.Add(-1.0f); // w inc, h inc
            gridData.Add((float)(u1 + left) / txtWidth); gridData.Add((float)(v2 - bottom) / txtHeight); // uv

            gridData.Add(0); gridData.Add(bottom); gridData.Add(0); // pos up l
            gridData.Add(0); gridData.Add(-1.0f); // w inc, h inc
            gridData.Add((float)u1 / txtWidth); gridData.Add((float)(v2 - bottom) / txtHeight); // uv


            indices.Add(0); indices.Add(1); indices.Add(2);
            indices.Add(0); indices.Add(2); indices.Add(3);

            indices.Add(1); indices.Add(5); indices.Add(6);
            indices.Add(2); indices.Add(1); indices.Add(6);

            indices.Add(4); indices.Add(5); indices.Add(6);
            indices.Add(6); indices.Add(7); indices.Add(4);

            indices.Add(3); indices.Add(2); indices.Add(14);
            indices.Add(15); indices.Add(14); indices.Add(3);

            indices.Add(2); indices.Add(6); indices.Add(10);
            indices.Add(14); indices.Add(2); indices.Add(10);

            indices.Add(6); indices.Add(7); indices.Add(11);
            indices.Add(10); indices.Add(11); indices.Add(6);

            indices.Add(12); indices.Add(13); indices.Add(14);
            indices.Add(12); indices.Add(14); indices.Add(15);

            indices.Add(13); indices.Add(14); indices.Add(10);
            indices.Add(9); indices.Add(10); indices.Add(13);

            indices.Add(8); indices.Add(9); indices.Add(10);
            indices.Add(8); indices.Add(10); indices.Add(11);


            indexCount = indices.Count;

            int graphicVBO;
            GL.GenBuffers(1, out graphicVBO);
            GL.BindBuffer(BufferTarget.ArrayBuffer, graphicVBO);
            GL.BufferData(BufferTarget.ArrayBuffer, (IntPtr)(gridData.Count * sizeof(float)), gridData.ToArray(), BufferUsageHint.StaticDraw);
            GL.BindBuffer(BufferTarget.ArrayBuffer, 0);

            int indexBuffer;
            GL.GenBuffers(1, out indexBuffer);
            GL.BindBuffer(BufferTarget.ElementArrayBuffer, indexBuffer);
            GL.BufferData(BufferTarget.ElementArrayBuffer, new IntPtr(sizeof(uint) * indices.Count), indices.ToArray(), BufferUsageHint.StaticDraw);
            GL.BindBuffer(BufferTarget.ElementArrayBuffer, 0);

            GL.GenVertexArrays(1, out graphicVOA);
            GL.BindVertexArray(graphicVOA);
            GL.BindBuffer(BufferTarget.ElementArrayBuffer, indexBuffer);
            GL.BindBuffer(BufferTarget.ArrayBuffer, graphicVBO);

            GL.EnableVertexAttribArray(0);
            GL.EnableVertexAttribArray(1);
            GL.EnableVertexAttribArray(2);

            GL.VertexAttribPointer(0, 3, VertexAttribPointerType.Float, true, Vector3.SizeInBytes + Vector2.SizeInBytes * 2, 0);
            GL.VertexAttribPointer(1, 2, VertexAttribPointerType.Float, true, Vector3.SizeInBytes + Vector2.SizeInBytes * 2, Vector3.SizeInBytes);
            GL.VertexAttribPointer(2, 2, VertexAttribPointerType.Float, true, Vector3.SizeInBytes + Vector2.SizeInBytes * 2, Vector3.SizeInBytes + Vector2.SizeInBytes);
            GL.BindVertexArray(0);


            if (program == -1)
            {
                program = ShaderCompiler.CreateShaderProgram("cgimin/gui/Bitmap9Grid_VS.glsl", "cgimin/gui/Bitmap9Grid_FS.glsl");
                GL.BindAttribLocation(program, 0, "in_position");
                GL.BindAttribLocation(program, 1, "add_pos");
                GL.BindAttribLocation(program, 2, "in_uv");
                GL.LinkProgram(program);

                modelViewProjectionLocation = GL.GetUniformLocation(program, "modelview_projection_matrix");
                colorTextureLocation = GL.GetUniformLocation(program, "sampler");
                alphaLocation = GL.GetUniformLocation(program, "alpha");
                widthheightLocation = GL.GetUniformLocation(program, "width_height");
            }

        }


        public override void Draw()
        {
            base.Draw();

            GL.BindVertexArray(graphicVOA);

            GL.UseProgram(program);
            GL.Disable(EnableCap.DepthTest);
            GL.Disable(EnableCap.CullFace);
            GL.Enable(EnableCap.Blend);
            GL.BlendFunc(BlendingFactorSrc.SrcAlpha, BlendingFactorDest.OneMinusSrcAlpha);

            GL.Uniform1(colorTextureLocation, 0);
            GL.ActiveTexture(TextureUnit.Texture0);
            GL.BindTexture(TextureTarget.Texture2D, tID);

            Matrix4 projection = Matrix4.CreateTranslation(intx, inty, 0) * Camera.PerspectiveProjection;
            GL.UniformMatrix4(modelViewProjectionLocation, false, ref projection);

            GL.Uniform1(alphaLocation, Math.Min(Math.Max(intAlpha, 0.0f), 1.0f));

            GL.Uniform2(widthheightLocation, new Vector2(Width, Height));

            GL.DrawElements(PrimitiveType.Triangles, indexCount, DrawElementsType.UnsignedInt, IntPtr.Zero);

            GL.Enable(EnableCap.DepthTest);
            GL.Enable(EnableCap.CullFace);
            GL.Disable(EnableCap.Blend);

            GL.ActiveTexture(TextureUnit.Texture0);
            GL.BindVertexArray(0);

        }



    }
}
