﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Engine.cgimin.gui
{
    public class GUITextElement: GUIElement
    {

        private BitmapFont mFont;

        public GUITextElement(BitmapFont font): base()
        {
            mFont = font;
            r = 255;
            g = 255;
            b = 255; 
        }
         
        public String Text { get; set; }
        public int r { get; set; }
        public int g { get; set; }
        public int b { get; set; }

        public int CalcTextWidth() {
            return mFont.CalcTextWidth(Text);
        }

        public void SetTextColor(int red, int green, int blue)
        {
            r = red;
            g = green;
            b = blue;
        } 

        public override void Draw()
        {
            base.Draw();
            mFont.DrawString(Text, intx, inty, r, g, b, (int)Math.Min(Math.Max(intAlpha, 0.0f), 1.0f) * 255);
        }

    }
}
