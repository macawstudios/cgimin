﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Engine.cgimin.gui
{
    public class GUIElement
    {

        internal int intx;
        internal int inty;
        internal float intAlpha;

        public int x;
        public int y;
        public bool visible;

        public float alpha = 1.0f;

        public GUIElement()
        {
            x = 0;
            y = 0;
            visible = true;
        }

        public virtual void SetToInitialValues() {
            intx = x;
            inty = y;
            intAlpha = alpha;
        }

        public virtual void Update() { }
        public virtual void Draw() { }

    }
}
