﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenTK;
using OpenTK.Graphics.OpenGL;
using Engine.cgimin.helpers;
using Engine.cgimin.camera;

namespace Engine.cgimin.gui
{
    public class GUIGraphic: GUIElement
    {

        public float scaleX { get; set; }
        public float scaleY { get; set; }

        private int baseWidth;
        private int baseHeight;

        private int tWidth;
        private int tHeight;
        private int tID;

        private int indexCount;

        private int graphicVOA;


        // static
        private static int program = -1;
        private static int colorTextureLocation;
        private static int modelViewProjectionLocation;
        private static int alphaLocation;
       
        public GUIGraphic(int txtWidth, int txtHeight, int textureID, int u1, int v1, int u2, int v2):base()
        {
            scaleX = 1.0f;
            scaleY = 1.0f;

            tWidth = txtWidth;
            tHeight = txtHeight;

            tID = textureID;

            baseWidth = u2 - u1;
            baseHeight = v2 - v2;

            List<float> fontData = new List<float>();
            List<int> indices = new List<int>();

            fontData.Add(0); fontData.Add(0); fontData.Add(0);
            fontData.Add((float)(u1) / txtWidth); fontData.Add((float)(v1) / txtWidth);

            fontData.Add(u2 - u1); fontData.Add(0); fontData.Add(0);
            fontData.Add((float)(u2) / txtWidth); fontData.Add((float)(v1) / txtWidth);

            fontData.Add(u2 - u1); fontData.Add(v2 - v1); fontData.Add(0);
            fontData.Add((float)(u2) / txtWidth); fontData.Add((float)(v2) / txtHeight);

            fontData.Add(0); fontData.Add(v2 - v1); fontData.Add(0);
            fontData.Add((float)(u1) / txtWidth); fontData.Add((float)(v2) / txtHeight);

            indices.Add(0); indices.Add(1); indices.Add(2);
            indices.Add(0); indices.Add(2); indices.Add(3);


            indexCount = indices.Count;

            int graphicVBO;
            GL.GenBuffers(1, out graphicVBO);
            GL.BindBuffer(BufferTarget.ArrayBuffer, graphicVBO);
            GL.BufferData(BufferTarget.ArrayBuffer, (IntPtr)(fontData.Count * sizeof(float)), fontData.ToArray(), BufferUsageHint.StaticDraw);
            GL.BindBuffer(BufferTarget.ArrayBuffer, 0);

            int indexBuffer;
            GL.GenBuffers(1, out indexBuffer);
            GL.BindBuffer(BufferTarget.ElementArrayBuffer, indexBuffer);
            GL.BufferData(BufferTarget.ElementArrayBuffer, new IntPtr(sizeof(uint) * indices.Count), indices.ToArray(), BufferUsageHint.StaticDraw);
            GL.BindBuffer(BufferTarget.ElementArrayBuffer, 0);

            GL.GenVertexArrays(1, out graphicVOA);
            GL.BindVertexArray(graphicVOA);
            GL.BindBuffer(BufferTarget.ElementArrayBuffer, indexBuffer);
            GL.BindBuffer(BufferTarget.ArrayBuffer, graphicVBO);

            GL.EnableVertexAttribArray(0);
            GL.EnableVertexAttribArray(1);

            GL.VertexAttribPointer(0, 3, VertexAttribPointerType.Float, true, Vector3.SizeInBytes + Vector2.SizeInBytes, 0);
            GL.VertexAttribPointer(1, 2, VertexAttribPointerType.Float, true, Vector3.SizeInBytes + Vector2.SizeInBytes, Vector3.SizeInBytes);
            GL.BindVertexArray(0);


            if (program == -1)
            {
                program = ShaderCompiler.CreateShaderProgram("cgimin/gui/BitmapGraphic_VS.glsl", "cgimin/gui/BitmapGraphic_FS.glsl");
                GL.BindAttribLocation(program, 0, "in_position");
                GL.BindAttribLocation(program, 1, "in_uv");
                GL.LinkProgram(program);

                modelViewProjectionLocation = GL.GetUniformLocation(program, "modelview_projection_matrix");
                colorTextureLocation = GL.GetUniformLocation(program, "sampler");
                alphaLocation = GL.GetUniformLocation(program, "alpha");
            }

        }


        public float Width
        {
            get
            {
                return baseWidth * scaleX;
            }
        }


        public float Height
        {
            get
            {
                return baseHeight * scaleY;
            }
        }

        public override void Draw()
        {
            base.Draw();

            GL.BindVertexArray(graphicVOA);

            GL.UseProgram(program);
            GL.Disable(EnableCap.DepthTest);
            GL.Disable(EnableCap.CullFace);
            GL.Enable(EnableCap.Blend);
            GL.BlendFunc(BlendingFactorSrc.SrcAlpha, BlendingFactorDest.OneMinusSrcAlpha);

            GL.Uniform1(colorTextureLocation, 0);
            GL.ActiveTexture(TextureUnit.Texture0);
            GL.BindTexture(TextureTarget.Texture2D, tID);

            Matrix4 projection = Matrix4.CreateScale(scaleX, scaleY, 0) * Matrix4.CreateTranslation(x, y, 0) * Camera.PerspectiveProjection;
            GL.UniformMatrix4(modelViewProjectionLocation, false, ref projection);

            GL.Uniform1(alphaLocation, Math.Min(Math.Max(intAlpha, 0.0f), 1.0f));

            GL.DrawElements(PrimitiveType.Triangles, 6, DrawElementsType.UnsignedInt, IntPtr.Zero);
            
            GL.Enable(EnableCap.DepthTest);
            GL.Enable(EnableCap.CullFace);
            GL.Disable(EnableCap.Blend);

            GL.ActiveTexture(TextureUnit.Texture0);
            GL.BindVertexArray(0);

        }


    }
}
