#version 130
precision highp float;

uniform sampler2D color_texture; 
uniform vec4 color;

in vec2 texcoord;

out vec4 outputColor;

void main()
{
	float alpha = texture(color_texture, texcoord).r;
    outputColor = vec4(color.r, color.g, color.b, color.a * alpha);
}