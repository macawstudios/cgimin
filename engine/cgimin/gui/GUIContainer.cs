﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Engine.cgimin.gui
{
    public class GUIContainer : GUIElement
    {

        public List<GUIElement> children;

        public GUIContainer():base()
        {
            children = new List<GUIElement>();
        }

        public override void Draw()
        {
            base.Draw();

            for (int i = 0; i < children.Count; i++)
            {
                if (children[i].visible) {
                    children[i].intx += this.intx;
                    children[i].inty += this.inty;
                    children[i].intAlpha *= this.intAlpha;
                    children[i].Draw();
                }
            }
        }

        public override void Update()
        {
            foreach (GUIElement element in children) element.Update();
        }

        public override void SetToInitialValues()
        {
            foreach (GUIElement element in children) element.SetToInitialValues();
            base.SetToInitialValues();
        }

        public void AddChild(GUIElement element)
        {
            children.Add(element);
        }

        public void RemoveChild(GUIElement element)
        {
            if (children.Contains(element))
            {
                children.Remove(element);
            } else
            {
                Console.WriteLine("RemoveChild - Container does NOT conatain element");
            }
        }


        public void RemoveAllChildren()
        {
            children.Clear();
        }

        public bool Contains(GUIElement element)
        {
            return children.Contains(element);
        }

    }
}
