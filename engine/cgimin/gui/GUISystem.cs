﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Engine.cgimin.camera;
using OpenTK;

namespace Engine.cgimin.gui
{
    public class GUISystem
    {

        public static GUIContainer container = new GUIContainer();

        private static bool upPushed = false;
        private static bool downPushed = false;
        private static bool leftPushed = false;
        private static bool rightPushed = false;
        private static bool enterPushed = false;

        public static void Update(GameWindow gameWindow)
        {
            container.Update();

            if (gameWindow.Keyboard[OpenTK.Input.Key.Up] && !upPushed) { GUIButton.NavigateUP(); upPushed = true; }
            if (gameWindow.Keyboard[OpenTK.Input.Key.Down] && !downPushed) { GUIButton.NavigateDown(); downPushed = true; }
            if (gameWindow.Keyboard[OpenTK.Input.Key.Left] && !leftPushed) { GUIButton.NavigateLeft(); leftPushed = true; }
            if (gameWindow.Keyboard[OpenTK.Input.Key.Right] && !rightPushed) { GUIButton.NavigateRight(); rightPushed = true; }
            if (gameWindow.Keyboard[OpenTK.Input.Key.Enter] && !enterPushed) { GUIButton.NavigateEnter(); enterPushed = true; }

            if (!gameWindow.Keyboard[OpenTK.Input.Key.Up]) upPushed = false;
            if (!gameWindow.Keyboard[OpenTK.Input.Key.Down]) downPushed = false;
            if (!gameWindow.Keyboard[OpenTK.Input.Key.Left]) leftPushed = false;
            if (!gameWindow.Keyboard[OpenTK.Input.Key.Right]) rightPushed = false;
            if (!gameWindow.Keyboard[OpenTK.Input.Key.Enter]) enterPushed = false;

        }

        public static void Draw()
        {
            container.intAlpha = 1.0f;
            container.alpha = 1.0f;
            container.SetToInitialValues();
            Camera.SetToGUICam();
            container.Draw();
            Camera.SetBackToLastCameraSettings();
        }



    }

}
