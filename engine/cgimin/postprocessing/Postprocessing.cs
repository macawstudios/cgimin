﻿using System;
using OpenTK;
using OpenTK.Graphics.OpenGL;
using OpenTK.Graphics;
using Engine.cgimin.material.simpletexture;
using Engine.cgimin.object3d;


namespace Engine.cgimin.postprocessing
{
    public class Postprocessing
    {
        private static int width;
        private static int height;

        private static int FramebufferName;

        private static int ColorTextureName;
        private static int GlowTextureName;

        private static int PingPongFBO0;
        private static int PingPongBuffer0;

        private static int PingPongFBO1;
        private static int PingPongBuffer1;

        private static BaseObject3D fullscreenQuad;

        private static BlurHorizontalMaterial horizontalBlurMaterial;
        private static BlurVerticalMaterial verticalBlurMaterial;
        private static SimpleFullscreenMaterial simpleFullscreenMaterial;

        public static void Init(int screenWidth, int screenHeight)
        {
            fullscreenQuad = new BaseObject3D();
            fullscreenQuad.addTriangle(new Vector3( 1, -1, 0), new Vector3(-1, -1, 0), new Vector3(1, 1, 0), new Vector2(1, 0), new Vector2(0, 0), new Vector2(1, 1));
            fullscreenQuad.addTriangle(new Vector3(-1, -1, 0), new Vector3(1, 1, 0), new Vector3(-1, 1, 0), new Vector2(0, 0), new Vector2(1, 1), new Vector2(0, 1));
            fullscreenQuad.CreateVAO();

            verticalBlurMaterial = new BlurVerticalMaterial();
            horizontalBlurMaterial = new BlurHorizontalMaterial();
            simpleFullscreenMaterial = new SimpleFullscreenMaterial();

            width = screenWidth;
            height = screenHeight;

            FramebufferName = 0;
            GL.GenFramebuffers(1, out FramebufferName);
            GL.BindFramebuffer(FramebufferTarget.Framebuffer, FramebufferName);

            int colorTextureRef = 0;
            GL.GenTextures(1, out colorTextureRef);
            GL.BindTexture(TextureTarget.Texture2D, colorTextureRef);
            ColorTextureName = colorTextureRef;
            GL.TexImage2D(TextureTarget.Texture2D, 0, PixelInternalFormat.Rgba, screenWidth, screenHeight, 0, PixelFormat.Rgba, PixelType.UnsignedInt8888, IntPtr.Zero);
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMagFilter, (int)TextureMagFilter.Linear);
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMinFilter, (int)TextureMinFilter.Linear);

            int glowTextureRef = 0;
            GL.GenTextures(1, out glowTextureRef);
            GL.BindTexture(TextureTarget.Texture2D, glowTextureRef);
            GlowTextureName = glowTextureRef;
            GL.TexImage2D(TextureTarget.Texture2D, 0, PixelInternalFormat.Rgba, screenWidth, screenHeight, 0, PixelFormat.Rgba, PixelType.UnsignedInt8888, IntPtr.Zero);
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMagFilter, (int)TextureMagFilter.Linear);
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMinFilter, (int)TextureMinFilter.Linear);


            GL.FramebufferTexture(FramebufferTarget.Framebuffer, FramebufferAttachment.ColorAttachment0, ColorTextureName, 0);
            GL.FramebufferTexture(FramebufferTarget.Framebuffer, FramebufferAttachment.ColorAttachment1, GlowTextureName, 0);

            int depthrenderbuffer;
            GL.GenRenderbuffers(1, out depthrenderbuffer);
            GL.BindRenderbuffer(RenderbufferTarget.Renderbuffer, depthrenderbuffer);
            GL.RenderbufferStorage(RenderbufferTarget.Renderbuffer, RenderbufferStorage.DepthComponent, screenWidth, screenHeight);
            GL.FramebufferRenderbuffer(FramebufferTarget.Framebuffer, FramebufferAttachment.DepthAttachment, RenderbufferTarget.Renderbuffer, depthrenderbuffer);
            
            DrawBuffersEnum[] drawEnum = { DrawBuffersEnum.ColorAttachment0, DrawBuffersEnum.ColorAttachment1 };
            GL.DrawBuffers(2, drawEnum);

            FramebufferErrorCode eCode = GL.CheckFramebufferStatus(FramebufferTarget.Framebuffer);
            if (eCode != FramebufferErrorCode.FramebufferComplete)
            {
                Console.WriteLine("eCode Wrong" + eCode.ToString());
            }
            else {
                Console.WriteLine("eCode Correct");
            }

            // Ping-Pong Buffer 0
            GL.GenFramebuffers(1, out PingPongFBO0);
            GL.GenTextures(1, out PingPongBuffer0);
            GL.BindFramebuffer(FramebufferTarget.Framebuffer, PingPongFBO0);
            GL.BindTexture(TextureTarget.Texture2D, PingPongBuffer0);
            GL.TexImage2D(TextureTarget.Texture2D, 0, PixelInternalFormat.Rgba, screenWidth, screenHeight, 0, PixelFormat.Bgra, PixelType.UnsignedByte, IntPtr.Zero);
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMagFilter, (int)TextureMagFilter.Linear);
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMinFilter, (int)TextureMinFilter.Linear);
            GL.FramebufferTexture(FramebufferTarget.Framebuffer, FramebufferAttachment.ColorAttachment0, PingPongBuffer0, 0);

            // Ping-Pong Buffer 1
            GL.GenFramebuffers(1, out PingPongFBO1);
            GL.GenTextures(1, out PingPongBuffer1);
            GL.BindFramebuffer(FramebufferTarget.Framebuffer, PingPongFBO1);
            GL.BindTexture(TextureTarget.Texture2D, PingPongBuffer1);
            GL.TexImage2D(TextureTarget.Texture2D, 0, PixelInternalFormat.Rgba, screenWidth, screenHeight, 0, PixelFormat.Bgra, PixelType.UnsignedByte, IntPtr.Zero);
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMagFilter, (int)TextureMagFilter.Linear);
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMinFilter, (int)TextureMinFilter.Linear);
            GL.FramebufferTexture(FramebufferTarget.Framebuffer, FramebufferAttachment.ColorAttachment0, PingPongBuffer1, 0);

            GL.DrawBuffers(2, drawEnum);
  

        }

        public static void Start()
        {
            GL.BindFramebuffer(FramebufferTarget.Framebuffer, FramebufferName);
            DrawBuffersEnum[] drawEnum = { DrawBuffersEnum.ColorAttachment0, DrawBuffersEnum.ColorAttachment1 };
            GL.DrawBuffers(2, drawEnum);


            GL.ClearColor(new Color4(0,0,0,0));
            GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);

            GL.Viewport(0, 0, width, height);
        }


        public static void ApplyAndDraw()
        {
           GL.BindFramebuffer(FramebufferTarget.Framebuffer, 0);


           GL.Disable(EnableCap.CullFace);
           GL.Disable(EnableCap.DepthTest);
           GL.Disable(EnableCap.Blend);
            
           // Den normalen Screen malen
           simpleFullscreenMaterial.Draw(fullscreenQuad, ColorTextureName);

     
           for (int i = 0; i < 6; i++)
           {
              GL.BindFramebuffer(FramebufferTarget.Framebuffer, PingPongFBO0);
              if (i == 0) { horizontalBlurMaterial.Draw(fullscreenQuad, GlowTextureName); } else { horizontalBlurMaterial.Draw(fullscreenQuad, PingPongBuffer1); }

              GL.BindFramebuffer(FramebufferTarget.Framebuffer, PingPongFBO1);
              verticalBlurMaterial.Draw(fullscreenQuad, PingPongBuffer0);
           }
           
           
            GL.Enable(EnableCap.Blend);
            GL.BlendFunc(BlendingFactorSrc.One, BlendingFactorDest.One);
            GL.BindFramebuffer(FramebufferTarget.Framebuffer, 0);
            horizontalBlurMaterial.Draw(fullscreenQuad, PingPongBuffer1);
          

            

        }

    }
}
