﻿using System;
using System.Collections.Generic;
using OpenTK;
using OpenTK.Graphics.OpenGL;
using Engine.cgimin.material.simpletexture;
using Engine.cgimin.helpers;
using Engine.cgimin.camera;
using System.Diagnostics;

namespace Engine.cgimin.weather
{
    public class Rain
    {

        private int rainVOA;

        private int program;

       // private SimpleTextureMaterial material;

        private int colorTextureLocation;
        private int projectionLocation;
        private int modelLocation;
        private int cameraPositionLocation;
        private int fallUpdateLocation;
        private int speedValueLocation;

        private Stopwatch stopWatch;

        private int indexCount;

        public Rain()
        {

           // material = new SimpleTextureMaterial();

            List<float> rainData = new List<float>();
            List<int> indices = new List<int>();

            Random random = new Random();

            float xSize = 0.03f;
            float ySize = 0.55f;
            
            for (int i = 0; i < 4000; i++)
            {
                
                float xPos = random.Next(-3000, 3000) / 100.0f;
                float yPos = random.Next(-3000, 3000) / 100.0f;
                float zPos = random.Next(-3000, 3000) / 100.0f;

                rainData.Add(xPos);
                rainData.Add(yPos);
                rainData.Add(zPos);
                rainData.Add(- xSize);
                rainData.Add(- ySize);
                rainData.Add(1);
                rainData.Add(0);
                rainData.Add(1);

                rainData.Add(xPos);
                rainData.Add(yPos);
                rainData.Add(zPos);
                rainData.Add(+ xSize);
                rainData.Add(- ySize);
                rainData.Add(1);
                rainData.Add(1);
                rainData.Add(1);

                rainData.Add(xPos);
                rainData.Add(yPos);
                rainData.Add(zPos);
                rainData.Add(+ xSize);
                rainData.Add(+ ySize);
                rainData.Add(-1);
                rainData.Add(1);
                rainData.Add(0);

                rainData.Add(xPos);
                rainData.Add(yPos);
                rainData.Add(zPos);
                rainData.Add(- xSize);
                rainData.Add(+ ySize);
                rainData.Add(-1);
                rainData.Add(0);
                rainData.Add(0);

                int bIndex = i * 4;

                indices.Add(bIndex);
                indices.Add(bIndex + 2);
                indices.Add(bIndex + 1);

                indices.Add(bIndex);
                indices.Add(bIndex + 3);
                indices.Add(bIndex + 2);

            }

            indexCount = indices.Count;

            int rianVBO;
            GL.GenBuffers(1, out rianVBO);
            GL.BindBuffer(BufferTarget.ArrayBuffer, rianVBO);
            GL.BufferData(BufferTarget.ArrayBuffer, (IntPtr)(rainData.Count * sizeof(float)), rainData.ToArray(), BufferUsageHint.StaticDraw);
            GL.BindBuffer(BufferTarget.ArrayBuffer, 0);

            int indexBuffer;
            GL.GenBuffers(1, out indexBuffer);
            GL.BindBuffer(BufferTarget.ElementArrayBuffer, indexBuffer);
            GL.BufferData(BufferTarget.ElementArrayBuffer, new IntPtr(sizeof(uint) * indices.Count), indices.ToArray(), BufferUsageHint.StaticDraw);
            GL.BindBuffer(BufferTarget.ElementArrayBuffer, 0);

            GL.GenVertexArrays(1, out rainVOA);
            GL.BindVertexArray(rainVOA);
            GL.BindBuffer(BufferTarget.ElementArrayBuffer, indexBuffer);
            GL.BindBuffer(BufferTarget.ArrayBuffer, rianVBO);

            GL.EnableVertexAttribArray(0);
            GL.EnableVertexAttribArray(1);
            GL.EnableVertexAttribArray(2);

            GL.VertexAttribPointer(0, 3, VertexAttribPointerType.Float, true, Vector3.SizeInBytes * 2 + Vector2.SizeInBytes, 0);
            GL.VertexAttribPointer(1, 3, VertexAttribPointerType.Float, true, Vector3.SizeInBytes * 2 + Vector2.SizeInBytes, Vector3.SizeInBytes);
            GL.VertexAttribPointer(2, 2, VertexAttribPointerType.Float, true, Vector3.SizeInBytes * 2 + Vector2.SizeInBytes, Vector3.SizeInBytes + Vector3.SizeInBytes);
            GL.BindVertexArray(0);


            program = ShaderCompiler.CreateShaderProgram("cgimin/weather/rain/Rain_VS.glsl", "cgimin/weather/rain/Rain_FS.glsl");

            GL.BindAttribLocation(program, 0, "in_position");
            GL.BindAttribLocation(program, 1, "in_sub_position");
            GL.BindAttribLocation(program, 2, "in_uv");

            GL.LinkProgram(program);

            colorTextureLocation = GL.GetUniformLocation(program, "color_texture");
            projectionLocation = GL.GetUniformLocation(program, "projection_matrix");
            modelLocation = GL.GetUniformLocation(program, "model_matrix");
            cameraPositionLocation = GL.GetUniformLocation(program, "cam_position");
            fallUpdateLocation = GL.GetUniformLocation(program, "fall_update");
            speedValueLocation = GL.GetUniformLocation(program, "speed_value");

            stopWatch = new Stopwatch();
            stopWatch.Start();
        }

        public void Draw(int colorTexture, float speedValue)
        {
            GL.Disable(EnableCap.CullFace);
            GL.Enable(EnableCap.Blend);
            GL.BlendFunc(BlendingFactorSrc.SrcAlpha, BlendingFactorDest.OneMinusSrcAlpha);

            GL.BindVertexArray(rainVOA);

            GL.UseProgram(program);

            GL.Uniform1(colorTextureLocation, 0);
            GL.ActiveTexture(TextureUnit.Texture0);
            GL.BindTexture(TextureTarget.Texture2D, colorTexture);

            Matrix4 projection = Camera.PerspectiveProjection;
            GL.UniformMatrix4(projectionLocation, false, ref projection);

            Matrix4 rotation = Matrix4.CreateTranslation(Camera.Position) * Camera.Transformation;

            Matrix4 model = rotation.Inverted();
            GL.UniformMatrix4(modelLocation, false, ref model);

            GL.Uniform3(cameraPositionLocation, -Camera.Position);

            GL.Uniform1(fallUpdateLocation, stopWatch.ElapsedMilliseconds / 50.0f);

            GL.Uniform1(speedValueLocation, speedValue);

            GL.DrawElements(PrimitiveType.Triangles, indexCount, DrawElementsType.UnsignedInt, IntPtr.Zero);

            GL.ActiveTexture(TextureUnit.Texture0);
            GL.BindVertexArray(0);
            GL.Disable(EnableCap.Blend);
            GL.Enable(EnableCap.CullFace);

        }


    }
}
