#version 330
precision highp float;

// input aus der VAO-Datenstruktur
in vec3 in_position;
in vec3 in_sub_position;
in vec2 in_uv; 

// Parameter
uniform mat4 projection_matrix;
uniform mat4 model_matrix;
uniform vec3 cam_position;
uniform float fall_update;
uniform float speed_value;

// "texcoord" wird an den Fragment-Shader weitergegeben, daher als "out" deklariert
out vec2 texcoord;

void main()
{
	texcoord = in_uv;

	vec4 pos = vec4(cam_position,1) + vec4(in_position,1);

	pos.y -= fall_update;

	pos.x = mod(pos.x + 30.0, 60.0) - 30.0;  
	pos.y = mod(pos.y + 30.0, 60.0) - 30.0; 
	pos.z = mod(pos.z + 30.0, 60.0) - 30.0;
	
	pos *= model_matrix;
	pos +=  vec4(in_sub_position * vec3(1,1,speed_value),1);

	gl_Position = projection_matrix * pos;
}


