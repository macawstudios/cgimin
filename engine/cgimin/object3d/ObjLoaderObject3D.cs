﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using OpenTK;

namespace Engine.cgimin.object3d
{
    public class ObjLoaderObject3D : BaseObject3D
    {

        public ObjLoaderObject3D(String filePath, float scaleFactor = 1.0f, Boolean doAverageTangets = false, Boolean createVAO = true, Boolean xMirror = false)
        {

            Positions = new List<Vector3>();
            Normals = new List<Vector3>();
            UVs = new List<Vector2>();
            Tangents = new List<Vector3>();
            BiTangents = new List<Vector3>();
            Indices = new List<int>();

            List<Vector3> v = new List<Vector3>();
            List<Vector2> vt = new List<Vector2>();
            List<Vector3> vn = new List<Vector3>();

            var input = File.ReadLines(filePath);

            foreach (string line in input)
            {
                string[] parts = line.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);

                if (!xMirror)
                {
                    if (parts.Length > 0)
                    {
                        if (parts[0] == "v") v.Add(new Vector3(float.Parse(parts[1], CultureInfo.InvariantCulture) * scaleFactor, float.Parse(parts[2], CultureInfo.InvariantCulture) * scaleFactor, float.Parse(parts[3], CultureInfo.InvariantCulture) * scaleFactor));
                        if (parts[0] == "vt") vt.Add(new Vector2(float.Parse(parts[1], CultureInfo.InvariantCulture), 1.0f - float.Parse(parts[2], CultureInfo.InvariantCulture)));
                        if (parts[0] == "vn") vn.Add(new Vector3(float.Parse(parts[1], CultureInfo.InvariantCulture), float.Parse(parts[2], CultureInfo.InvariantCulture), float.Parse(parts[3], CultureInfo.InvariantCulture)));

                        if (parts[0] == "f")
                        {
                            string[] triIndicesV1 = parts[1].Split(new char[] { '/' }, StringSplitOptions.RemoveEmptyEntries);
                            string[] triIndicesV2 = parts[2].Split(new char[] { '/' }, StringSplitOptions.RemoveEmptyEntries);
                            string[] triIndicesV3 = parts[3].Split(new char[] { '/' }, StringSplitOptions.RemoveEmptyEntries);


                            addTriangle(v[Convert.ToInt32(triIndicesV1[0]) - 1], v[Convert.ToInt32(triIndicesV2[0]) - 1], v[Convert.ToInt32(triIndicesV3[0]) - 1],
                                        vn[Convert.ToInt32(triIndicesV1[2]) - 1], vn[Convert.ToInt32(triIndicesV2[2]) - 1], vn[Convert.ToInt32(triIndicesV3[2]) - 1],
                                        vt[Convert.ToInt32(triIndicesV1[1]) - 1], vt[Convert.ToInt32(triIndicesV2[1]) - 1], vt[Convert.ToInt32(triIndicesV3[1]) - 1]);
                          

                        }
                    }
                }
                else {
                    if (parts.Length > 0)
                    {
                        if (parts[0] == "v") v.Add(new Vector3(-float.Parse(parts[1], CultureInfo.InvariantCulture) * scaleFactor, float.Parse(parts[2], CultureInfo.InvariantCulture) * scaleFactor, float.Parse(parts[3], CultureInfo.InvariantCulture) * scaleFactor));
                        if (parts[0] == "vt") vt.Add(new Vector2(float.Parse(parts[1], CultureInfo.InvariantCulture), 1.0f - float.Parse(parts[2], CultureInfo.InvariantCulture)));
                        if (parts[0] == "vn") vn.Add(new Vector3(-float.Parse(parts[1], CultureInfo.InvariantCulture), float.Parse(parts[2], CultureInfo.InvariantCulture), float.Parse(parts[3], CultureInfo.InvariantCulture)));

                        if (parts[0] == "f")
                        {
                            string[] triIndicesV1 = parts[1].Split(new char[] { '/' }, StringSplitOptions.RemoveEmptyEntries);
                            string[] triIndicesV2 = parts[2].Split(new char[] { '/' }, StringSplitOptions.RemoveEmptyEntries);
                            string[] triIndicesV3 = parts[3].Split(new char[] { '/' }, StringSplitOptions.RemoveEmptyEntries);

                            addTriangle(v[Convert.ToInt32(triIndicesV1[0]) - 1], v[Convert.ToInt32(triIndicesV3[0]) - 1], v[Convert.ToInt32(triIndicesV2[0]) - 1],
                                        vn[Convert.ToInt32(triIndicesV1[2]) - 1], vn[Convert.ToInt32(triIndicesV3[2]) - 1], vn[Convert.ToInt32(triIndicesV2[2]) - 1],
                                        vt[Convert.ToInt32(triIndicesV1[1]) - 1], vt[Convert.ToInt32(triIndicesV3[1]) - 1], vt[Convert.ToInt32(triIndicesV2[1]) - 1]);
                          

                        }
                    }
                }
            }

            if (doAverageTangets == true) averageTangents();

            if (createVAO) CreateVAO();

        }


        public static void WriteObjFile(List<BaseObject3D> objects, string filename)
        {
            List<string> v = new List<string>();
            List<string> vn = new List<string>();
            List<string> vt = new List<string>();

            for (int i = 0; i < objects.Count; i++)
            {
                v.AddRange(objects[i].GetVertexStrings());
                vn.AddRange(objects[i].GetVertexNormalStrings());
                vt.AddRange(objects[i].GetUVStrings());
            }

            int indexCount = v.Count;

            int o = 1;
            List<string> f = new List<string>();
            for (int i = 0; i < indexCount / 3; i++)
            {
                string s = "f " + o.ToString() + "/" + o.ToString() + "/" + o.ToString() + " ";
                o++;
                s+= o.ToString() + "/" + o.ToString() + "/" + o.ToString() + " ";
                o++;
                s += o.ToString() + "/" + o.ToString() + "/" + o.ToString() + " ";
                o++;
                f.Add(s);
            }

            string filepath = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);

            using (StreamWriter outputFile = new StreamWriter(filepath + @"\" + filename))
            {
                outputFile.WriteLine("o " + filename);

                for (int i = 0; i < v.Count; i++) outputFile.WriteLine(v[i]);
                for (int i = 0; i < vt.Count; i++) outputFile.WriteLine(vt[i]);
                for (int i = 0; i < vn.Count; i++) outputFile.WriteLine(vn[i]);
                for (int i = 0; i < f.Count; i++) outputFile.WriteLine(f[i]);
            }

        }



    }
}
