#version 330
precision highp float;

// input aus der VAO-Datenstruktur
in vec3 in_position;
in vec2 in_uv;
in vec2 in_size;
in float in_life;

// Parameter
uniform mat4 projection_matrix;
uniform mat4 model_view_matrix;
uniform float update;
uniform float scale;

// "texcoord" wird an den Fragment-Shader weitergegeben, daher als "out" deklariert
out vec2 texcoord;
out float alpha;

void main()
{
	
	texcoord = in_uv;

	vec4 pos = model_view_matrix * vec4(in_position, 1);
	
	float size = (in_life + update) * scale;
	if (size < 0) size = 0;

	alpha = (200 - update - in_life) / 200.0;
	if (alpha < 0) alpha = 0;

	pos.x += in_size.x * size;
	pos.y += in_size.y * size + update * 0.01;

	gl_Position = projection_matrix * pos;
}


