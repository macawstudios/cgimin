﻿using System;
using System.Collections.Generic;
using OpenTK;
using OpenTK.Graphics.OpenGL;
using Engine.cgimin.helpers;
using Engine.cgimin.camera;

namespace Engine.cgimin.effects
{
    public class Explosion
    {

        private int explosionVOA;

        private int program;

        private int indexCount;

        private int colorTextureLocation;
        private int projectionLocation;
        private int modelViewLocation;
        private int updateLocation;

        public Explosion()
        {

            List<float> explosionData = new List<float>();
            List<int> indices = new List<int>();

            Random random = new Random();

            for (int i = 0; i < 50; i++)
            {
                float xPos = random.Next(-30, 30) / 30.0f;
                float yPos = random.Next(10, 30) / 30.0f;
                float zPos = random.Next(-30, 30) / 30.0f;

                float xDir = random.Next(-1000, 1000) / 6000.0f;
                float yDir = random.Next(-100, 1500) / 6000.0f;
                float zDir = random.Next(-1000, 1000) / 6000.0f;

                float size = random.Next(100) / 100.0f * 0.5f + 0.7f;

                float lifeData = random.Next(10) + 10;

                explosionData.Add(xPos);
                explosionData.Add(yPos);
                explosionData.Add(zPos);
                explosionData.Add(0);
                explosionData.Add(0);
                explosionData.Add(-size);
                explosionData.Add(-size);
                explosionData.Add(xDir);
                explosionData.Add(yDir);
                explosionData.Add(zDir);
                explosionData.Add(lifeData);

                explosionData.Add(xPos);
                explosionData.Add(yPos);
                explosionData.Add(zPos);
                explosionData.Add(1);
                explosionData.Add(0);
                explosionData.Add(size);
                explosionData.Add(-size);
                explosionData.Add(xDir);
                explosionData.Add(yDir);
                explosionData.Add(zDir);
                explosionData.Add(lifeData);

                explosionData.Add(xPos);
                explosionData.Add(yPos);
                explosionData.Add(zPos);
                explosionData.Add(1);
                explosionData.Add(1);
                explosionData.Add(size);
                explosionData.Add(size);
                explosionData.Add(xDir);
                explosionData.Add(yDir);
                explosionData.Add(zDir);
                explosionData.Add(lifeData);

                explosionData.Add(xPos); // pos
                explosionData.Add(yPos);
                explosionData.Add(zPos);
                explosionData.Add(0); // uv
                explosionData.Add(1);
                explosionData.Add(-size); // size
                explosionData.Add(size);
                explosionData.Add(xDir); // direction
                explosionData.Add(yDir);
                explosionData.Add(zDir);
                explosionData.Add(lifeData); // life data

                int bIndex = i * 4;

                indices.Add(bIndex);
                indices.Add(bIndex + 2);
                indices.Add(bIndex + 1);

                indices.Add(bIndex);
                indices.Add(bIndex + 3);
                indices.Add(bIndex + 2);
            }

            indexCount = indices.Count;

            int explosionVBO;
            GL.GenBuffers(1, out explosionVBO);
            GL.BindBuffer(BufferTarget.ArrayBuffer, explosionVBO);
            GL.BufferData(BufferTarget.ArrayBuffer, (IntPtr)(explosionData.Count * sizeof(float)), explosionData.ToArray(), BufferUsageHint.StaticDraw);
            GL.BindBuffer(BufferTarget.ArrayBuffer, 0);

            int indexBuffer;
            GL.GenBuffers(1, out indexBuffer);
            GL.BindBuffer(BufferTarget.ElementArrayBuffer, indexBuffer);
            GL.BufferData(BufferTarget.ElementArrayBuffer, new IntPtr(sizeof(uint) * indices.Count), indices.ToArray(), BufferUsageHint.StaticDraw);
            GL.BindBuffer(BufferTarget.ElementArrayBuffer, 0);

            GL.GenVertexArrays(1, out explosionVOA);
            GL.BindVertexArray(explosionVOA);
            GL.BindBuffer(BufferTarget.ElementArrayBuffer, indexBuffer);
            GL.BindBuffer(BufferTarget.ArrayBuffer, explosionVBO);

            GL.EnableVertexAttribArray(0); // vec3 pos
            GL.EnableVertexAttribArray(1); // vec2 uv
            GL.EnableVertexAttribArray(2); // vec2 size
            GL.EnableVertexAttribArray(3); // vec3 direction
            GL.EnableVertexAttribArray(4); // float life

            int strideSize = Vector3.SizeInBytes * 2 + Vector2.SizeInBytes * 2 + sizeof(float);

            GL.VertexAttribPointer(0, 3, VertexAttribPointerType.Float, true, strideSize, 0);
            GL.VertexAttribPointer(1, 2, VertexAttribPointerType.Float, true, strideSize, Vector3.SizeInBytes);
            GL.VertexAttribPointer(2, 2, VertexAttribPointerType.Float, true, strideSize, Vector3.SizeInBytes + Vector2.SizeInBytes);
            GL.VertexAttribPointer(3, 3, VertexAttribPointerType.Float, true, strideSize, Vector3.SizeInBytes + Vector2.SizeInBytes * 2);
            GL.VertexAttribPointer(4, 1, VertexAttribPointerType.Float, true, strideSize, Vector3.SizeInBytes * 2 + Vector2.SizeInBytes * 2);

            GL.BindVertexArray(0);

            program = ShaderCompiler.CreateShaderProgram("cgimin/effects/Explosion_VS.glsl", "cgimin/effects/Explosion_FS.glsl");

            GL.BindAttribLocation(program, 0, "in_position");
            GL.BindAttribLocation(program, 1, "in_uv");
            GL.BindAttribLocation(program, 2, "in_size");
            GL.BindAttribLocation(program, 3, "in_direction");
            GL.BindAttribLocation(program, 4, "in_life");

            GL.LinkProgram(program);

            colorTextureLocation = GL.GetUniformLocation(program, "color_texture");
            projectionLocation = GL.GetUniformLocation(program, "projection_matrix");
            modelViewLocation = GL.GetUniformLocation(program, "model_view_matrix");
            updateLocation = GL.GetUniformLocation(program, "update");


            GL.DisableVertexAttribArray(0); // vec3 pos
            GL.DisableVertexAttribArray(1); // vec2 uv
            GL.DisableVertexAttribArray(2); // vec2 size
            GL.DisableVertexAttribArray(3); // vec3 direction
            GL.DisableVertexAttribArray(4); // float life

        }


        public void Draw(Matrix4 transformation, int colorTexture, float update)
        {
            GL.Disable(EnableCap.CullFace);
            GL.Enable(EnableCap.Blend);
            GL.BlendFunc(BlendingFactorSrc.One, BlendingFactorDest.OneMinusSrcAlpha);

            GL.DepthMask(false);

            GL.BindVertexArray(explosionVOA);

            GL.UseProgram(program);

            GL.Uniform1(colorTextureLocation, 0);
            GL.ActiveTexture(TextureUnit.Texture0);
            GL.BindTexture(TextureTarget.Texture2D, colorTexture);

            Matrix4 projection = Camera.PerspectiveProjection;
            GL.UniformMatrix4(projectionLocation, false, ref projection);

            Matrix4 modelView = transformation * Camera.Transformation;
            GL.UniformMatrix4(modelViewLocation, false, ref modelView);

            GL.Uniform1(updateLocation, update);

            GL.DrawElements(PrimitiveType.Triangles, indexCount, DrawElementsType.UnsignedInt, IntPtr.Zero);

            GL.ActiveTexture(TextureUnit.Texture0);
            GL.BindVertexArray(0);
            GL.Disable(EnableCap.Blend);
            GL.Enable(EnableCap.CullFace);

            GL.DepthMask(true);
        }

    }
}
