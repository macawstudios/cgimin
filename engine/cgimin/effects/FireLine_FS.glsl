#version 330
precision highp float;

uniform sampler2D color_texture;

in vec2 texcoord;
in float alpha;

out vec4 outputColor;

void main()
{

    outputColor = texture(color_texture, texcoord);
	outputColor *= alpha;
}