﻿using System;
using System.Diagnostics;
using System.Collections.Generic;
using OpenTK;
using OpenTK.Graphics.OpenGL;
using Engine.cgimin.helpers;
using Engine.cgimin.camera;
using Engine.cgimin.light;

namespace Engine.cgimin.water
{
    public class Water
    {
        private int waterTileVAO;

        private int program;

        private int viewProjectionMatrixLocation;
        private int modelMatrixLocation;
        private int modelViewMatrixLocation;
        private int updateLocation;

        private int normalTextureLocation;

        private int ambientColorLocation;
        private int diffuseColorLocation;
        private int lightDirectionLocation;
        private int cameraPositionLocation;
        private int cubeTextureLocation;
        private int textureScaleLocation;
        private int textureSpeedLocation;

        private int waveLengthXLocation;
        private int waveLengthZLocation;
        private int waveHeightLocation;

        private int fogStartLocation;
        private int fogEndLocation;
        private int fogColorLocation;

        private int indexCount;

        private int update = 0;

        private Stopwatch stopWatch;

        private void generateWaterTile() {

            List<float> waterData = new List<float>();
            List<int> indices = new List<int>();

            int ind = 0;

            for (int x = 0; x < 65; x++)
            {
                for (int z = 0; z < 65; z++)
                {
                    waterData.Add(x * 4.0f);
                    waterData.Add(0);
                    waterData.Add(z * 4.0f);

                    if (x < 64 && z < 64)
                    {
                        indices.Add(ind);
                        indices.Add(ind + 65);
                        indices.Add(ind + 66);

                        indices.Add(ind);
                        indices.Add(ind + 66);
                        indices.Add(ind + 1);
                    }
                    ind++;
                }
            }

            indexCount = indices.Count;

            int waterVBO;
            GL.GenBuffers(1, out waterVBO);
            GL.BindBuffer(BufferTarget.ArrayBuffer, waterVBO);
            GL.BufferData(BufferTarget.ArrayBuffer, (IntPtr)(waterData.Count * sizeof(float)), waterData.ToArray(), BufferUsageHint.StaticDraw);
            GL.BindBuffer(BufferTarget.ArrayBuffer, 0);

            int indexBuffer;
            GL.GenBuffers(1, out indexBuffer);
            GL.BindBuffer(BufferTarget.ElementArrayBuffer, indexBuffer);
            GL.BufferData(BufferTarget.ElementArrayBuffer, new IntPtr(sizeof(uint) * indices.Count), indices.ToArray(), BufferUsageHint.StaticDraw);
            GL.BindBuffer(BufferTarget.ElementArrayBuffer, 0);

            GL.GenVertexArrays(1, out waterTileVAO);
            GL.BindVertexArray(waterTileVAO);
            GL.BindBuffer(BufferTarget.ElementArrayBuffer, indexBuffer);
            GL.BindBuffer(BufferTarget.ArrayBuffer, waterVBO);
            GL.EnableVertexAttribArray(0);
            GL.VertexAttribPointer(0, 3, VertexAttribPointerType.Float, true, Vector3.SizeInBytes, 0);
            GL.BindVertexArray(0);

        }


        public Water()
        {

            generateWaterTile();

            program = ShaderCompiler.CreateShaderProgram("cgimin/water/Water_VS.glsl", "cgimin/water/Water_FS.glsl");

            GL.BindAttribLocation(program, 0, "in_position");
            GL.LinkProgram(program);

            normalTextureLocation = GL.GetUniformLocation(program, "normal_texture");
            cubeTextureLocation = GL.GetUniformLocation(program, "cube_texture");

            modelMatrixLocation = GL.GetUniformLocation(program, "model_matrix");
            updateLocation = GL.GetUniformLocation(program, "update");

            ambientColorLocation = GL.GetUniformLocation(program, "ambient_color");
            diffuseColorLocation = GL.GetUniformLocation(program, "diffuse_color");
            lightDirectionLocation = GL.GetUniformLocation(program, "light_direction");
            cameraPositionLocation = GL.GetUniformLocation(program, "camera_position");

            waveLengthXLocation = GL.GetUniformLocation(program, "wavelength_x");
            waveLengthZLocation = GL.GetUniformLocation(program, "wavelength_z");
            waveHeightLocation = GL.GetUniformLocation(program, "waveheight");

            textureScaleLocation = GL.GetUniformLocation(program, "texture_scale");
            textureSpeedLocation = GL.GetUniformLocation(program, "texture_speed");

            viewProjectionMatrixLocation = GL.GetUniformLocation(program, "view_projection_matrix");
            modelViewMatrixLocation = GL.GetUniformLocation(program, "model_view_matrix");

            fogStartLocation = GL.GetUniformLocation(program, "fogStart");
            fogEndLocation = GL.GetUniformLocation(program, "fogEnd");
            fogColorLocation = GL.GetUniformLocation(program, "fogColor");

            stopWatch = new Stopwatch();
            stopWatch.Start();
        }


        public void Draw(float yPos, int normalTextureID, int cubeTextureID, int ambientR, int ambientG, int ambientB, int diffuseR, int diffuseG, int diffuseB, float waveLengthX, float waveLengthZ, float waveHeight, float textureScale, float textureSpeed, float waveSpeed)
        {

            GL.BindVertexArray(waterTileVAO);

            GL.UseProgram(program);

            GL.Uniform1(normalTextureLocation, 0);
            GL.ActiveTexture(TextureUnit.Texture0);
            GL.BindTexture(TextureTarget.Texture2D, normalTextureID);

            GL.Uniform1(cubeTextureLocation, 1);
            GL.ActiveTexture(TextureUnit.Texture1);
            GL.BindTexture(TextureTarget.TextureCubeMap, cubeTextureID);

            Matrix4 modelViewProjection = Camera.Transformation * Camera.PerspectiveProjection;
            GL.UniformMatrix4(viewProjectionMatrixLocation, false, ref modelViewProjection);

            GL.Uniform4(diffuseColorLocation, new Vector4(diffuseR / 255.0f, diffuseG / 255.0f, diffuseB / 255.0f, 1));
            GL.Uniform4(ambientColorLocation, new Vector4(ambientR / 255.0f, ambientG / 255.0f, ambientB / 255.0f, 1));
            GL.Uniform3(lightDirectionLocation, Light.lightDirection);
            GL.Uniform4(cameraPositionLocation, new Vector4(Camera.Position.X, Camera.Position.Y, Camera.Position.Z, 1));

            GL.Uniform1(waveLengthXLocation, waveLengthX);
            GL.Uniform1(waveLengthZLocation, waveLengthZ);
            GL.Uniform1(waveHeightLocation, waveHeight);
            GL.Uniform1(textureScaleLocation, textureScale);
            GL.Uniform1(textureSpeedLocation, textureSpeed);

            GL.Uniform1(fogStartLocation, Camera.FogStart);
            GL.Uniform1(fogEndLocation, Camera.FogEnd);
            GL.Uniform3(fogColorLocation, Camera.FogColor);

            float upd = update / 20.0f;
            GL.Uniform1(updateLocation, stopWatch.ElapsedMilliseconds / 1000.0f * waveSpeed);

            float cmpRadius = (float)Math.Sqrt(128 * 128 + 128 * 128);

            int inViewCount = 0;

            for (int x = -8; x < 8; x++)
            {
                for (int z = -8; z < 8; z++)
                {
                    if (Camera.SphereIsInFrustum(new Vector3(x * 256 + 128, 0, z * 256 + 128), cmpRadius))
                    {
                        inViewCount++;
                        Matrix4 model = Matrix4.CreateTranslation(x * 256, yPos, z * 256);
                        GL.UniformMatrix4(modelMatrixLocation, false, ref model);

                        Matrix4 modelView = model * Camera.Transformation;
                        GL.UniformMatrix4(modelViewMatrixLocation, false, ref modelView);

                        GL.DrawElements(PrimitiveType.Triangles, indexCount, DrawElementsType.UnsignedInt, IntPtr.Zero);
                    }
                }
            }

            //Console.WriteLine(inViewCount);

            GL.ActiveTexture(TextureUnit.Texture0);
            GL.BindVertexArray(0);

        }



    }
}
