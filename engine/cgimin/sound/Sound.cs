﻿using System;
using System.IO;
using OpenTK.Audio;
using OpenTK.Audio.OpenAL;

namespace Engine.cgimin.sound
{
	public class Sound
	{
		private static AudioContext context;

		private int buffer;

		/// <summary>
		/// Zugriff auf den Index der Soundquelle.
		/// Dies wird als Ziel aller nativen OpenAL genutzt. 
		/// </summary>
		public int Source
		{
			get;
		}

		/// <summary>
		/// Aktueller Status des Soundeffektes.
		/// </summary>
		public ALSourceState State => AL.GetSourceState(Source);

		/// <summary>
		/// Gain des Soundeffektes.
		/// Erlaubt Werte zwischen 0f und 1f.
		/// </summary>
		public float Gain
		{
			get
			{
				float value;
				AL.GetSource(Source, ALSourcef.Gain, out value);
				return value;
			}
			set
			{
				AL.Source(Source, ALSourcef.Gain, value);
			}
		}

		/// <summary>
		/// Pitch des Soundeffektes.
		/// </summary>
		public float Pitch
		{
			get
			{
				float value;
				AL.GetSource(Source, ALSourcef.Pitch, out value);
				return value;
			}
			set
			{
				AL.Source(Source, ALSourcef.Pitch, value);
			}
		}

		/// <summary>
		/// Erstellt eine neue Instanz.
		/// </summary>
		/// <param name="filePath">Pfad zur .wav Datei.</param>
		/// <param name="looping">Entschiedet ob der Sound immer wiederholt werden soll.</param>
		public Sound(String filePath, bool looping = false)
		{
			buffer = AL.GenBuffer();
			Source = AL.GenSource();

			AL.Source(Source, ALSourceb.Looping, looping);

			int channels, bits_per_sample, sample_rate;
			byte[] sound_data = LoadWave(File.Open(filePath, FileMode.Open), out channels, out bits_per_sample, out sample_rate);
			AL.BufferData(buffer, GetSoundFormat(channels, bits_per_sample), sound_data, sound_data.Length, sample_rate);

			AL.Source(Source, ALSourcei.Buffer, buffer);
		}

		public void UnLoad()
		{
			AL.SourceStop(Source);
			AL.DeleteSource(Source);
			AL.DeleteBuffer(buffer);
		}

		/// <summary>
		/// Spielt den Sound ab.
		/// Wird der Sound bereits abgespielt startet er von vorne.
		/// </summary>
		public void Play()
		{
			AL.SourcePlay(Source);
		}

		/// <summary>
		/// Stoppt den Sound.
		/// </summary>
		public void Stop()
		{
			AL.SourceStop(Source);
		}

		/// <summary>
		/// Pausiert den Sound an der aktuellen Position.
		/// </summary>
		public void Pause()
		{
			AL.SourcePause(Source);
		}

		/// <summary>
		/// Initialisiert die Sound Klasse.
		/// Muss vor dem Erstellen einer Sound Instanz aufgerufen werden.
		/// </summary>
		public static void Init()
		{
			context = new AudioContext();
		}

		// Loads a wave/riff audio file.
		private static byte[] LoadWave(Stream stream, out int channels, out int bits, out int rate)
		{
			if (stream == null)
				throw new ArgumentNullException(nameof(stream));

			using (BinaryReader reader = new BinaryReader(stream))
			{
				// RIFF header
				string signature = new string(reader.ReadChars(4));
				if (signature != "RIFF")
					throw new NotSupportedException("Specified stream is not a wave file.");

				int riff_chunck_size = reader.ReadInt32();

				string format = new string(reader.ReadChars(4));
				if (format != "WAVE")
					throw new NotSupportedException("Specified stream is not a wave file.");

				// WAVE header
				string format_signature = new string(reader.ReadChars(4));
				if (format_signature != "fmt ")
					throw new NotSupportedException("Specified wave file is not supported.");

				int format_chunk_size = reader.ReadInt32();
				int audio_format = reader.ReadInt16();
				int num_channels = reader.ReadInt16();
				int sample_rate = reader.ReadInt32();
				int byte_rate = reader.ReadInt32();
				int block_align = reader.ReadInt16();
				int bits_per_sample = reader.ReadInt16();

				string data_signature = new string(reader.ReadChars(4));
				if (data_signature != "data")
					throw new NotSupportedException("Specified wave file is not supported.");

				int data_chunk_size = reader.ReadInt32();

				channels = num_channels;
				bits = bits_per_sample;
				rate = sample_rate;

				return reader.ReadBytes((int)reader.BaseStream.Length);
			}
		}

		private static ALFormat GetSoundFormat(int channels, int bits)
		{
			switch (channels)
			{
			case 1: return bits == 8 ? ALFormat.Mono8 : ALFormat.Mono16;
			case 2: return bits == 8 ? ALFormat.Stereo8 : ALFormat.Stereo16;
			default: throw new NotSupportedException("The specified sound format is not supported.");
			}
		}
	}
}

