#version 330
precision highp float;

const int NUM_CASCADES = 3;

// Basis Textur und Normalmap
uniform sampler2D color_texture;
uniform sampler2D normalmap_texture;
uniform samplerCube cube_texture;

// "model_matrix" Matrix wird als Parameter erwartet, vom Typ Matrix4
uniform mat4 model_matrix;

// Parameter f�r direktionales Licht
uniform vec3 light_direction;
uniform vec4 light_ambient_color;
uniform vec4 light_diffuse_color;
uniform float fresnel_value;

// Fog Start, Ende und Farbe
uniform float fogStart;
uniform float fogEnd;
uniform vec3 fogColor;

// input vom Vertex-Shader
in vec2 fragTexcoord;
in mat3 fragTBN;
in vec3 fragV;
in vec3 fragVFresnel;

// fog
in vec4 viewPosition;

// output
out vec4 outputColor[2];

void main()
{	
	// surfaceColor ist die farbe aus der Textur...
	vec4 surfaceColor = texture(color_texture, fragTexcoord);
	if (surfaceColor.a < 0.5) discard; 

	// die Vertex-Normale berechnen
    vec4 normalc = texture(normalmap_texture, fragTexcoord).rgba;
	vec3 normal = vec3(normalc);
	normal = normalize(normal * 2.0 - 1.0); 
	normal = normalize(fragTBN * normal); 

	vec3 texcoord = normalize(reflect(fragV, normal));
	vec4 cubeColor = texture(cube_texture, texcoord);

	// die Helligkeit berechnen, resultierund aus dem Winkel 
	float brightness = clamp(dot(normalize(normal), light_direction), 0, 1);
	
	//vec3 h = normalize(light_direction + fragVFresnel);
	vec3 h = normalize(normal + fragVFresnel);
	float fresnel = dot(fragVFresnel, h);
	fresnel = max(fresnel, 0.0);
	fresnel = pow(fresnel, 5.0);
	fresnel += fresnel_value * (1.0 - fresnel);

	outputColor[1] = vec4(0,0,0,0);
	
	vec4 outputC = surfaceColor * (light_ambient_color +  brightness * light_diffuse_color) + cubeColor * fresnel * normalc.a;
	float fogFactor = (fogEnd - length(viewPosition.xyz)) / (fogEnd - fogStart);
	fogFactor = clamp(fogFactor, 0, 1);
	outputColor[0] = fogFactor * outputC + ((1 - fogFactor) * vec4(fogColor, 1));

}