#version 330
precision highp float;

uniform sampler2D sampler; 

in vec2 texcoord;
in vec2 texcoord2;

// fog
in vec4 viewPosition;

// Fog Start, Ende und Farbe
uniform float fogStart;
uniform float fogEnd;
uniform vec3 fogColor;

// output
out vec4 outputColor[2];

void main()
{
    vec4 color = texture(sampler, texcoord);
	color *= texture(sampler, texcoord2);

	float fogFactor = (fogEnd - length(viewPosition.xyz)) / (fogEnd - fogStart);
	fogFactor = clamp(fogFactor, 0, 1);
	color = fogFactor * color + ((1 - fogFactor) * vec4(fogColor, 1));

	outputColor[1] = color;
	outputColor[0] = color;

}