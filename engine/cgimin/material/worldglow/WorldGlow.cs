﻿using System;
using Engine.cgimin.camera;
using Engine.cgimin.object3d;
using OpenTK;
using OpenTK.Graphics.OpenGL;

namespace Engine.cgimin.material.worldglow
{
    public class WorldGlow : BaseMaterial
    {

        private int modelviewProjectionMatrixLocation;

        private int fogStartLocation;
        private int fogEndLocation;
        private int fogColorLocation;

        private int modelViewMatrixLocation;

        public WorldGlow()
        {
            // Shader-Programm wird aus den externen Files generiert...
            CreateShaderProgram(MATERIAL_DIRECTORY + "worldglow/WorldGlow_VS.glsl",
                                MATERIAL_DIRECTORY + "worldglow/WorldGlow_FS.glsl");

            // GL.BindAttribLocation, gibt an welcher Index in unserer Datenstruktur welchem "in" Parameter auf unserem Shader zugeordnet wird
            // folgende Befehle müssen aufgerufen werden...
            GL.BindAttribLocation(Program, 0, "in_position");
            GL.BindAttribLocation(Program, 1, "in_normal");
            GL.BindAttribLocation(Program, 2, "in_uv");

            // ...bevor das Shader-Programm "gelinkt" wird.
            GL.LinkProgram(Program);

            // Die Stelle an der im Shader der per "uniform" der Input-Paremeter "modelview_projection_matrix" definiert wird, wird ermittelt.
            modelviewProjectionMatrixLocation = GL.GetUniformLocation(Program, "modelview_projection_matrix");

            modelViewMatrixLocation = GL.GetUniformLocation(Program, "model_view_matrix");

            fogStartLocation = GL.GetUniformLocation(Program, "fogStart");
            fogEndLocation = GL.GetUniformLocation(Program, "fogEnd");
            fogColorLocation = GL.GetUniformLocation(Program, "fogColor");

            

        }

        public void Draw(BaseObject3D object3d, int textureID)
        {
            // Textur wird "gebunden"
            GL.BindTexture(TextureTarget.Texture2D, textureID);

            // das Vertex-Array-Objekt unseres Objekts wird benutzt
            GL.BindVertexArray(object3d.Vao);

            // Unser Shader Programm wird benutzt
            GL.UseProgram(Program);

            // Die Matrix, welche wir als "modelview_projection_matrix" übergeben, wird zusammengebaut:
            // Objekt-Transformation * Kamera-Transformation * Perspektivische Projektion der kamera.
            // Auf dem Shader wird jede Vertex-Position mit dieser Matrix multipliziert. Resultat ist die Position auf dem Screen.
            Matrix4 modelviewProjection = object3d.Transformation * Camera.Transformation * Camera.PerspectiveProjection;
            GL.UniformMatrix4(modelviewProjectionMatrixLocation, false, ref modelviewProjection);

            Matrix4 modelView = object3d.Transformation * Camera.Transformation;
            GL.UniformMatrix4(modelViewMatrixLocation, false, ref modelView);

            // Fog Values
            GL.Uniform1(fogStartLocation, Camera.FogStart);
            GL.Uniform1(fogEndLocation, Camera.FogEnd);
            GL.Uniform3(fogColorLocation, Camera.FogColor);

            // Das Objekt wird gezeichnet
            GL.DrawElements(PrimitiveType.Triangles, object3d.Indices.Count, DrawElementsType.UnsignedInt, IntPtr.Zero);

            // Unbinden des Vertex-Array-Objekt damit andere Operation nicht darauf basieren
            GL.BindVertexArray(0);
        }


        public override void DrawWithSettings(BaseObject3D object3d, MaterialSettings settings)
        {
            Draw(object3d, settings.colorTexture);
        }


    }
}
