#version 330
precision highp float;

uniform sampler2D sampler; 

in vec2 texcoord;

// output
out vec4 outputColor[2];

void main()
{
	gl_FragDepth = 0.999999;
	outputColor[1] = vec4(0,0,0,0);
    outputColor[0] = texture(sampler, texcoord);
}