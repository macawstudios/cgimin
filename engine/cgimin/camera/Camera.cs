﻿using System;
using OpenTK;
using System.Collections.Generic;
using OpenTK.Graphics.OpenGL;
using System.Diagnostics;

namespace Engine.cgimin.camera
{
    public class Camera
    {
        // Enumeration für die 6 Clipping-Ebenen
        public enum planeEnum : int
        {
            NEAR_PLANE = 0,
            FAR_PLANE = 1,
            LEFT_PLANE = 2,
            RIGHT_PLANE = 3,
            TOP_PLANE = 4,
            BOTTOM_PLANE = 5
        };

        // Struct für die Ebene
        public struct Plane
        {
            public float d;
            public Vector3 normal;
        }

        // frustum Clipping-Ebenen
        private static List<Plane> planes;

        // Matrizen für die Kamera Transformation (Position / Rotation) ...
        private static Matrix4 transformation;

        // ... und für die perspektivische Projektion
        private static Matrix4 perspectiveProjection;

        // Position der Kamera wird extra zwischengespeichert
        private static Vector3 position;
        private static Vector3 positionRestrictionPos = Vector3.Zero;
        private static Vector3 positionRestrictionNeg = Vector3.Zero;

        // für das steuern der fly-camera
        private static float xRotation;
        private static float yRotation;

        // behalten der Kamera-Werte, wenn die Kamera aufgrund vom Shadow-Mapping "umgebogen" wird
        private static int savedScreenWidth;
        private static int savedScreenHeight;
        private static float savedFov;
        private static float savedNear;
        private static float savedFar;
        private static Matrix4 savedProjection;
        private static Matrix4 savedTransformation;

        // dynamische Kamera-Werte, werden gespeichert um bei resize korrekt gesetzt zu sein
        private static float dynamicNear = 1.0f;
        private static float dynamicFar = 1000.0f;
        private static float dynamicFov = 60.0f;

        public static float Fov { get { return savedFov; } }

        public static void Init()
        {
            Planes = new List<Plane>();
            for (int i = 0; i < 6; i++) Planes.Add(new Plane());

            perspectiveProjection = Matrix4.Identity;
            transformation = Matrix4.Identity;
            xRotation = 0;
            yRotation = 0;
            CamPosition = Vector3.Zero;
        }

        public static void InitPositionRestriction(Vector3 _positionRestrictionPos, Vector3 _positionRestrictionNeg)
        {
            positionRestrictionPos = _positionRestrictionPos;
            positionRestrictionNeg = _positionRestrictionNeg;
        }


        // danymisches verändern der entsprechenden Werte
        public static void SetDynamicFovNearFar(float fov, float near, float far)
        {
            dynamicNear = near;
            dynamicFar = far;
            dynamicFov = fov;

            float aspectRatio = savedScreenWidth / (float)savedScreenHeight;
            Matrix4.CreatePerspectiveFieldOfView((float)(fov * Math.PI / 180.0f), aspectRatio, near, far, out perspectiveProjection);
        }

        public static void SetDynamicFov(float fov)
        {
            dynamicFov = fov;
            float aspectRatio = savedScreenWidth / (float)savedScreenHeight;
            Matrix4.CreatePerspectiveFieldOfView((float)(fov * Math.PI / 180.0f), aspectRatio, dynamicNear, dynamicFar, out perspectiveProjection);
        }

        public static void SetWidthHeightForResize(int width, int height)
        {
            SetWidthHeightFov(width, height, dynamicFov, dynamicNear, dynamicFar);
        }


        // width, height = Größe des Screens in Pixeln, fov = "Field of View", der Öffnungswinkel der Kameralinse
        public static void SetWidthHeightFov(int width, int height, float fov, float near = 1, float far = 1000)
        {
            savedScreenWidth = width;
            savedScreenHeight = height;
            savedFov = fov;
            savedNear = near;
            savedFar = far;
            
            float aspectRatio = width / (float)height;
            Matrix4.CreatePerspectiveFieldOfView((float)(fov * Math.PI / 180.0f), aspectRatio, near, far, out perspectiveProjection);

            savedProjection = perspectiveProjection;
            savedTransformation = transformation;

            CreateViewFrustumPlanes(transformation * perspectiveProjection);
        }

        // zurücksetzen vom view-port und den die gespeicherten Kamera-Werte
        public static void SetBackToLastCameraSettings()
        {
            transformation = savedTransformation;
            perspectiveProjection = savedProjection;
            GL.Viewport(0, 0, savedScreenWidth, savedScreenHeight);
            SetWidthHeightFov(savedScreenWidth, savedScreenHeight, savedFov, savedNear, savedFar);

        }

        // direktes setzen der Projektionsmatrix für das Shadow-Mapping
        public static void SetProjectionMatrix(Matrix4 projection)
        {
            perspectiveProjection = projection;
        }

        // direktes setzen der Transformationsmatrix für das Shadow-Mapping
        public static void SetTransformMatrix(Matrix4 transform)
        {
            transformation = transform;
        }

        // GUI-Mode setzen
        public static void SetToGUICam()
        {
            if (savedScreenHeight > 0)
            {
                Matrix4 ddProjection = new Matrix4();
                int w = 1080 * savedScreenWidth / savedScreenHeight;
                Matrix4.CreateOrthographicOffCenter(-w / 2, w / 2, -540, 540, -1, 1, out ddProjection);
                Camera.SetProjectionMatrix(ddProjection);
                GL.Viewport(0, 0, savedScreenWidth, savedScreenHeight);
            }
        }

        // Canvas Breite (für GUI)
        public static int GetGuiCanvasWidth()
        {
            return savedScreenWidth;
        } 


        public static Matrix4 GetTransformMatrix()
        {
            return transformation;
        }

        // Generierung der Kamera-Transformation per LookAt
        // Position des Kamera-"Auges", Punkt auf den geschaut wird, "Nach Oben" Richtung der Kamera (z.B. um den Blick schräg zu stellen)
        public static void SetLookAt(Vector3 eye, Vector3 target, Vector3 up)
        {
            CamPosition = eye;
            transformation = Matrix4.LookAt(eye, target, up);
            savedTransformation = transformation;
            CreateViewFrustumPlanes(transformation * perspectiveProjection);
        }


        public static void UpdateFlyCamera
           (
            bool rotLeft, bool rotRight,
            bool moveForward, bool moveBack,
            bool moveUp = false, bool moveDown = false,
            bool tiltFoward = false, bool tiltBackward = false,
            bool moveLeft = false, bool moveRight = false,
            float speed_trans = 0.08f, float speed_rot = 0.025f
           )
        {
            if (rotLeft) yRotation -= speed_rot;
            if (rotRight) yRotation += speed_rot;

            if (moveForward) CamPosition -= new Vector3(transformation.Column2.X, transformation.Column2.Y, transformation.Column2.Z) * speed_trans;
            if (moveBack) CamPosition += new Vector3(transformation.Column2.X, transformation.Column2.Y, transformation.Column2.Z) * speed_trans;

            if (moveUp) CamPosition += new Vector3(transformation.Column1.X, transformation.Column1.Y, transformation.Column1.Z) * speed_trans;
            if (moveDown) CamPosition -= new Vector3(transformation.Column1.X, transformation.Column1.Y, transformation.Column1.Z) * speed_trans;

            if (tiltFoward) xRotation += speed_rot;
            if (tiltBackward) xRotation -= speed_rot;

            if (moveLeft) CamPosition -= new Vector3(transformation.Column0.X, transformation.Column0.Y, transformation.Column0.Z) * speed_trans;
            if (moveRight) CamPosition += new Vector3(transformation.Column0.X, transformation.Column0.Y, transformation.Column0.Z) * speed_trans;

            transformation = Matrix4.Identity;
            transformation *= Matrix4.CreateTranslation(-CamPosition.X, -CamPosition.Y, -CamPosition.Z);
            transformation *= Matrix4.CreateRotationX(xRotation);
            transformation *= Matrix4.CreateRotationY(yRotation);

            savedTransformation = transformation;

            CreateViewFrustumPlanes(transformation * perspectiveProjection);
        }

        
        public static void UpdateMouseCamera(float strafeSpeed, bool strafeLeft, bool strafeRight, bool moveForward, bool moveBack, float mouseDeltaUp, float mouseDeltaLeft)
        {
            yRotation -= mouseDeltaLeft;
            xRotation -= mouseDeltaUp;

            if (moveForward) CamPosition -= new Vector3(transformation.Column2.X, transformation.Column2.Y, transformation.Column2.Z) * strafeSpeed;
            if (moveBack) CamPosition += new Vector3(transformation.Column2.X, transformation.Column2.Y, transformation.Column2.Z) * strafeSpeed;

            if (strafeLeft) CamPosition -= new Vector3(transformation.Column0.X, transformation.Column0.Y, transformation.Column0.Z) * strafeSpeed;
            if (strafeRight) CamPosition += new Vector3(transformation.Column0.X, transformation.Column0.Y, transformation.Column0.Z) * strafeSpeed;

            transformation = Matrix4.Identity;
            transformation *= Matrix4.CreateTranslation(-CamPosition.X, -CamPosition.Y, -CamPosition.Z);
            transformation *= Matrix4.CreateRotationY(yRotation);
            transformation *= Matrix4.CreateRotationX(xRotation);

            savedTransformation = transformation;

            CreateViewFrustumPlanes(transformation * perspectiveProjection);
        }


        public static void CreateViewFrustumPlanes(Matrix4 mat)
        {
            // left
            Plane plane = new Plane();
            plane.normal.X = mat.M14 + mat.M11;
            plane.normal.Y = mat.M24 + mat.M21;
            plane.normal.Z = mat.M34 + mat.M31;
            plane.d = mat.M44 + mat.M41;
            Planes[(int)planeEnum.LEFT_PLANE] = plane;

            // right
            plane = new Plane();
            plane.normal.X = mat.M14 - mat.M11;
            plane.normal.Y = mat.M24 - mat.M21;
            plane.normal.Z = mat.M34 - mat.M31;
            plane.d = mat.M44 - mat.M41;
            Planes[(int)planeEnum.RIGHT_PLANE] = plane;

            // bottom
            plane = new Plane();
            plane.normal.X = mat.M14 + mat.M12;
            plane.normal.Y = mat.M24 + mat.M22;
            plane.normal.Z = mat.M34 + mat.M32;
            plane.d = mat.M44 + mat.M42;
            Planes[(int)planeEnum.BOTTOM_PLANE] = plane;

            // top
            plane = new Plane();
            plane.normal.X = mat.M14 - mat.M12;
            plane.normal.Y = mat.M24 - mat.M22;
            plane.normal.Z = mat.M34 - mat.M32;
            plane.d = mat.M44 - mat.M42;
            Planes[(int)planeEnum.TOP_PLANE] = plane;

            // near
            plane = new Plane();
            plane.normal.X = mat.M14 + mat.M13;
            plane.normal.Y = mat.M24 + mat.M23;
            plane.normal.Z = mat.M34 + mat.M33;
            plane.d = mat.M44 + mat.M43;
            Planes[(int)planeEnum.NEAR_PLANE] = plane;

            // far
            plane = new Plane();
            plane.normal.X = mat.M14 - mat.M13;
            plane.normal.Y = mat.M24 - mat.M23;
            plane.normal.Z = mat.M34 - mat.M33;
            plane.d = mat.M44 - mat.M43;
            Planes[(int)planeEnum.FAR_PLANE] = plane;

            // Normalisieren
            for(int i = 0; i< 6; i++)
            {                
                plane = Planes[i];

                float length = plane.normal.Length;
                plane.normal.X = plane.normal.X / length;
                plane.normal.Y = plane.normal.Y / length;
                plane.normal.Z = plane.normal.Z / length;
                plane.d = plane.d / length;

                Planes[i] = plane;
            }
        }


        private static float SignedDistanceToPoint(int planeID, Vector3 pt)
        {
            return Vector3.Dot(Planes[planeID].normal, pt) + Planes[planeID].d;
        }


        public static bool SphereIsInFrustum(Vector3 center, float radius)
        {
            for (int i = 0; i < 6; i++)
            {
                if (SignedDistanceToPoint(i, center) < -radius)
                {
                    return false;
                }
            }
            return true;
        }

		public static void SetupFog(float fogStart, float fogEnd, Vector3 fogColor)
		{
			FogStart = fogStart;
			FogEnd = fogEnd;
			FogColor = fogColor;
		}

		public static float FogStart {
			get;
			set;
		}

		public static float FogEnd {
			get;
			set;
		}

		public static Vector3 FogColor {
			get;
			set;
		}

        // Getter
        public static Vector3 Position
        {
            get { return CamPosition; }
        }


        public static Matrix4 Transformation
        {
            get { return transformation; }
        }


        public static Matrix4 PerspectiveProjection
        {
            get { return perspectiveProjection; }
        }

        public static Vector3 CamPosition
        {
            get
            {
                return position;
            }

            set
            {
                if (positionRestrictionPos != Vector3.Zero || positionRestrictionNeg != Vector3.Zero)
                {
                    float camPosX = Clamp(value.X, positionRestrictionNeg.X, positionRestrictionPos.X);
                    float camPosY = Clamp(value.Y, positionRestrictionNeg.Y, positionRestrictionPos.Y);
                    float camPosZ = Clamp(value.Z, positionRestrictionNeg.Z, positionRestrictionPos.Z);

                    position = new Vector3(camPosX, camPosY, camPosZ);
                }
                else
                {
                    position = value;
                }
            }
        }

        public static List<Plane> Planes
        {
            get
            {
                return planes;
            }

            private set
            {
                planes = value;
            }
        }

        public static float Clamp(float value, float min, float max)
        {
            return (value < min) ? min : (value > max) ? max : value;
        }
    }
}
