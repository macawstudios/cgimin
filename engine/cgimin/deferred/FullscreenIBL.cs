﻿using System;
using Engine.cgimin.camera;
using Engine.cgimin.object3d;
using OpenTK;
using OpenTK.Graphics.OpenGL;
using Engine.cgimin.material;
using Engine.cgimin.texture;

namespace Engine.cgimin.deferred
{
    public class FullscreenIBL : BaseMaterial
    {
        private int gColorRoughnessLocation;
        private int gNormalLocation;
        private int gPositionLocation;
        private int gMetalnessLocation;
        private int gGlowLocation;

        private int iblSpcularCubeLocation;
        private int iblIrradianceCubeLocation;

        private int cameraPosLocation;

        private int iblLookupTexture;
        private int iblLookupTextureLocation;

        public FullscreenIBL()
        {
            iblLookupTexture = TextureManager.LoadTexture("data/textures/ibl_brdf_lut.png");

            CreateShaderProgram("cgimin/deferred/FullscreenIBL_VS.glsl", "cgimin/deferred/FullscreenIBL_FS.glsl");

            // GL.BindAttribLocation, gibt an welcher Index in unserer Datenstruktur welchem "in" Parameter auf unserem Shader zugeordnet wird
            // folgende Befehle müssen aufgerufen werden...
            GL.BindAttribLocation(Program, 0, "in_position");
            GL.BindAttribLocation(Program, 1, "in_normal");
            GL.BindAttribLocation(Program, 2, "in_uv");

            // ...bevor das Shader-Programm "gelinkt" wird.
            GL.LinkProgram(Program);

            gColorRoughnessLocation = GL.GetUniformLocation(Program, "gColorRoughness");
            gNormalLocation = GL.GetUniformLocation(Program, "gNormal");
            gPositionLocation = GL.GetUniformLocation(Program, "gPosition");
            gMetalnessLocation = GL.GetUniformLocation(Program, "gMetalness");
            gGlowLocation = GL.GetUniformLocation(Program, "gGlow");

            iblSpcularCubeLocation = GL.GetUniformLocation(Program, "iblSpecular");
            iblIrradianceCubeLocation = GL.GetUniformLocation(Program, "iblIrradiance");

            cameraPosLocation = GL.GetUniformLocation(Program, "camera_position");

            iblLookupTextureLocation = GL.GetUniformLocation(Program, "brdfLUT");
        }

        public void Draw(BaseObject3D object3d, int colorRoughnessBufferTex, int normalBufferTex, int positionBufferTex, int metalnessBufferTex, int glowBufferTex, int iblSpecularCube, int iblIrradianceCube)
        {
            // das Vertex-Array-Objekt unseres Objekts wird benutzt
            GL.BindVertexArray(object3d.Vao);

            // Unser Shader Programm wird benutzt
            GL.UseProgram(Program);

            // Farb-Textur wird "gebunden"
            GL.Uniform1(gColorRoughnessLocation, 0);
            GL.ActiveTexture(TextureUnit.Texture0);
            GL.BindTexture(TextureTarget.Texture2D, colorRoughnessBufferTex);

            GL.Uniform1(gNormalLocation, 1);
            GL.ActiveTexture(TextureUnit.Texture1);
            GL.BindTexture(TextureTarget.Texture2D, normalBufferTex);

            GL.Uniform1(gPositionLocation, 2);
            GL.ActiveTexture(TextureUnit.Texture2);
            GL.BindTexture(TextureTarget.Texture2D, positionBufferTex);

            GL.Uniform1(gMetalnessLocation, 3);
            GL.ActiveTexture(TextureUnit.Texture3);
            GL.BindTexture(TextureTarget.Texture2D, metalnessBufferTex);

            GL.Uniform1(iblLookupTextureLocation, 4);
            GL.ActiveTexture(TextureUnit.Texture4);
            GL.BindTexture(TextureTarget.Texture2D, iblLookupTexture);

            GL.Uniform1(gGlowLocation, 5);
            GL.ActiveTexture(TextureUnit.Texture5);
            GL.BindTexture(TextureTarget.Texture2D, glowBufferTex);

            // Cube Mapping -Textur wird "gebunden"
            GL.Uniform1(iblSpcularCubeLocation, 6);
            GL.ActiveTexture(TextureUnit.Texture6);
            GL.BindTexture(TextureTarget.TextureCubeMap, iblSpecularCube);
           
            GL.Uniform1(iblIrradianceCubeLocation, 7);
            GL.ActiveTexture(TextureUnit.Texture7);
            GL.BindTexture(TextureTarget.TextureCubeMap, iblIrradianceCube);
           

            // Positions Parameter
            GL.Uniform4(cameraPosLocation, new Vector4(Camera.Position.X, Camera.Position.Y, Camera.Position.Z, 1));

            // Das Objekt wird gezeichnet
            GL.DrawElements(PrimitiveType.Triangles, object3d.Indices.Count, DrawElementsType.UnsignedInt, IntPtr.Zero);

            // Unbinden des Vertex-Array-Objekt damit andere Operation nicht darauf basieren
            GL.BindVertexArray(0);

            // Active Textur wieder auf 0, um andere Materialien nicht durcheinander zu bringen
            GL.ActiveTexture(TextureUnit.Texture0);
            GL.BindTexture(TextureTarget.Texture2D, 0);
        }


        public override void DrawWithSettings(BaseObject3D object3d, MaterialSettings settings)
        {
            //throw new NotImplementedException();
        }
    }
}
