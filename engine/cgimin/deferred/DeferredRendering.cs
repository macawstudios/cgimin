﻿using System;
using OpenTK;
using OpenTK.Graphics.OpenGL;
using OpenTK.Graphics;
using Engine.cgimin.texture;
using Engine.cgimin.object3d;
using Engine.cgimin.postprocessing;
using Engine.cgimin.octree;
using Engine.cgimin.deferred.lights;

namespace Engine.cgimin.deferred
{
    public class DeferredSceneDesc
    {
        public int SpecularIBL;
        public int IrradianceIBL;
    }


    public class DeferredRendering
    {

        private const bool ENABLE_HALF_SIZE_RENDERING = false;
        
        // Fallback Shadow Textures
        public static int NoShadowTextureID;
        public static int AdditionalFallbackTexture;
        public static int NormalFallbackTexture;

        // G Buffers
        public static int GColorAndRoughnessBuffer;
        public static int GMetalnessAndShadowBuffer;
        public static int GPositionBuffer;
        public static int GNormalBuffer;
        public static int GGlowBuffer;

        private static int GFramebufferName;
        public static int width;
        public static int height;

        // IBL
        private static int IBLFramebufferName;
        private static int IBLTexture;

        // Materials
        public static DeferredSphereLightMaterial SphereLightMaterial;
        public static DeferredNullShadermaterial NullMaterial;

        private static SimpleFullscreenMaterial simpleFullscreenMaterial;
        private static FullscreenIBL fullscreenIBL;
        private static BlurHorizontalMaterial horizontalBlurMaterial;
        private static BlurVerticalMaterial verticalBlurMaterial;

        // Sphere Light Object
        private static ObjLoaderObject3D sphereLightObject;

        private static BaseObject3D fullscreenQuad;
        
        // Ping Pong Buffer
        private static int PingPongFBO0;
        private static int PingPongBuffer0;

        private static int PingPongFBO1;
        private static int PingPongBuffer1;

        public static void Init(int screenWidth, int screenHeight)
        {
            NoShadowTextureID = TextureManager.LoadTexture("data/textures/no_shadow.tif");
            AdditionalFallbackTexture = TextureManager.LoadTexture("data/textures/additional_fallback.tif");
            NormalFallbackTexture = TextureManager.LoadTexture("data/textures/normal_fallback.tif");

            fullscreenQuad = new BaseObject3D();
            fullscreenQuad.addTriangle(new Vector3(1, -1, 0), new Vector3(-1, -1, 0), new Vector3(1, 1, 0), new Vector2(1, 0), new Vector2(0, 0), new Vector2(1, 1));
            fullscreenQuad.addTriangle(new Vector3(-1, -1, 0), new Vector3(1, 1, 0), new Vector3(-1, 1, 0), new Vector2(0, 0), new Vector2(1, 1), new Vector2(0, 1));
            fullscreenQuad.CreateVAO();

            // Material init
            simpleFullscreenMaterial = new SimpleFullscreenMaterial();
            NullMaterial = new DeferredNullShadermaterial();
            fullscreenIBL = new FullscreenIBL();
            SphereLightMaterial = new DeferredSphereLightMaterial();

            horizontalBlurMaterial = new BlurHorizontalMaterial();
            verticalBlurMaterial = new BlurVerticalMaterial();

            if (ENABLE_HALF_SIZE_RENDERING)
            {
                screenWidth = screenWidth / 2;
                screenHeight = screenHeight / 2;
            }

            width = screenWidth;
            height = screenHeight;

            sphereLightObject = new ObjLoaderObject3D("../../data/objects/0_light.obj", 10.0f);

            // IBL Buffer
            IBLFramebufferName = 0;
            GL.GenFramebuffers(1, out IBLFramebufferName);
            GL.BindFramebuffer(FramebufferTarget.Framebuffer, IBLFramebufferName);

            GL.GenTextures(1, out IBLTexture);
            GL.BindTexture(TextureTarget.Texture2D, IBLTexture);
            GL.TexImage2D(TextureTarget.Texture2D, 0, PixelInternalFormat.Rgba, screenWidth, screenHeight, 0, PixelFormat.Rgba, PixelType.UnsignedInt8888, IntPtr.Zero);
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMagFilter, (int)TextureMagFilter.Linear);
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMinFilter, (int)TextureMinFilter.Linear);
            GL.FramebufferTexture(FramebufferTarget.Framebuffer, FramebufferAttachment.ColorAttachment0, IBLTexture, 0);

            
            int depthrenderbuffer2;
            GL.GenRenderbuffers(1, out depthrenderbuffer2);
            GL.BindRenderbuffer(RenderbufferTarget.Renderbuffer, depthrenderbuffer2);
            GL.RenderbufferStorage(RenderbufferTarget.Renderbuffer, RenderbufferStorage.Depth32fStencil8, screenWidth, screenHeight);
            GL.FramebufferRenderbuffer(FramebufferTarget.Framebuffer, FramebufferAttachment.DepthStencilAttachment, RenderbufferTarget.Renderbuffer, depthrenderbuffer2);

            // G Buffers
            GFramebufferName = 0;
            GL.GenFramebuffers(1, out GFramebufferName);
            GL.BindFramebuffer(FramebufferTarget.Framebuffer, GFramebufferName);

            GL.GenTextures(1, out GColorAndRoughnessBuffer);
            GL.BindTexture(TextureTarget.Texture2D, GColorAndRoughnessBuffer);
            GL.TexImage2D(TextureTarget.Texture2D, 0, PixelInternalFormat.Rgba, screenWidth, screenHeight, 0, PixelFormat.Rgba, PixelType.UnsignedInt8888, IntPtr.Zero);
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMagFilter, (int)TextureMagFilter.Linear);
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMinFilter, (int)TextureMinFilter.Linear);
            GL.FramebufferTexture(FramebufferTarget.Framebuffer, FramebufferAttachment.ColorAttachment0, GColorAndRoughnessBuffer, 0);

            GL.GenTextures(1, out GPositionBuffer);
            GL.BindTexture(TextureTarget.Texture2D, GPositionBuffer);
            GL.TexImage2D(TextureTarget.Texture2D, 0, PixelInternalFormat.Rgb32f, screenWidth, screenHeight, 0, PixelFormat.Rgb, PixelType.Float, IntPtr.Zero);
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMagFilter, (int)TextureMagFilter.Linear);
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMinFilter, (int)TextureMinFilter.Linear);
            GL.FramebufferTexture(FramebufferTarget.Framebuffer, FramebufferAttachment.ColorAttachment1, GPositionBuffer, 0);

            GL.GenTextures(1, out GNormalBuffer);
            GL.BindTexture(TextureTarget.Texture2D, GNormalBuffer);
            GL.TexImage2D(TextureTarget.Texture2D, 0, PixelInternalFormat.Rgb16f, screenWidth, screenHeight, 0, PixelFormat.Rgb, PixelType.Float, IntPtr.Zero);
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMagFilter, (int)TextureMagFilter.Linear);
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMinFilter, (int)TextureMinFilter.Linear);
            GL.FramebufferTexture(FramebufferTarget.Framebuffer, FramebufferAttachment.ColorAttachment2, GNormalBuffer, 0);

            GL.GenTextures(1, out GMetalnessAndShadowBuffer);
            GL.BindTexture(TextureTarget.Texture2D, GMetalnessAndShadowBuffer);
            GL.TexImage2D(TextureTarget.Texture2D, 0, PixelInternalFormat.Rg8, screenWidth, screenHeight, 0, PixelFormat.Rg, PixelType.UnsignedByte, IntPtr.Zero);
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMagFilter, (int)TextureMagFilter.Linear);
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMinFilter, (int)TextureMinFilter.Linear);
            GL.FramebufferTexture(FramebufferTarget.Framebuffer, FramebufferAttachment.ColorAttachment3, GMetalnessAndShadowBuffer, 0);

            GL.GenTextures(1, out GGlowBuffer);
            GL.BindTexture(TextureTarget.Texture2D, GGlowBuffer);
            GL.TexImage2D(TextureTarget.Texture2D, 0, PixelInternalFormat.Rgb, screenWidth, screenHeight, 0, PixelFormat.Rgb, PixelType.UnsignedByte, IntPtr.Zero);
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMagFilter, (int)TextureMagFilter.Linear);
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMinFilter, (int)TextureMinFilter.Linear);
            GL.FramebufferTexture(FramebufferTarget.Framebuffer, FramebufferAttachment.ColorAttachment4, GGlowBuffer, 0);

            DrawBuffersEnum[] drawEnum = { DrawBuffersEnum.ColorAttachment0, DrawBuffersEnum.ColorAttachment1, DrawBuffersEnum.ColorAttachment2, DrawBuffersEnum.ColorAttachment3, DrawBuffersEnum.ColorAttachment4 };
            GL.DrawBuffers(5, drawEnum);

            int depthrenderbuffer;
            GL.GenRenderbuffers(1, out depthrenderbuffer);
            GL.BindRenderbuffer(RenderbufferTarget.Renderbuffer, depthrenderbuffer);
            GL.RenderbufferStorage(RenderbufferTarget.Renderbuffer, RenderbufferStorage.DepthComponent, screenWidth, screenHeight);
            GL.FramebufferRenderbuffer(FramebufferTarget.Framebuffer, FramebufferAttachment.DepthAttachment, RenderbufferTarget.Renderbuffer, depthrenderbuffer);

            /// ---- Ping Pong buffer for glow
     
             // Ping-Pong Buffer 0
            GL.GenFramebuffers(1, out PingPongFBO0);
            GL.GenTextures(1, out PingPongBuffer0);
            GL.BindFramebuffer(FramebufferTarget.Framebuffer, PingPongFBO0);
            GL.BindTexture(TextureTarget.Texture2D, PingPongBuffer0);
            GL.TexImage2D(TextureTarget.Texture2D, 0, PixelInternalFormat.Rgba, screenWidth, screenHeight, 0, PixelFormat.Bgra, PixelType.UnsignedByte, IntPtr.Zero);
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMagFilter, (int)TextureMagFilter.Linear);
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMinFilter, (int)TextureMinFilter.Linear);
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureWrapS, (int)TextureWrapMode.ClampToEdge);
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureWrapT, (int)TextureWrapMode.ClampToEdge);
            GL.FramebufferTexture(FramebufferTarget.Framebuffer, FramebufferAttachment.ColorAttachment0, PingPongBuffer0, 0);

            // Ping-Pong Buffer 1
            GL.GenFramebuffers(1, out PingPongFBO1);
            GL.GenTextures(1, out PingPongBuffer1);
            GL.BindFramebuffer(FramebufferTarget.Framebuffer, PingPongFBO1);
            GL.BindTexture(TextureTarget.Texture2D, PingPongBuffer1);
            GL.TexImage2D(TextureTarget.Texture2D, 0, PixelInternalFormat.Rgba, screenWidth, screenHeight, 0, PixelFormat.Bgra, PixelType.UnsignedByte, IntPtr.Zero);
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMagFilter, (int)TextureMagFilter.Linear);
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMinFilter, (int)TextureMinFilter.Linear);
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureWrapS, (int)TextureWrapMode.ClampToEdge);
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureWrapT, (int)TextureWrapMode.ClampToEdge);
            GL.FramebufferTexture(FramebufferTarget.Framebuffer, FramebufferAttachment.ColorAttachment0, PingPongBuffer1, 0);


            FramebufferErrorCode eCode = GL.CheckFramebufferStatus(FramebufferTarget.Framebuffer);
            if (eCode != FramebufferErrorCode.FramebufferComplete)
            {
                Console.WriteLine("GBuffer init wrong" + eCode.ToString());
            }
            else
            {
                Console.WriteLine("GBuffer init Correct");
            }

            GL.Enable(EnableCap.TextureCubeMapSeamless);

            GL.StencilMask(0xFFFFFFFF);
        }

        public static void DrawSphereLight(Vector3 position, float radius, Vector3 color)
        {
            sphereLightObject.Transformation = Matrix4.Identity;
            sphereLightObject.Transformation *= Matrix4.CreateTranslation(position);
            SphereLightMaterial.Draw(sphereLightObject, radius, color);
        }


        public static void StartGBufferRendering()
        {
            GL.BindFramebuffer(FramebufferTarget.Framebuffer, GFramebufferName);
            DrawBuffersEnum[] drawEnum = { DrawBuffersEnum.ColorAttachment0, DrawBuffersEnum.ColorAttachment1, DrawBuffersEnum.ColorAttachment2, DrawBuffersEnum.ColorAttachment3, DrawBuffersEnum.ColorAttachment4 };
            GL.DrawBuffers(5, drawEnum);

            GL.ClearColor(new Color4(0, 0, 0, 0));
            GL.DepthMask(true);
            GL.Enable(EnableCap.DepthTest);
            GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);

            GL.Viewport(0, 0, width, height);
        }


        public static void DrawScreen(DeferredSceneDesc sceneDescription, Octree lightOctree, Action CustomGlowRender = null, Action CustomColorRender = null, Action CustomLightRender = null)
        {
            // Depth in den Main-Buffer kopieren
            GL.BindFramebuffer(FramebufferTarget.ReadFramebuffer, GFramebufferName);
            GL.BindFramebuffer(FramebufferTarget.DrawFramebuffer, IBLFramebufferName);
            GL.BlitFramebuffer(0, 0, width, height, 0, 0, width, height, ClearBufferMask.DepthBufferBit, BlitFramebufferFilter.Nearest);
            
            // Auf den Main-Buffer zeichnen
            GL.BindFramebuffer(FramebufferTarget.Framebuffer, IBLFramebufferName);
            GL.Disable(EnableCap.CullFace);
            GL.Disable(EnableCap.DepthTest);
            GL.Disable(EnableCap.Blend);
            
            // Komplette Szene mit IBL-Lighting
            fullscreenIBL.Draw(fullscreenQuad, GColorAndRoughnessBuffer, GNormalBuffer, GPositionBuffer, GMetalnessAndShadowBuffer, GGlowBuffer, sceneDescription.SpecularIBL, sceneDescription.IrradianceIBL);

            // Stencil Pass
            GL.Enable(EnableCap.StencilTest);
            GL.Enable(EnableCap.DepthTest);
            GL.Disable(EnableCap.CullFace);
            GL.ColorMask(false, false, false, false);
            //GL.DepthMask(false);

            GL.ClearStencil(0);
            GL.Clear(ClearBufferMask.StencilBufferBit);
            GL.StencilFunc(StencilFunction.Always, 0, 0);
            GL.StencilOpSeparate(StencilFace.Front, StencilOp.Keep, StencilOp.IncrWrap, StencilOp.Keep);
            GL.StencilOpSeparate(StencilFace.Back, StencilOp.Keep, StencilOp.DecrWrap, StencilOp.Keep);
            
            NullMaterial.PrepareDraw(sphereLightObject);
            lightOctree.Draw();
            CustomLightRender?.Invoke();
            NullMaterial.FinishDraw();

            // Light Pass
            GL.ColorMask(true, true, true, true);
            GL.DepthMask(true);
            GL.StencilFunc(StencilFunction.Equal, 0, 0xFF);
            GL.Disable(EnableCap.DepthTest);
            GL.Enable(EnableCap.CullFace);
            GL.CullFace(CullFaceMode.Back);
            GL.Enable(EnableCap.Blend);
            GL.BlendFunc(BlendingFactorSrc.One, BlendingFactorDest.One);
            
            SphereLightMaterial.PrepareDraw(sphereLightObject);
            lightOctree.Draw();
            /*
            foreach (int o in Octree.DrawnIndexList)
            {
                OctreeEntity entety = lightOctree.GetEntityFromDrawIndex(o);
                sphereLightObject.Transformation = entety.Transform;
                SphereLightMaterial.Draw(sphereLightObject, entety.MaterialSetting.lightRadius, new Vector3(1.0f, 0.5f, 0.5f));
            }
            */
            CustomLightRender?.Invoke();

            SphereLightMaterial.FinishDraw();
            GL.Disable(EnableCap.Blend);
            GL.Disable(EnableCap.StencilTest);

            GL.Enable(EnableCap.DepthTest);
            GL.DepthMask(true);
 

            GL.BindFramebuffer(FramebufferTarget.Framebuffer, GFramebufferName);
            DrawBuffersEnum[] drawEnum2 = { DrawBuffersEnum.ColorAttachment4 };
            GL.DrawBuffers(1, drawEnum2);
            CustomGlowRender?.Invoke();

            GL.BindFramebuffer(FramebufferTarget.Framebuffer, IBLFramebufferName);
            DrawBuffersEnum[] drawEnum = { DrawBuffersEnum.ColorAttachment0 };
            GL.DrawBuffers(1, drawEnum);
            CustomColorRender?.Invoke();

            GL.Disable(EnableCap.DepthTest);
            GL.Disable(EnableCap.Blend);
            GL.Disable(EnableCap.CullFace);

            
            for (int i = 0; i < 4; i++)
            {
                GL.BindFramebuffer(FramebufferTarget.Framebuffer, PingPongFBO0);
                if (i == 0) { horizontalBlurMaterial.Draw(fullscreenQuad, GGlowBuffer); } else { horizontalBlurMaterial.Draw(fullscreenQuad, PingPongBuffer1); }

                GL.BindFramebuffer(FramebufferTarget.Framebuffer, PingPongFBO1);
                verticalBlurMaterial.Draw(fullscreenQuad, PingPongBuffer0);
            }

            GL.Enable(EnableCap.Blend);
            GL.BlendFunc(BlendingFactorSrc.One, BlendingFactorDest.One);
            
            GL.BindFramebuffer(FramebufferTarget.Framebuffer, IBLFramebufferName);
            horizontalBlurMaterial.Draw(fullscreenQuad, PingPongBuffer1);
            GL.Disable(EnableCap.Blend);


            if (ENABLE_HALF_SIZE_RENDERING)
            {
                GL.Viewport(0, 0, width * 2, height * 2);
            }

            GL.BindFramebuffer(FramebufferTarget.Framebuffer, 0);
            simpleFullscreenMaterial.Draw(fullscreenQuad, IBLTexture);

            GL.Enable(EnableCap.CullFace);

        }

    }
}
