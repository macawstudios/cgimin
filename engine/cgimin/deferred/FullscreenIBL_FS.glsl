#version 330 core
precision highp float;

uniform sampler2D gColorRoughness;
uniform sampler2D gNormal;
uniform sampler2D gPosition;
uniform sampler2D gMetalness;
uniform sampler2D gGlow;

uniform sampler2D brdfLUT;

uniform samplerCube iblSpecular;
uniform samplerCube iblIrradiance;

uniform vec4 camera_position;

in vec2 texcoord;
out vec4 outputColor;

const float ao = 1.0; //verdacht auf ambient occlusion

vec3 FresnelSchlickRoughness(float cosTheta, vec3 F0, float roughness)
{
    return F0 + (max(vec3(1.0 - roughness), F0) - F0) * pow(1.0 - cosTheta, 5.0);
}  

void main()
{             
	vec3 N = texture(gNormal, texcoord).rgb;
	vec4 glow = texture(gGlow, texcoord).rgba;
	vec4 pos = texture(gPosition, texcoord);	
	vec3 V = vec3(normalize(camera_position - pos));
	vec3 R = reflect(-V, N);  
	
	vec4 c = texture(gColorRoughness, texcoord);
	vec3 albedo = c.rgb;
	float roughness = c.a;
	
	vec2 metalAndShadow = texture(gMetalness, texcoord).rg;
	float metallic = metalAndShadow.r;
	
	vec3 F0 = vec3(0.04, 0.04, 0.04); 
    F0 = mix(F0, albedo, metallic);

	vec3 F = FresnelSchlickRoughness(max(dot(N, V), 0.0), F0, roughness);

	vec3 kS = F;
	vec3 kD = 1.0 - kS;
	kD *= 1.0 - metallic;	  
  
	vec3 irradiance = texture(iblIrradiance, N).rgb;
	vec3 diffuse    = irradiance * albedo;
  
	const float MAX_REFLECTION_LOD = 8.0;

	//vec3 prefilteredColor = textureLod(iblSpecular, R,  roughness * MAX_REFLECTION_LOD).rgb;
	vec3 prefilteredColor = pow(textureLod(iblSpecular, R,  roughness * MAX_REFLECTION_LOD).rgb, vec3(1.1));

	vec2 envBRDF  = texture(brdfLUT, vec2(max(dot(N, V), 0.0), roughness)).rg;
	vec3 specular = prefilteredColor * (F * envBRDF.x + envBRDF.y);
  
	vec3 ambient = (kD * diffuse + specular) * ao * metalAndShadow.g; 

	
	outputColor =  vec4(ambient + diffuse + specular, 1);

	// mix color and glow (higher glow = more glow color)
	outputColor = glow + /*(vec4(1,1,1,1)) * */ outputColor;
	
	//outputColor = glow + outputColor;
	//outputColor.a = 1.0;

	//outputColor = vec4(1.0) - exp(-outputColor * 3.1);
	//outputColor = pow(outputColor, vec4(1.0/2.2)); 
	
	//outputColor =  vec4(c.a, c.a, c.a, 1); // roughness test
	//outputColor =  vec4(metalAndShadow.r, metalAndShadow.r, metalAndShadow.r, 1); // metalness test


}