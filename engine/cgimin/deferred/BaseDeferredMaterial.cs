﻿using System;
using System.IO;
using Engine.cgimin.object3d;
using OpenTK.Graphics.OpenGL;
using Engine.cgimin.helpers;
using OpenTK;

namespace Engine.cgimin.deferred
{

    public class DeferredMaterialSettings
    {
        // Texturen
        public int colorTexture;
        public int normalTexture;
        public int shadowTexture;
        public int additionalTexture;

        // Animierte Texture
        public float textureAnimationSpeed;

        // Werte für das Blending 
        //public bool transparent;
        public float alpha;
        public BlendingFactorSrc blendFactorSource;
        public BlendingFactorDest blendFactorDest;

        // speed für animation etc.
        public float speed;

        // light options
        public float lightRadius;
        public Vector3 lightColor;
    }

    public class DefferredObjectEntety
    {
        public BaseObject3D object3D;
        public DeferredMaterialSettings settings;
        public float SqDistToCam;
    }


    public abstract class BaseDeferredMaterial
    {
        private int VertexObject;
        private int FragmentObject;

        protected const string DEFERRED_MATERIAL_DIRECTORY = "cgimin/deferred/";

        public int Program;

        // Update
        public static int updateTick = 0;

        public static void Update()
        {
            updateTick++;
        }

        public void CreateShaderProgram(string pathVS, string pathFS)
        {
            Program = ShaderCompiler.CreateShaderProgram(pathVS, pathFS);

            // Hinweis: Program wird noch nicht gelinkt.
        }

        /// <summary>
        /// Abstrakt, damit erbende Klassen zum implementieren einer Draw-Methode gezwungen werden.
        /// </summary>
        public abstract void DrawWithSettings(BaseObject3D object3d, DeferredMaterialSettings settings);

    }
}
