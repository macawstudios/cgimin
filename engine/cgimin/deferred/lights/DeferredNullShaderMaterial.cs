﻿using System;
using Engine.cgimin.camera;
using Engine.cgimin.object3d;
using OpenTK;
using OpenTK.Graphics.OpenGL;
using Engine.cgimin.light;
using Engine.cgimin.shadowmappingcascaded;
namespace Engine.cgimin.deferred.lights
{
    public class DeferredNullShadermaterial : BaseDeferredMaterial
    {

        private int modelviewProjectionMatrixLocation;

        private int midPositionLocation;
        private int radiusLocation;
        private int colorLocation;
        private int cameraPosLocation;

        private int gNormalLocation;
        private int gPositionLocation;
        private int GColorAndRoughnessLocation;
        private int GMetalnessAndShadowLocation;


        public DeferredNullShadermaterial()
        {
            // Shader-Programm wird aus den externen Files generiert...
            CreateShaderProgram("cgimin/deferred/lights/DeferredNull_VS.glsl",
                                "cgimin/deferred/lights/DeferredNull_FS.glsl");

            // GL.BindAttribLocation, gibt an welcher Index in unserer Datenstruktur welchem "in" Parameter auf unserem Shader zugeordnet wird
            // folgende Befehle müssen aufgerufen werden...
            GL.BindAttribLocation(Program, 0, "in_position");


            // ...bevor das Shader-Programm "gelinkt" wird.
            GL.LinkProgram(Program);

            // Die Stelle an der im Shader der per "uniform" der Input-Paremeter "modelview_projection_matrix" definiert wird, wird ermittelt.
            modelviewProjectionMatrixLocation = GL.GetUniformLocation(Program, "modelview_projection_matrix");


        }

        public void PrepareDraw(BaseObject3D object3d)
        {
            // Textur wird "gebunden"
            GL.BindVertexArray(object3d.Vao);

            // Unser Shader Programm wird benutzt
            GL.UseProgram(Program);
        }

        public void FinishDraw()
        {
            // Unbinden des Vertex-Array-Objekt damit andere Operation nicht darauf basieren
            GL.BindVertexArray(0);
        }

        public void Draw(BaseObject3D object3d, float radius, Vector3 color)
        {
            Matrix4 modelviewProjection = object3d.Transformation * Camera.Transformation * Camera.PerspectiveProjection;
            GL.UniformMatrix4(modelviewProjectionMatrixLocation, false, ref modelviewProjection);

            GL.DrawElements(PrimitiveType.Triangles, object3d.Indices.Count, DrawElementsType.UnsignedInt, IntPtr.Zero);
        }

        public override void DrawWithSettings(BaseObject3D object3d, DeferredMaterialSettings settings)
        {
            Draw(object3d, settings.lightRadius, new Vector3(0.5f, 0.5f, 0.1f));
        }

    }
}
