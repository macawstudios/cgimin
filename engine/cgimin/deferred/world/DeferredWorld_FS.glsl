#version 330
precision highp float;

const int NUM_CASCADES = 3;

// Shadowmapping Texturen
uniform sampler2DShadow shadowmap_texture1;
uniform sampler2DShadow shadowmap_texture2;
uniform sampler2DShadow shadowmap_texture3;

uniform float dist3;
uniform float dist2;
uniform float dist1;

uniform sampler2D color_texture; 
uniform sampler2D normalmap_texture;
uniform sampler2D staticshadow_texture;
uniform sampler2D additional_texture;

in vec2 texcoord;
in vec2 secTexcoord;
in vec3 position;
in mat3 fragTBN;
in vec4 viewPosition;
in vec4 ShadowCoord[NUM_CASCADES];

// output
layout (location = 0) out vec4 gColorRoughness;
layout (location = 1) out vec3 gPosition;
layout (location = 2) out vec3 gNormal;
layout (location = 3) out vec2 gMetalnessAndShadow;
layout (location = 4) out vec3 gGlow;


void main()
{
	// additional info als erstes
	vec3 additional = texture(additional_texture, texcoord).rgb;
	if (additional.b < 0.5) discard; 

	// die Vertex-Normale berechnen
    vec4 normalc = texture(normalmap_texture, texcoord).rgba;
	gMetalnessAndShadow.r = normalc.a;
	
	float visibility = 1.0;
	if (-viewPosition.z < dist1) {
		visibility = texture(shadowmap_texture1, vec3(ShadowCoord[0].xy, ShadowCoord[0].z/ShadowCoord[0].w * 0.998));
	} else if (-viewPosition.z < dist2) {
		visibility = texture(shadowmap_texture2, vec3(ShadowCoord[1].xy, ShadowCoord[1].z/ShadowCoord[1].w * 0.998));
	} else if (-viewPosition.z < dist3) {
		visibility = texture(shadowmap_texture3, vec3(ShadowCoord[2].xy, ShadowCoord[2].z/ShadowCoord[2].w * 0.998));
	}	

	gMetalnessAndShadow.g = visibility *  texture(staticshadow_texture, secTexcoord).r * additional.r;

	vec3 normal = vec3(normalc);
	normal = normalize(normal * 2.0 - 1.0); 
	normal = normalize(fragTBN * normal); 
	gNormal = normal;

	vec4 colorRoughness = texture(color_texture, texcoord);
	gColorRoughness = colorRoughness;

	gGlow = colorRoughness.rgb * additional.ggg;

	gPosition = position;


   
}