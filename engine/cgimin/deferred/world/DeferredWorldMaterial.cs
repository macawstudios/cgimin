﻿using System;
using Engine.cgimin.camera;
using Engine.cgimin.object3d;
using OpenTK;
using OpenTK.Graphics.OpenGL;
using Engine.cgimin.light;
using Engine.cgimin.shadowmappingcascaded;
using System.Collections.Generic;
using System.Linq;

namespace Engine.cgimin.deferred.world
{
    public class DeferredWorldMaterial : BaseDeferredMaterial
    {

        private int modelviewProjectionMatrixLocation;
        private int modelViewMatrixLocation;
        private int modelMatrixLocation;

        private int colorTextureLocation;
        private int normalTextureLocation;
        private int staticShadowTextureLocation;
        private int additionalTextureLocation;

        private int shadowTextureLocation1;
        private int shadowTextureLocation2;
        private int shadowTextureLocation3;

        private int dist1Location;
        private int dist2Location;
        private int dist3Location;

        private int DepthBiasMVPLocation1;
        private int DepthBiasMVPLocation2;
        private int DepthBiasMVPLocation3;
        private int lightDirectionLocation;
        private int cameraPositionLocation;

        private List<DefferredObjectEntety> onScreenCollection = new List<DefferredObjectEntety>();

        public DeferredWorldMaterial()
        {
            // Shader-Programm wird aus den externen Files generiert...
            CreateShaderProgram(DEFERRED_MATERIAL_DIRECTORY + "world/DeferredWorld_VS.glsl",
                                DEFERRED_MATERIAL_DIRECTORY + "world/DeferredWorld_FS.glsl");

            // GL.BindAttribLocation, gibt an welcher Index in unserer Datenstruktur welchem "in" Parameter auf unserem Shader zugeordnet wird
            // folgende Befehle müssen aufgerufen werden...
            GL.BindAttribLocation(Program, 0, "in_position");
            GL.BindAttribLocation(Program, 1, "in_normal");
            GL.BindAttribLocation(Program, 2, "in_uv");
            GL.BindAttribLocation(Program, 3, "in_sec_uv");
            GL.BindAttribLocation(Program, 4, "in_tangent");
            GL.BindAttribLocation(Program, 5, "in_bitangent");

            // ...bevor das Shader-Programm "gelinkt" wird.
            GL.LinkProgram(Program);

            DepthBiasMVPLocation1 = GL.GetUniformLocation(Program, "DepthBiasMVP1");
            DepthBiasMVPLocation2 = GL.GetUniformLocation(Program, "DepthBiasMVP2");
            DepthBiasMVPLocation3 = GL.GetUniformLocation(Program, "DepthBiasMVP3");
            lightDirectionLocation = GL.GetUniformLocation(Program, "light_direction");
            cameraPositionLocation = GL.GetUniformLocation(Program, "camera_position");

            modelviewProjectionMatrixLocation = GL.GetUniformLocation(Program, "modelview_projection_matrix");
            modelMatrixLocation = GL.GetUniformLocation(Program, "model_matrix");
            modelViewMatrixLocation = GL.GetUniformLocation(Program, "model_view_matrix");

            colorTextureLocation = GL.GetUniformLocation(Program, "color_texture");
            normalTextureLocation = GL.GetUniformLocation(Program, "normalmap_texture");
            staticShadowTextureLocation = GL.GetUniformLocation(Program, "staticshadow_texture");
            additionalTextureLocation = GL.GetUniformLocation(Program, "additional_texture");

            shadowTextureLocation1 = GL.GetUniformLocation(Program, "shadowmap_texture1");
            shadowTextureLocation2 = GL.GetUniformLocation(Program, "shadowmap_texture2");
            shadowTextureLocation3 = GL.GetUniformLocation(Program, "shadowmap_texture3");

            dist1Location = GL.GetUniformLocation(Program, "dist1");
            dist2Location = GL.GetUniformLocation(Program, "dist2");
            dist3Location = GL.GetUniformLocation(Program, "dist3");

        }

        public void Draw(BaseObject3D object3d, int textureID, int normalTextureID, int staticShadowTextureID, int additionalTextureID)
        {
            PrepareDraw();
            DrawPart(object3d, textureID, normalTextureID, staticShadowTextureID, additionalTextureID);
            FinalizeDraw();
        }


        public void PrepareDraw()
        {
            // Unser Shader Programm wird benutzt
            GL.UseProgram(Program);

            // Shadowmap-Textur wird "gebunden"
            GL.Uniform1(shadowTextureLocation1, 4);
            GL.ActiveTexture(TextureUnit.Texture4);
            GL.BindTexture(TextureTarget.Texture2D, ShadowMappingCascaded.cascades[0].depthTexture);

            GL.Uniform1(shadowTextureLocation2, 5);
            GL.ActiveTexture(TextureUnit.Texture5);
            GL.BindTexture(TextureTarget.Texture2D, ShadowMappingCascaded.cascades[1].depthTexture);

            GL.Uniform1(shadowTextureLocation3, 6);
            GL.ActiveTexture(TextureUnit.Texture6);
            GL.BindTexture(TextureTarget.Texture2D, ShadowMappingCascaded.cascades[2].depthTexture);

        }

        public void DrawPart(BaseObject3D object3d, int textureID, int normalTextureID, int staticShadowTextureID, int additionalTextureID)
        {
            // Farb-Textur wird "gebunden"
            GL.Uniform1(colorTextureLocation, 0);
            GL.ActiveTexture(TextureUnit.Texture0);
            GL.BindTexture(TextureTarget.Texture2D, textureID);

            // Normalmap-Textur wird "gebunden"
            GL.Uniform1(normalTextureLocation, 1);
            GL.ActiveTexture(TextureUnit.Texture1);
            GL.BindTexture(TextureTarget.Texture2D, normalTextureID);

            // statische Shadow-Textur wird "gebunden"
            GL.Uniform1(staticShadowTextureLocation, 2);
            GL.ActiveTexture(TextureUnit.Texture2);
            GL.BindTexture(TextureTarget.Texture2D, staticShadowTextureID);

            // statische Additional-Textur wird "gebunden"
            GL.Uniform1(additionalTextureLocation, 3);
            GL.ActiveTexture(TextureUnit.Texture3);
            GL.BindTexture(TextureTarget.Texture2D, additionalTextureID);


            // das Vertex-Array-Objekt unseres Objekts wird benutzt
            GL.BindVertexArray(object3d.Vao);

            Matrix4 modelViewProjection = object3d.Transformation * Camera.Transformation * Camera.PerspectiveProjection;
            GL.UniformMatrix4(modelviewProjectionMatrixLocation, false, ref modelViewProjection);

            Matrix4 modelView = object3d.Transformation * Camera.Transformation;
            GL.UniformMatrix4(modelViewMatrixLocation, false, ref modelView);

            Matrix4 model = object3d.Transformation;
            GL.UniformMatrix4(modelMatrixLocation, false, ref model);


            GL.Uniform1(dist1Location, ShadowMappingCascaded.cascades[0].borderDistance);
            GL.Uniform1(dist2Location, ShadowMappingCascaded.cascades[1].borderDistance);
            GL.Uniform1(dist3Location, ShadowMappingCascaded.cascades[2].borderDistance);

            Matrix4 depthMVP = object3d.Transformation * ShadowMappingCascaded.cascades[0].shadowTransformation * ShadowMappingCascaded.cascades[0].depthBias * ShadowMappingCascaded.cascades[0].shadowProjection;
            GL.UniformMatrix4(DepthBiasMVPLocation1, false, ref depthMVP);

            Matrix4 depthMVP2 = object3d.Transformation * ShadowMappingCascaded.cascades[1].shadowTransformation * ShadowMappingCascaded.cascades[1].depthBias * ShadowMappingCascaded.cascades[1].shadowProjection;
            GL.UniformMatrix4(DepthBiasMVPLocation2, false, ref depthMVP2);

            Matrix4 depthMVP3 = object3d.Transformation * ShadowMappingCascaded.cascades[2].shadowTransformation * ShadowMappingCascaded.cascades[2].depthBias * ShadowMappingCascaded.cascades[2].shadowProjection;
            GL.UniformMatrix4(DepthBiasMVPLocation3, false, ref depthMVP3);

            GL.Uniform3(lightDirectionLocation, Light.lightDirection);


            // Das Objekt wird gezeichnet
            GL.DrawElements(PrimitiveType.Triangles, object3d.Indices.Count, DrawElementsType.UnsignedInt, IntPtr.Zero);
        }

        public void FinalizeDraw()
        {
            // Unbinden des Vertex-Array-Objekt damit andere Operation nicht darauf basieren
            GL.BindVertexArray(0);
        }

        public void PrepareSortedFlushDraw()
        {
            onScreenCollection.Clear();
        }
        
        public override void DrawWithSettings(BaseObject3D object3d, DeferredMaterialSettings materialSettings)
        {
            onScreenCollection.Add(new DefferredObjectEntety() { object3D = object3d, settings = materialSettings });
        }

        public void FlushDraw()
        {
            PrepareDraw();
            foreach (DefferredObjectEntety entety in onScreenCollection)
            {
                entety.SqDistToCam = (Camera.Position - entety.object3D.Transformation.ExtractTranslation()).LengthSquared;
            }

            onScreenCollection.OrderBy(x => x.SqDistToCam);

            foreach (DefferredObjectEntety entety in onScreenCollection)
            {
                DrawPart(entety.object3D, entety.settings.colorTexture, entety.settings.normalTexture, entety.settings.shadowTexture, entety.settings.additionalTexture);
            }

            FinalizeDraw();
        }


    }
}
