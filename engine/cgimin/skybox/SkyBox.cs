﻿using System;
using Engine.cgimin.texture;
using Engine.cgimin.object3d;
using Engine.cgimin.material.simpletexture;
using Engine.cgimin.camera;
using OpenTK;
using OpenTK.Graphics.OpenGL;

namespace Engine.cgimin.skybox
{
    public class SkyBox
    {

        private static int frontID;
        private static int backID;
        private static int leftID;
        private static int rightID;
        private static int upID;
        private static int downID;

        private static BaseObject3D frontSide;
        private static BaseObject3D backSide;
        private static BaseObject3D leftSide;
        private static BaseObject3D rightSide;
        private static BaseObject3D upSide;
        private static BaseObject3D downSide;

        private static SkyboxTextureMaterial skyboxTextureMaterial;

        public static void Init(String front, String back, String left, String right, String up, String down)
        {
            skyboxTextureMaterial = new SkyboxTextureMaterial();

            frontID = TextureManager.LoadTexture(front, true);
            backID = TextureManager.LoadTexture(back, true);
            leftID = TextureManager.LoadTexture(left, true);
            rightID = TextureManager.LoadTexture(right, true);
            upID = TextureManager.LoadTexture(up, true);
            downID = TextureManager.LoadTexture(down, true);

            float s = 30.0f;

            frontSide = new BaseObject3D();
            frontSide.addTriangle(new Vector3(-s, -s, -s), new Vector3(s, -s, -s), new Vector3(s, s, -s), new Vector2(0, 1), new Vector2(1, 1), new Vector2(1, 0));
            frontSide.addTriangle(new Vector3(-s, -s, -s), new Vector3(s, s, -s), new Vector3(-s, s, -s), new Vector2(0, 1), new Vector2(1, 0), new Vector2(0, 0));
            frontSide.CreateVAO();

            backSide = new BaseObject3D();
            backSide.addTriangle(new Vector3(-s, -s, s), new Vector3(s, -s, s), new Vector3(s, s, s), new Vector2(1, 1), new Vector2(0, 1), new Vector2(0, 0));
            backSide.addTriangle(new Vector3(-s, -s, s), new Vector3(s, s, s), new Vector3(-s, s, s), new Vector2(1, 1), new Vector2(0, 0), new Vector2(1, 0));
            backSide.CreateVAO();

            rightSide = new BaseObject3D();
            rightSide.addTriangle(new Vector3(-s, -s, -s), new Vector3(-s, s, -s), new Vector3(-s, s, s), new Vector2(1, 1), new Vector2(1, 0), new Vector2(0, 0));
            rightSide.addTriangle(new Vector3(-s, -s, -s), new Vector3(-s, s, s), new Vector3(-s, -s, s), new Vector2(1, 1), new Vector2(0, 0), new Vector2(0, 1));
            rightSide.CreateVAO();

            leftSide = new BaseObject3D();
            leftSide.addTriangle(new Vector3(s, -s, -s), new Vector3(s, s, -s), new Vector3(s, s, s), new Vector2(0, 1), new Vector2(0, 0), new Vector2(1, 0));
            leftSide.addTriangle(new Vector3(s, -s, -s), new Vector3(s, s, s), new Vector3(s, -s, s), new Vector2(0, 1), new Vector2(1, 0), new Vector2(1, 1));
            leftSide.CreateVAO();

            upSide = new BaseObject3D();
            upSide.addTriangle(new Vector3(-s, s, -s), new Vector3(s, s, -s), new Vector3(s, s, s), new Vector2(1, 0), new Vector2(0, 0), new Vector2(0, 1));
            upSide.addTriangle(new Vector3(-s, s, -s), new Vector3(s, s, s), new Vector3(-s, s, s), new Vector2(1, 0), new Vector2(0, 1), new Vector2(1, 1));
            upSide.CreateVAO();

            downSide = new BaseObject3D();
            downSide.addTriangle(new Vector3(-s, -s, -s), new Vector3(s, -s, -s), new Vector3(s, -s, s), new Vector2(0, 0), new Vector2(1, 0), new Vector2(1, 1));
            downSide.addTriangle(new Vector3(-s, -s, -s), new Vector3(s, -s, s), new Vector3(-s, -s, s), new Vector2(0, 0), new Vector2(1, 1), new Vector2(0, 1));
            downSide.CreateVAO();

        }

        public static void Draw()
        {
            Matrix4 saveTrasform = Camera.GetTransformMatrix();
            Matrix4 cameraTransform = Matrix4.CreateTranslation(Camera.Position.X, Camera.Position.Y, Camera.Position.Z) * saveTrasform;
            Camera.SetTransformMatrix(cameraTransform);

            GL.Disable(EnableCap.CullFace);
            //GL.Disable(EnableCap.DepthTest);

            GL.ActiveTexture(TextureUnit.Texture0);

            skyboxTextureMaterial.Draw(frontSide, frontID);
            skyboxTextureMaterial.Draw(backSide, backID);
            skyboxTextureMaterial.Draw(leftSide, leftID);
            skyboxTextureMaterial.Draw(rightSide, rightID);
            skyboxTextureMaterial.Draw(upSide, upID);
            skyboxTextureMaterial.Draw(downSide, downID);

            GL.Enable(EnableCap.CullFace);
            //GL.Enable(EnableCap.DepthTest);

            Camera.SetTransformMatrix(saveTrasform);
        }

    }
}
